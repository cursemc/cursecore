package net.aminecraftdev.cursecore.entity;

import com.massivecraft.massivecore.store.Entity;

public class Conf extends Entity<Conf> {

    private static Conf instance;
    public static Conf get() { return instance; }
    public static void set(Conf conf) { instance = conf; }

}
