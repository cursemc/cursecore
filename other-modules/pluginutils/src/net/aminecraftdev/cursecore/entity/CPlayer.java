package net.aminecraftdev.cursecore.entity;

import com.massivecraft.massivecore.store.SenderEntity;
import net.aminecraftdev.cursecore.coll.CPlayerColl;

public class CPlayer extends SenderEntity<CPlayer> {

    public static CPlayer get(Object object) { return CPlayerColl.get().get(object); }

    @Override
    public CPlayer load(CPlayer that) {

        return this;
    }

}
