package net.aminecraftdev.cursecore.entity;

import com.massivecraft.massivecore.store.Entity;
import net.aminecraftdev.cursecore.storage.LangObject;

public class LangConf extends Entity<LangConf> {

    private static LangConf instance;
    public static LangConf get() { return instance; }
    public static void set(LangConf langConf) { instance = langConf; }

    public String errorMessageCode = "&c";
    public String normalMessageCode = "&7";

    public LangObject prefix = new LangObject("&a&lCurse&f&lMC &8» ");
    public LangObject mustBePlayer = new LangObject("You must be a player to use this command!");
    public LangObject invalidNumber = new LangObject("You have specified an invalid number. An example is &f1 or 100&c.");
    public LangObject inventoryFull = new LangObject("Your inventory is currently full!");
    public LangObject insufficientFunds = new LangObject("You do not have enough money to buy this item. You need another &a$&f{amount}&c.");
    public LangObject removeMoney = new LangObject("&c- {amount}");
    public LangObject addMoney = new LangObject("&a+ {amount}");

    public LangObject customEnchantsNotEnoughExp = new LangObject("You do not have enchant EXP to purchase that enchantment book.");
    public LangObject customEnchantsBought = new LangObject("You have bought a &e{rarity} Enchantment Book&7 for &e{cost}&7!");
    public LangObject customEnchantsTinkererAccept = new LangObject("Trade accepted!");
    public LangObject customEnchantsDoesntAccept = new LangObject("Tinkerer does not accept this item.");
    public LangObject customEnchantsAlreadyHasEnchant = new LangObject("That ItemStack already has the enchant applied to it.");
    public LangObject customEnchantsAddedEnchant = new LangObject("You have just successfully applied &f{enchant} {level}&7 to your item.");
    public LangObject customEnchantsFailedProtected = new LangObject("The enchant you have applied failed, however your &fShield Scroll&c protected your item.");
    public LangObject customEnchantsFailedNotDestroy = new LangObject("The enchant you have applied failed, however it did not destroy your item.");
    public LangObject customEnchantsFailedDestroyed = new LangObject("The enchant you have applied failed and destroyed your item.");
    public LangObject customEnchantsEnchantNotFound = new LangObject("The enchantment you specified could not be found.");
    public LangObject customEnchantsBookReceived = new LangObject("You have just received a custom enchantment book.");
    public LangObject customEnchantsBlackScrollReceived = new LangObject("You have just received {amount} Black Scroll(s).");
    public LangObject customEnchantsDustNotFound = new LangObject("The dust type you specified could not be found.");
    public LangObject customEnchantsDustReceived = new LangObject("You have just received &f{type} Dust&7 with the rate of &f{rate}%&7.");
    public LangObject customEnchantsOrderScrollReceived = new LangObject("You have just received {amount} Order Scroll(s).");
    public LangObject customEnchantsShieldScrollReceived = new LangObject("You have just received {amount} Shield Scroll(s).");
    public LangObject customEnchantsItemPurchased = new LangObject("You have just purchased a &f{scroll} Scroll&7 for &a$&f{cost}&7.");
    public LangObject customEnchantsDecreasedDestroy = new LangObject("You have decreased the destroy rate of the enchant book by &f{chance}%&7 to now &f{totalChance}%&7.");
    public LangObject customEnchantsIncreasedSuccess = new LangObject("You have increased the success rate of the enchant book by &f{chance}%&7 to now &f{totalChance}%&7.");
    public LangObject customEnchantsDustFound = new LangObject("You have discovered a &f{dustType} Dust&7 from your Mystery Dust!");
    public LangObject customEnchantsShieldApplied = new LangObject("You have just applied a &bShield Scroll&7 to that custom item.");
    public LangObject customEnchantsDirectNonNearby = new LangObject("There is no &7Diamond Ore&c within a &7{radius}&c block radius.");
    public LangObject customEnchantsMoneyStealAttacker = new LangObject("You have received &f{amount}&7 from the MoneySteal enchant from &f{target}&7.");
    public LangObject customEnchantsMoneyStealDefender = new LangObject("&f{player}&c has just procced MoneySteal on you and you lost &f{amount}&c.");
    public LangObject customEnchantsMoneyStealBlocked = new LangObject("You have just blocked an attempt at &fMoneySteal&7 with your &fAntiSteal&7 enchant.");
    public LangObject customEnchantsItemStealAttacker = new LangObject("You have just received an item from &f{target}&7 due to ItemSteal.");
    public LangObject customEnchantsItemStealDefender = new LangObject("&f{player}&7 just stole an item from your Inventory due to ItemSteal.");
    public LangObject customEnchantsItemStealBlocked = new LangObject("You have just blocked an attempt at &fItemSteal&7 with your &fAntiSteal&7 enchant.");

}
