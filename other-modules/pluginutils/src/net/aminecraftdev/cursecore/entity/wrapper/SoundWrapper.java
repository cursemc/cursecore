package net.aminecraftdev.cursecore.entity.wrapper;

import lombok.Getter;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

public class SoundWrapper {

    @Getter private float volume, pitch;
    @Getter private Sound soundType;

    public SoundWrapper(float volume, float pitch, Sound soundType) {
        this.volume = volume;
        this.pitch = pitch;
        this.soundType = soundType;
    }

    public void playSound(Player player) {
        player.playSound(player.getLocation(), this.soundType, this.volume, this.pitch);
    }

}
