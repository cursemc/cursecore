package net.aminecraftdev.cursecore.entity.wrapper;

import lombok.Getter;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class PotionEffectWrapper {

    @Getter private PotionEffectType potionEffectType;
    @Getter private int level, duration;
    @Getter private boolean overrides;

    public PotionEffectWrapper(PotionEffectType potionEffectType, int level, int duration) {
        this(potionEffectType, level, duration, false);
    }

    public PotionEffectWrapper(PotionEffectType potionEffectType, int level, int duration, boolean overrides) {
        this.potionEffectType = potionEffectType;
        this.level = level;
        this.duration = duration;
        this.overrides = overrides;
    }

    public void addEffect(Player player) {
        if(!isOverrides()) {
            if(player.hasPotionEffect(getPotionEffectType())) {
                PotionEffect potionEffect = player.getActivePotionEffects().stream().filter(pE -> pE.getType() == getPotionEffectType()).findFirst().orElse(null);

                if(potionEffect != null) {
                    int level = potionEffect.getAmplifier()-1;

                    if(level > getLevel()) return;
                }

                player.removePotionEffect(getPotionEffectType());
            }
        }

        player.addPotionEffect(new PotionEffect(getPotionEffectType(), getDuration() < 0? Integer.MAX_VALUE : getDuration()*20, getLevel()-1));
    }

    public void removeEffect(Player player) {
        player.removePotionEffect(this.potionEffectType);
    }

}
