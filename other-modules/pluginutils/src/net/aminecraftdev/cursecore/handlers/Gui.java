package net.aminecraftdev.cursecore.handlers;

import lombok.Getter;
import net.aminecraftdev.cursecore.storage.Icon;
import net.aminecraftdev.cursecore.storage.InventoryStorage;
import net.aminecraftdev.cursecore.utils.NbtFactory;
import org.bukkit.craftbukkit.v1_8_R3.inventory.CraftItemStack;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class Gui implements InventoryHolder {

    private Map<String, List<Integer>> actionSlots = new HashMap<>();
    private Map<String, Icon> actionIconSlots = new HashMap<>();
    private Map<Integer, String> actionLocations = new HashMap<>();

    @Getter private boolean closed = false;

    private final Player player;
    private Inventory inventory;

    public Gui(Player player) {
        this.player = player;
        this.inventory = null;
    }

    public Gui(Player player, InventoryStorage inventoryStorage) {
        this.inventory = inventoryStorage.toInventory(this);
        this.player = player;
    }

    public abstract void init();
    public abstract Map<String, String> getReplaceMap();

    @Override
    public Inventory getInventory() {
        return this.inventory;
    }

    public Player getPlayer() {
        return this.player;
    }

    public void setInventory(InventoryStorage inventoryStorage) {
        this.inventory = inventoryStorage.toInventory(this);
    }

    public void open() {
        this.init();

        this.player.openInventory(this.inventory);
    }

    public void close(boolean close) {
        this.closed = true;

        if (close) {
            this.player.closeInventory();
        }
    }

    public String getAction(ItemStack itemStack) {
        NbtFactory.NbtCompound nbtCompound = NbtFactory.fromItemTag(NbtFactory.getCraftItemStack(itemStack));

        return nbtCompound.getString("Action", "");
    }

    public void addActionSlot(String action, int slot) {
        List<Integer> slots = this.actionSlots.getOrDefault(action, new ArrayList<>());

        slots.add(slot);
        this.actionSlots.put(action, slots);
        this.actionLocations.put(slot, action);
    }

    public void addActionSlot(String action, Icon icon) {
        this.actionIconSlots.put(action, icon);
    }

    public List<Integer> getActionSlots(String action) {
        return this.actionSlots.getOrDefault(action, new ArrayList<>());
    }

    public String getActionSlot(int slot) {
        return this.actionLocations.getOrDefault(slot, "EMPTY");
    }

    public Icon getIconSlot(String action) {
        return this.actionIconSlots.getOrDefault(action, null);
    }

}
