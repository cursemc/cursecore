package net.aminecraftdev.cursecore.storage;

import com.massivecraft.massivecore.util.Txt;
import net.aminecraftdev.cursecore.entity.LangConf;
import org.bukkit.command.CommandSender;

import java.util.Map;

public class LangObject {

    private String message;

    public LangObject(String message) {
        this.message = message;
    }

    public String toString() {
        return this.message;
    }

    public String toExceptionString() {
        return Txt.parse(LangConf.get().errorMessageCode + toString());
    }

    public void msgError(CommandSender commandSender) {
        msgError(commandSender, null);
    }

    public void msgError(CommandSender commandSender, Map<String, String> replaceMap) {
        msg(commandSender, replaceMap, LangConf.get().errorMessageCode + this.message, true);
    }

    public void msgEmpty(CommandSender commandSender) {
        msgEmpty(commandSender, null);
    }

    public void msgEmpty(CommandSender commandSender, Map<String, String> replaceMap) {
        msg(commandSender, replaceMap, this.message, false);
    }

    public void msg(CommandSender commandSender) {
        msg(commandSender, null);
    }

    public void msg(CommandSender commandSender, Map<String, String> replaceMap) {
        msg(commandSender, replaceMap, LangConf.get().normalMessageCode + this.message, true);
    }

    private void msg(CommandSender commandSender, Map<String, String> replaceMap, String message, boolean prefix) {
        if(replaceMap != null) {
            for(String placeholder : replaceMap.keySet()) {
                message = message.replace(placeholder, replaceMap.get(placeholder));
            }
        }

        if(prefix) {
            commandSender.sendMessage(Txt.parse(LangConf.get().prefix.toString() + message));
        } else {
            commandSender.sendMessage(Txt.parse(message));
        }
    }

}
