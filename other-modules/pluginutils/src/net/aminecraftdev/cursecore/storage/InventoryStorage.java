package net.aminecraftdev.cursecore.storage;

import com.massivecraft.massivecore.util.Txt;
import net.aminecraftdev.cursecore.handlers.Gui;
import net.aminecraftdev.cursecore.utils.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.inventory.Inventory;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 04-Aug-19
 */
public class InventoryStorage {

    private Map<Integer, Icon> defaultItemStacks = new HashMap<>();

    private String name;
    private int slots;

    public InventoryStorage(String name, int slots, Map<Integer, Icon> defaultItemStacks) {
        this.name = name;
        this.slots = slots;
        this.defaultItemStacks.putAll(defaultItemStacks);
    }

    public Inventory toInventory(Gui gui) {
        String name = this.name;

        if(gui.getReplaceMap() != null) {
            name = StringUtils.get().replaceLine(gui.getReplaceMap(), name);
        }

        Inventory inventory = Bukkit.createInventory(gui, this.slots, Txt.parse(name));

        this.defaultItemStacks.forEach((slot, icon) -> {
            if(slot == -1) {
                for(int i = 0; i < inventory.getSize(); i++) {
                    inventory.setItem(i, icon.toItemStack());
                }
            } else {
                inventory.setItem(slot, icon.toItemStack(gui.getReplaceMap()));
            }

            if(icon.getAction() != null) {
                gui.addActionSlot(icon.getAction(), slot);
                gui.addActionSlot(icon.getAction(), icon);
            }
        });

        return inventory;
    }


}
