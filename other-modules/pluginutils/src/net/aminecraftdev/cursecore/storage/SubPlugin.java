package net.aminecraftdev.cursecore.storage;

import com.massivecraft.massivecore.Active;
import com.massivecraft.massivecore.Engine;
import com.massivecraft.massivecore.MassivePlugin;
import com.massivecraft.massivecore.ModuloRepeatTask;
import com.massivecraft.massivecore.command.MassiveCommand;
import com.massivecraft.massivecore.store.Coll;

import java.util.ArrayList;
import java.util.List;

public abstract class SubPlugin {

    private static MassivePlugin massivePlugin = PluginStorage.get();

    private final List<Class<? extends MassiveCommand>> commands = new ArrayList<>();
    private final List<Class<? extends ModuloRepeatTask>> tasks = new ArrayList<>();
    private final List<Class<? extends Engine>> engines = new ArrayList<>();
    private final List<Class<? extends Coll>> colls = new ArrayList<>();

    private final List<Active> commandInstances = new ArrayList<>();
    private final List<Active> engineInstances = new ArrayList<>();
    private final List<Active> taskInstances = new ArrayList<>();
    private final List<Active> collInstances = new ArrayList<>();

    public abstract String getPluginName();

    protected abstract void onEnableInner();
    protected abstract void onEnablePost();

    protected abstract void onDisable();

    protected void addEngine(Class<? extends Engine> engine) {
        this.engines.add(engine);
    }

    protected void addCommand(Class<? extends MassiveCommand> massiveCommand) {
        this.commands.add(massiveCommand);
    }

    protected void addColl(Class<? extends Coll> coll) {
        this.colls.add(coll);
    }

    protected void addTask(Class<? extends ModuloRepeatTask> task) {
        this.tasks.add(task);
    }

    public void enableModule() {
        massivePlugin.log("== ENABLING " + getPluginName() + " MODULE ==");

        long startMs = System.currentTimeMillis();

        this.collInstances.clear();
        this.colls.clear();
        this.engineInstances.clear();
        this.engines.clear();
        this.commandInstances.clear();
        this.commands.clear();
        this.taskInstances.clear();
        this.tasks.clear();

        onEnableInner();

        massivePlugin.log(" - Activating Colls");
        massivePlugin.activateCollection(this.collInstances, this.colls.toArray());

        massivePlugin.log(" - Activating Engines");
        massivePlugin.activateCollection(this.engineInstances, this.engines.toArray());

        massivePlugin.log(" - Activating Commands");
        massivePlugin.activateCollection(this.commandInstances, this.commands.toArray());

        massivePlugin.log(" - Activating Tasks");
        massivePlugin.activateCollection(this.taskInstances, this.tasks.toArray());

        onEnablePost();

        massivePlugin.log("== '" + getPluginName() + "' TOOK " + (System.currentTimeMillis() - startMs) + "ms TO LOAD ==");
    }

    public void disableModule() {
        massivePlugin.log("Disabling " + getPluginName() + " module...");

        long startMs = System.currentTimeMillis();

        onDisable();

        massivePlugin.log(" - Deactivating Tasks");
        massivePlugin.deactivate(this.taskInstances);

        massivePlugin.log(" - Deactivating Commands");
        massivePlugin.deactivate(this.commandInstances);

        massivePlugin.log(" - Deactivating Engines");
        massivePlugin.deactivate(this.engineInstances);

        massivePlugin.log(" - Deactivating Colls");
        massivePlugin.deactivate(this.collInstances);

        this.taskInstances.clear();
        this.tasks.clear();
        this.commandInstances.clear();
        this.commands.clear();
        this.engineInstances.clear();
        this.engines.clear();
        this.collInstances.clear();
        this.colls.clear();

        massivePlugin.log("== '" + getPluginName() + "' TOOK " + (System.currentTimeMillis() - startMs) + "ms TO UNLOAD ==");
    }

}
