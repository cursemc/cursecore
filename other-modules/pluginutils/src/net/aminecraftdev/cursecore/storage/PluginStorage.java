package net.aminecraftdev.cursecore.storage;

import com.massivecraft.massivecore.MassivePlugin;

import java.util.HashMap;
import java.util.Map;

public class PluginStorage {

    private static MassivePlugin massivePlugin;
    public static MassivePlugin get() { return massivePlugin; }
    public static void set(MassivePlugin plugin) { massivePlugin = plugin; }

    private static final transient Map<String, Boolean> currentPluginStatus = new HashMap<>();
    private static Map<String, Boolean> getCurrentPluginStatus() { return new HashMap<>(currentPluginStatus); }

    public static boolean hasStatusChanged(String name, boolean newStatus) {
        if(!getCurrentPluginStatus().containsKey(name)) {
            currentPluginStatus.put(name, newStatus);
            return true;
        }

        boolean status = getCurrentPluginStatus().get(name);

        if(status != newStatus) {
            currentPluginStatus.put(name, newStatus);
            return true;
        } else {
            return false;
        }
    }
}
