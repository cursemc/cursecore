package net.aminecraftdev.cursecore.storage;

import com.google.common.collect.Lists;
import com.massivecraft.massivecore.store.EntityInternal;
import com.massivecraft.massivecore.util.Txt;
import lombok.Getter;
import net.aminecraftdev.cursecore.utils.NbtFactory;
import net.aminecraftdev.cursecore.utils.StringUtils;
import net.minecraft.server.v1_8_R3.NBTTagCompound;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_8_R3.inventory.CraftItemStack;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Icon extends EntityInternal<Icon> {

    private Material type;
    private int amount;
    private short damage;

    private String displayName;
    private List<String> lore;

    private boolean shiny;

    @Getter private String action;

    public Icon(String action) {
        this(Material.AIR, 1, (short) 0, null, Lists.newArrayList(), false, action);
    }

    public Icon(Material type, int amount, short damage) {
        this(type, amount, damage, null, Lists.newArrayList(), false, "FILLER");
    }

    public Icon(Material type, int amount, short damage, boolean emptyName) {
        this(type, amount, damage, emptyName? "&7" : null, Lists.newArrayList(), false, "FILLER");
    }

    public Icon(Material type, int amount, short damage,
                String displayName, List<String> lore,
                boolean shiny,
                String action) {
        this.type = type;
        this.amount = amount;
        this.damage = damage;

        this.displayName = displayName;
        this.lore = lore;

        this.shiny = shiny;

        this.action = action;
    }

    @Override
    public Icon load(Icon that) {
        this.type = that.type;
        this.amount = that.amount;
        this.damage = that.damage;

        this.displayName = that.displayName;
        this.lore = that.lore;

        this.shiny = that.shiny;

        this.action = that.action;

        return this;
    }

    public ItemStack toItemStack(Map<String, String> replaceMap) {
        ItemStack itemStack = new ItemStack(this.type, this.amount, this.damage);
        ItemMeta itemMeta = itemStack.getItemMeta();

        if(this.displayName != null) {
            String name;

            if(replaceMap != null) {
                name = Txt.parse(StringUtils.get().replaceLine(replaceMap, this.displayName));
            } else {
                name = Txt.parse(this.displayName);
            }

            itemMeta.setDisplayName(name);
        }
        if(this.lore != null && !this.lore.isEmpty()) {
            List<String> lore = new ArrayList<>();

            for(String s : new ArrayList<>(this.lore)) {
                if(replaceMap != null) {
                    lore.add(Txt.parse(StringUtils.get().replaceLine(replaceMap, s)));
                } else {
                    lore.add(Txt.parse(s));
                }
            }

            itemMeta.setLore(lore);
        }

        if (this.shiny) {
            itemMeta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
            itemMeta.addEnchant(Enchantment.DURABILITY, 1, true);
        }

        itemStack.setItemMeta(itemMeta);

        if(itemStack.getType() == Material.AIR) return itemStack;
        if(this.action == null) return itemStack;

        ItemStack craftStack = NbtFactory.getCraftItemStack(itemStack);
        NbtFactory.NbtCompound nbtCompound = NbtFactory.fromItemTag(craftStack);

        nbtCompound.put("Action", this.action);

        return craftStack;
    }

    public ItemStack toItemStack() {
        return toItemStack(null);
    }
}
