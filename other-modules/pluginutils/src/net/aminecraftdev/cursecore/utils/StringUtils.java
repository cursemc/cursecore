package net.aminecraftdev.cursecore.utils;

import java.util.Map;

public class StringUtils {

    private static StringUtils instance = new StringUtils();
    public static StringUtils get() { return instance; }

    public boolean isEmpty(String input) {
        return input == null || input.length() == 0;
    }

    public String substringBeforeLast(String input, String separator) {
        if(isEmpty(input) || isEmpty(separator)) return input;

        int index = input.indexOf(separator);

        return index == -1? input : input.substring(0, index);
    }

    public String replaceLine(Map<String, String> replaceMap, String line) {
        String parsed = line;

        for(String key : replaceMap.keySet()) {
            parsed = parsed.replace(key, replaceMap.get(key));
        }

        return parsed;
    }

}
