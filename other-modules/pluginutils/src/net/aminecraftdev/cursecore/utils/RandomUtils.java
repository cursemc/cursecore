package net.aminecraftdev.cursecore.utils;

import java.util.concurrent.ThreadLocalRandom;

public class RandomUtils {

    private static RandomUtils instance = new RandomUtils();
    public static RandomUtils get() { return instance; }

    private ThreadLocalRandom random = ThreadLocalRandom.current();

    public int getRandomNumber(int amount) {
        return this.random.nextInt(amount);
    }

    public boolean canExecute() {
        return this.random.nextBoolean();
    }

    public int getRandomBetweenHundred() {
        return getRandomNumber(100) + 1;
    }

    public double getRandomDouble() {
        return this.random.nextDouble();
    }

    public double getRandomWholeDouble(int amount) {
        double d = this.random.nextInt(amount);

        return d + getRandomDouble();
    }

}
