package net.aminecraftdev.cursecore.utils.dependencies;

import lombok.Getter;
import net.milkbowl.vault.chat.Chat;
import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.permission.Permission;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.plugin.RegisteredServiceProvider;

public class VaultHelper {

    private static VaultHelper instance = new VaultHelper();
    public static VaultHelper get() { return instance; }

    @Getter private Permission permission;
    @Getter private Economy economy;
    @Getter private Chat chat;

    public void load() {
        RegisteredServiceProvider<Economy> economyRsp = Bukkit.getServer().getServicesManager().getRegistration(Economy.class);

        if(economyRsp != null) {
            this.economy = economyRsp.getProvider();
        }

        RegisteredServiceProvider<Permission> permissionRsp = Bukkit.getServer().getServicesManager().getRegistration(Permission.class);

        if(permissionRsp != null) {
            this.permission = permissionRsp.getProvider();
        }

        RegisteredServiceProvider<Chat> chatRsp = Bukkit.getServer().getServicesManager().getRegistration(Chat.class);

        if(chatRsp != null) {
            this.chat = chatRsp.getProvider();
        }
    }

    public double getBalance(OfflinePlayer offlinePlayer) {
        return getEconomy().getBalance(offlinePlayer);
    }

    public void withdrawBalance(OfflinePlayer offlinePlayer, double balance) {
        getEconomy().withdrawPlayer(offlinePlayer, balance);
    }

    public void depositBalance(OfflinePlayer offlinePlayer, double balance) {
        getEconomy().depositPlayer(offlinePlayer, balance);
    }

}
