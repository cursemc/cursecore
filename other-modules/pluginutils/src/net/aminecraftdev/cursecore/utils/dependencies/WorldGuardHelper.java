package net.aminecraftdev.cursecore.utils.dependencies;

import com.sk89q.worldguard.bukkit.WGBukkit;
import com.sk89q.worldguard.protection.ApplicableRegionSet;
import com.sk89q.worldguard.protection.flags.DefaultFlag;
import com.sk89q.worldguard.protection.flags.StateFlag;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import org.bukkit.Bukkit;
import org.bukkit.Location;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class WorldGuardHelper {

    private static WorldGuardHelper instance = new WorldGuardHelper();
    public static WorldGuardHelper get() { return instance; }

    private Boolean loaded = false;

    private boolean isPluginLoaded() {
        return loaded != null? loaded : (loaded = Bukkit.getServer().getPluginManager().getPlugin("WorldEdit") != null
                && Bukkit.getServer().getPluginManager().getPlugin("WorldGuard") != null);
    }

    public boolean isPvPAllowed(Location location) {
        if(isPluginLoaded()) {
            ApplicableRegionSet regionSet = WGBukkit.getPlugin().getRegionManager(location.getWorld()).getApplicableRegions(location);

            return regionSet.queryState(null, DefaultFlag.PVP) != StateFlag.State.DENY;
        }

        return true;
    }

    public boolean isBreakAllowed(Location location) {
        if(isPluginLoaded()) {
            ApplicableRegionSet set = WGBukkit.getPlugin().getRegionManager(location.getWorld()).getApplicableRegions(location);

            return set.queryState(null, DefaultFlag.BLOCK_BREAK) != StateFlag.State.DENY;
        }

        return true;
    }

    public boolean isMobSpawningAllowed(Location location) {
        if(isPluginLoaded()) {
            ApplicableRegionSet set = WGBukkit.getPlugin().getRegionManager(location.getWorld()).getApplicableRegions(location);

            return set.queryState(null, DefaultFlag.MOB_SPAWNING) != StateFlag.State.DENY;
        }

        return true;
    }

    public List<String> getRegionNames(Location location) {
        List<String> regions = new ArrayList<>();

        if(!isPluginLoaded()) return regions;

        ApplicableRegionSet set = WGBukkit.getPlugin().getRegionManager(location.getWorld()).getApplicableRegions(location);
        LinkedList<String> parentNames = new LinkedList<>();

        for(ProtectedRegion region : set) {
            String id = region.getId();

            regions.add(id);

            ProtectedRegion parent = region.getParent();

            while(parent != null) {
                parentNames.add(parent.getId());
                parent = parent.getParent();
            }
        }

        for(String name : parentNames) {
            regions.remove(name);
        }

        return regions;
    }


}
