package net.aminecraftdev.cursecore.utils;

import com.google.common.base.CharMatcher;
import lombok.Getter;

public class RomanNumberUtils {

    private static RomanNumberUtils instance = new RomanNumberUtils();
    public static RomanNumberUtils get() { return instance; }

    public String getRomanNumber(int value) {
        if(value <= 0) return null;

        StringBuilder stringBuilder = new StringBuilder();
        RomanNumber[] romanNumbers = RomanNumber.values();

        for(int i = 0; i < romanNumbers.length; i++) {
            RomanNumber romanNumber = romanNumbers[i];

            while(value >= romanNumber.getValue()) {
                value -= romanNumber.getValue();
                stringBuilder.append(romanNumber.name());
            }

            if(i < romanNumbers.length - 1) {
                int amount = i - i % 2+2;

                RomanNumber number = romanNumbers[amount];

                if(value >= romanNumber.getValue() - number.getValue()) {
                    value -= romanNumber.getValue() - number.getValue();
                    stringBuilder.append(number.name());
                    stringBuilder.append(romanNumber.name());
                }
            }
        }

        return stringBuilder.toString();
    }

    public int getValueOf(String input) {
        if(!CharMatcher.anyOf("MDCLXVI").matchesAllOf(input)) return 0;

        char[] chars = input.toCharArray();
        int amount = 0;

        for(int i = 0; i < chars.length; i++) {
            int value = getValueOf(chars[i]);

            if(i < chars.length - 1 && getValueOf(chars[i+1]) > value) {
                value = -value;
            }

            if(value == 0) return 0;

            amount += value;
        }

        return amount;
    }

    public int getValueOf(char number) {
        String input = ("" + number).toUpperCase();

        try {
            RomanNumber romanNumber = RomanNumber.valueOf(input);

            return romanNumber.getValue();
        } catch (IllegalArgumentException ex) {
            return 0;
        }
    }

    private enum RomanNumber {

        M(1000),
        D(500),
        C(100),
        L(50),
        X(10),
        V(5),
        I(1);

        @Getter private int value;

        RomanNumber(int value) {
            this.value = value;
        }


    }

}
