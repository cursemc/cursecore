package net.aminecraftdev.cursecore.utils;

import java.text.DecimalFormat;

public class NumberUtils {

    private static NumberUtils instance = new NumberUtils();
    public static NumberUtils get() { return instance; }

    private DecimalFormat numberFormat = new DecimalFormat("#,###.##");

    public String formatNumber(double d) {
        return this.numberFormat.format(d);
    }

    public boolean isDouble(String input) {
        try {
            Double.valueOf(input);
            return true;
        } catch (NumberFormatException ex) {
            return false;
        }
    }

    public boolean isInt(String input) {
        try {
            Integer.valueOf(input);
            return true;
        } catch (NumberFormatException ex) {
            return false;
        }
    }

    public int getNextNumber(int number, int divisible) {
        return (number >= 54) ? 54 : number + (divisible - number % divisible) * Math.min(1, number % divisible);
    }
}
