package net.aminecraftdev.cursecore.utils;

public interface ISimilar<T> {

    boolean isSimilar(T comparable);

}
