package net.aminecraftdev.cursecore.coll;

import com.massivecraft.massivecore.MassiveCore;
import com.massivecraft.massivecore.store.Coll;
import com.massivecraft.massivecore.store.MStore;
import net.aminecraftdev.cursecore.entity.Conf;
import net.aminecraftdev.cursecore.storage.PluginStorage;

public class ConfColl extends Coll<Conf> {

    private static ConfColl instance = new ConfColl();
    public static ConfColl get() { return instance; }

    private ConfColl() {
        super("cursecore_conf", Conf.class, MStore.getDb(), PluginStorage.get());
    }

    @Override
    public void setActive(boolean active) {
        super.setActive(active);

        if(!active) return;

        Conf.set(this.get(MassiveCore.INSTANCE, true));
    }

}
