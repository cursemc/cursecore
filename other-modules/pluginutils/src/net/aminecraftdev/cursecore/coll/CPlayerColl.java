package net.aminecraftdev.cursecore.coll;

import com.massivecraft.massivecore.store.Coll;
import com.massivecraft.massivecore.store.MStore;
import net.aminecraftdev.cursecore.entity.CPlayer;
import net.aminecraftdev.cursecore.storage.PluginStorage;

public class CPlayerColl extends Coll<CPlayer> {

    private static CPlayerColl instance = new CPlayerColl();
    public static CPlayerColl get() { return instance; }

    private CPlayerColl() {
        super("cursecore_players", CPlayer.class, MStore.getDb(), PluginStorage.get());

        this.setCreative(true);
    }

}
