package net.aminecraftdev.cursecore.coll;

import com.massivecraft.massivecore.MassiveCore;
import com.massivecraft.massivecore.store.Coll;
import com.massivecraft.massivecore.store.MStore;
import net.aminecraftdev.cursecore.entity.LangConf;
import net.aminecraftdev.cursecore.storage.PluginStorage;

public class LangConfColl extends Coll<LangConf> {

    private static LangConfColl instance = new LangConfColl();
    public static LangConfColl get() { return instance; }

    private LangConfColl() {
        super("cursecore_lang", LangConf.class, MStore.getDb(), PluginStorage.get());
    }

    @Override
    public void setActive(boolean active) {
        super.setActive(active);

        if(!active) return;

        LangConf.set(this.get(MassiveCore.INSTANCE, true));

    }

}
