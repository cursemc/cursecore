package net.aminecraftdev.cursecore.entity;

import com.massivecraft.massivecore.store.Entity;
import com.massivecraft.massivecore.util.MUtil;
import net.aminecraftdev.cursecore.SubPlugins;

import java.util.Map;

public class PluginsConf extends Entity<PluginsConf> {

    private static PluginsConf instance;
    public static PluginsConf get() { return instance; }
    public static void set(PluginsConf conf) { instance = conf; }

    public Map<String, Boolean> pluginStatus = MUtil.map(
            "customenchants", true
    );

    @Override
    public PluginsConf load(PluginsConf that) {
        if(that.pluginStatus != null) {
            this.pluginStatus = that.pluginStatus;

            if(get() != null) {
                SubPlugins.recheckPlugins();
            }
        }

        return this;
    }
}
