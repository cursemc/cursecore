package net.aminecraftdev.cursecore.engines;

import com.massivecraft.massivecore.Engine;
import com.massivecraft.massivecore.util.MUtil;
import net.aminecraftdev.cursecore.events.PlayerMoveBlockEvent;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.PlayerMoveEvent;

public class MoveBlockEngine extends Engine {

    private static MoveBlockEngine instance = new MoveBlockEngine();
    public static MoveBlockEngine get() { return instance; }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onMove(PlayerMoveEvent event) {
        if(MUtil.isSameBlock(event)) return;

        PlayerMoveBlockEvent playerMoveBlockEvent = new PlayerMoveBlockEvent(event.getPlayer(), event.getFrom(), event.getTo());
        Bukkit.getPluginManager().callEvent(playerMoveBlockEvent);

        event.setCancelled(playerMoveBlockEvent.isCancelled());
        event.setFrom(playerMoveBlockEvent.getFrom());
        event.setTo(playerMoveBlockEvent.getTo());
    }

}
