package net.aminecraftdev.cursecore.coll;

import com.massivecraft.massivecore.MassiveCore;
import com.massivecraft.massivecore.store.Coll;
import com.massivecraft.massivecore.store.MStore;
import net.aminecraftdev.cursecore.SubPlugins;
import net.aminecraftdev.cursecore.entity.PluginsConf;
import net.aminecraftdev.cursecore.storage.PluginStorage;

public class PluginsConfColl extends Coll<PluginsConf> {

    private static final PluginsConfColl instance = new PluginsConfColl();
    public static PluginsConfColl get() { return instance; }

    private PluginsConfColl() {
        super("cursecore_plugins", PluginsConf.class, MStore.getDb(), PluginStorage.get());
    }

    @Override
    public void setActive(boolean active) {
        super.setActive(active);

        if(!active) return;

        PluginsConf.set(this.get(MassiveCore.INSTANCE));
        SubPlugins.recheckPlugins();
    }

}
