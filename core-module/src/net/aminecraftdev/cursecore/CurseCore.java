package net.aminecraftdev.cursecore;

import com.massivecraft.massivecore.MassivePlugin;
import net.aminecraftdev.cursecore.coll.CPlayerColl;
import net.aminecraftdev.cursecore.coll.ConfColl;
import net.aminecraftdev.cursecore.coll.LangConfColl;
import net.aminecraftdev.cursecore.coll.PluginsConfColl;
import net.aminecraftdev.cursecore.engines.MoveBlockEngine;
import net.aminecraftdev.cursecore.storage.PluginStorage;
import net.aminecraftdev.cursecore.utils.dependencies.VaultHelper;
import net.aminecraftdev.cursecore.utils.dependencies.WorldGuardHelper;

public class CurseCore extends MassivePlugin {

    private static CurseCore instance;
    public static CurseCore get() { return instance; }

    public CurseCore() {
        setVersionSynchronized(false);
    }

    @Override
    public void onEnableInner() {
        instance = this;
        PluginStorage.set(this);

        log("Loading VaultHelper...");
        VaultHelper.get().load();

        log("Loading LangConf, CPlayer and Conf Coll's");
        this.activate(
                LangConfColl.class,
                CPlayerColl.class,
                ConfColl.class
        );

        log("Registering MoveBlockEngine");
        this.activate(
                MoveBlockEngine.class
        );

        log("Beginning to load PluginConfs...");

        long begin = System.currentTimeMillis();

        this.activate(
                PluginsConfColl.class
        );

        log("PluginConf's successfully loaded and took " + (System.currentTimeMillis() - begin) + "ms!");
    }

    @Override
    public void onDisable() {
        super.onDisable();
    }

}
