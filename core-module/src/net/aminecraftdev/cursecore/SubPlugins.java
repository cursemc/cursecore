package net.aminecraftdev.cursecore;

import net.aminecraftdev.cursecore.customenchants.CustomEnchants;
import net.aminecraftdev.cursecore.entity.PluginsConf;
import net.aminecraftdev.cursecore.storage.SubPlugin;

import java.util.HashMap;
import java.util.Map;

public enum SubPlugins {

    CustomEnchants(new CustomEnchants());

    private static final transient Map<String, Boolean> currentPluginStatus = new HashMap<>();
    private static Map<String, Boolean> getCurrentPluginStatus() { return new HashMap<>(currentPluginStatus); }

    private SubPlugin subPlugin;

    SubPlugins(SubPlugin subPlugin) {
        this.subPlugin = subPlugin;
    }

    public static boolean hasStatusChanged(String name, boolean newStatus) {
        if(!getCurrentPluginStatus().containsKey(name)) {
            currentPluginStatus.put(name, newStatus);
            return true;
        }

        boolean status = getCurrentPluginStatus().get(name);

        if(status != newStatus) {
            currentPluginStatus.put(name, newStatus);
            return true;
        } else {
            return false;
        }
    }

    public static void recheckPlugins() {
        for(SubPlugins subPlugin : values()) {
            String pluginName = subPlugin.subPlugin.getPluginName().toLowerCase();
            boolean currentStatus = getCurrentPluginStatus().getOrDefault(pluginName, false);

            if(hasStatusChanged(pluginName, PluginsConf.get().pluginStatus.getOrDefault(pluginName, false))) {
                boolean status = getCurrentPluginStatus().get(pluginName);

                if(status) {
                    subPlugin.subPlugin.enableModule();
                } else {
                    if(!currentStatus) return;

                    subPlugin.subPlugin.disableModule();
                }
            }
        }
    }
}
