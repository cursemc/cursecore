package net.aminecraftdev.cursecore.customenchants.events.enchants;

import lombok.Getter;
import net.aminecraftdev.cursecore.customenchants.events.StealEnchantProcEvent;
import org.bukkit.entity.Player;

public class MoneyStealEnchantProcEvent extends StealEnchantProcEvent {

    @Getter private double money;

    public MoneyStealEnchantProcEvent(Player player, Player target, int level, double money) {
        super(player, target, level);

        this.money = money;
    }
}
