package net.aminecraftdev.cursecore.customenchants.events.enchants;

import lombok.Getter;
import net.aminecraftdev.cursecore.customenchants.events.EnchantProcEvent;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 26-Jun-17
 */
public class BowEnchantProcEvent extends EnchantProcEvent {

    @Getter private double damage, originalDamage;
    @Getter private Projectile projectile;
    @Getter private LivingEntity target;
    @Getter private boolean pvp;

    public BowEnchantProcEvent(Player player, LivingEntity target, double damage, Projectile projectile, boolean pvp) {
        super(player);

        this.pvp = pvp;
        this.target = target;
        this.damage = damage;
        this.projectile = projectile;
        this.originalDamage = damage;
    }

    public void setDamage(double damage) {
        this.damage = damage;
    }
}
