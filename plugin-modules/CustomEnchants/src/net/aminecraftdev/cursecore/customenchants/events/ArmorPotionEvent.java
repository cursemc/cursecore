package net.aminecraftdev.cursecore.customenchants.events;

import lombok.Getter;
import lombok.Setter;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerEvent;
import org.bukkit.inventory.ItemStack;

public class ArmorPotionEvent extends PlayerEvent implements Cancellable {

    private static HandlerList HANDLER = new HandlerList();

    @Getter @Setter private boolean cancelled;
    @Getter private ItemStack itemStack;

    public ArmorPotionEvent(Player player, ItemStack itemStack) {
        super(player);

        this.itemStack = itemStack;
    }

    @Override
    public HandlerList getHandlers() {
        return HANDLER;
    }

    public static HandlerList getHandlerList() {
        return HANDLER;
    }
}
