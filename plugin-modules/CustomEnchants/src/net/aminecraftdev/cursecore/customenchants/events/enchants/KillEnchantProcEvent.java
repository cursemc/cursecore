package net.aminecraftdev.cursecore.customenchants.events.enchants;

import lombok.Getter;
import lombok.Setter;
import net.aminecraftdev.cursecore.customenchants.events.EnchantProcEvent;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.List;

public class KillEnchantProcEvent extends EnchantProcEvent {

    @Getter @Setter private double expMultiplier = 1.0;
    @Getter @Setter private int droppedExp;
    @Getter private List<ItemStack> drops;
    @Getter private LivingEntity target;
    @Getter private boolean pvp;

    public KillEnchantProcEvent(Player player, LivingEntity target, boolean pvp, int droppedExp, List<ItemStack> drops) {
        super(player);

        this.target = target;
        this.drops = drops;
        this.pvp = pvp;
        this.droppedExp = droppedExp;
    }

    public void addDrop(ItemStack itemStack) {
        this.drops.add(itemStack);
    }

    public void clearDrops() {
        this.drops.clear();
    }
}
