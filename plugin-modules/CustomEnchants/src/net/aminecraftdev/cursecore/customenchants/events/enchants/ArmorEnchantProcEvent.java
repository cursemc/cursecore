package net.aminecraftdev.cursecore.customenchants.events.enchants;

import lombok.Getter;
import lombok.Setter;
import net.aminecraftdev.cursecore.customenchants.events.EnchantProcEvent;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 26-Jun-17
 */
public class ArmorEnchantProcEvent extends EnchantProcEvent {

    @Getter @Setter private double damage, armorDamageMultiplier = 1.0;
    @Getter private LivingEntity target;
    @Getter private boolean pvp;

    public ArmorEnchantProcEvent(Player player, LivingEntity target, double damage, boolean pvp) {
        super(player);

        this.pvp = pvp;
        this.target = target;
        this.damage = damage;
    }
}
