package net.aminecraftdev.cursecore.customenchants.events.potion;

import net.aminecraftdev.cursecore.customenchants.events.ArmorPotionEvent;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class ItemPotionEquipEvent extends ArmorPotionEvent {

    public ItemPotionEquipEvent(Player player, ItemStack itemStack) {
        super(player, itemStack);
    }
}
