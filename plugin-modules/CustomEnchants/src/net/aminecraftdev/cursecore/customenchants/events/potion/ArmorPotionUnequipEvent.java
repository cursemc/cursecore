package net.aminecraftdev.cursecore.customenchants.events.potion;

import net.aminecraftdev.cursecore.customenchants.events.ArmorPotionEvent;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class ArmorPotionUnequipEvent extends ArmorPotionEvent {

    public ArmorPotionUnequipEvent(Player player, ItemStack itemStack) {
        super(player, itemStack);
    }
}
