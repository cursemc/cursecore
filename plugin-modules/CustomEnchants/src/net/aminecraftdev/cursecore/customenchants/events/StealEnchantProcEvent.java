package net.aminecraftdev.cursecore.customenchants.events;

import lombok.Getter;
import lombok.Setter;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class StealEnchantProcEvent extends Event implements Cancellable {

    private static final HandlerList handlers = new HandlerList();

    @Getter @Setter private boolean cancelled = false;
    @Getter private Player player, target;
    @Getter private int level;

    public StealEnchantProcEvent(Player player, Player target, int level) {
        this.player = player;
        this.target = target;
        this.level = level;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }
}