package net.aminecraftdev.cursecore.customenchants.events;

import lombok.Getter;
import lombok.Setter;
import net.aminecraftdev.cursecore.customenchants.entity.wrapper.RarityWrapper;
import net.aminecraftdev.cursecore.customenchants.handlers.CEnchantment;
import net.aminecraftdev.cursecore.customenchants.utils.EnchantmentType;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class EnchantUseEvent extends Event implements Cancellable {

    private static final HandlerList handlers = new HandlerList();

    @Getter @Setter private boolean cancelled = false;
    @Getter private CEnchantment enchantment;
    @Getter private Player player;
    @Getter private int level;

    public EnchantUseEvent(Player player, CEnchantment enchantment, int level) {
        this.player = player;
        this.enchantment = enchantment;
        this.level = level;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public RarityWrapper getRarity() {
        return getEnchantment().getRarityWrapper();
    }

    public EnchantmentType getEnchantmentType() {
        return getEnchantment().getWrapper().getEnchantmentType();
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }
}
