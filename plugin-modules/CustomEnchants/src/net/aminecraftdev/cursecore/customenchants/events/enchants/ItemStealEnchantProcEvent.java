package net.aminecraftdev.cursecore.customenchants.events.enchants;

import lombok.Getter;
import net.aminecraftdev.cursecore.customenchants.events.StealEnchantProcEvent;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class ItemStealEnchantProcEvent extends StealEnchantProcEvent {

    @Getter private ItemStack itemStack;

    public ItemStealEnchantProcEvent(Player player, Player target, int level, ItemStack itemStack) {
        super(player, target, level);

        this.itemStack = itemStack;
    }
}
