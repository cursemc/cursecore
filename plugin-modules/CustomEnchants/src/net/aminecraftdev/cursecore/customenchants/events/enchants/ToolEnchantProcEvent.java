package net.aminecraftdev.cursecore.customenchants.events.enchants;

import lombok.Getter;
import lombok.Setter;
import net.aminecraftdev.cursecore.customenchants.events.EnchantProcEvent;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 26-Jun-17
 */
public class ToolEnchantProcEvent extends EnchantProcEvent {

    @Getter private Map<Block, ItemStack> drops = new HashMap<>();
    @Getter private Map<Block, Integer> expDrop = new HashMap<>();
    @Getter @Setter private boolean smeltOres = false, explodeNearby = false;
    @Getter @Setter private List<Block> blocks;
    @Getter @Setter private int expToDrop;

    public ToolEnchantProcEvent(Player player, List<Block> blocks, int expToDrop) {
        super(player);

        this.blocks = blocks;
        this.expToDrop = expToDrop;
    }

    public void addBlock(Block block) {
        if(blocks.contains(block)) return;

        blocks.add(block);
    }

    public void setDrops(Block block, ItemStack itemStack) {
        if(!this.blocks.contains(block)) return;

        drops.put(block, itemStack);
    }

    public void setExpDrop(Block block, int exp) {
        if(!this.blocks.contains(block)) return;

        this.expDrop.put(block, exp);
    }
}
