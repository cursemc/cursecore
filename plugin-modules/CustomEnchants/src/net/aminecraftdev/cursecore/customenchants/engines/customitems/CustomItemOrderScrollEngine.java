package net.aminecraftdev.cursecore.customenchants.engines.customitems;

import com.massivecraft.massivecore.Engine;
import net.aminecraftdev.cursecore.customenchants.handlers.customitems.CustomItemsBook;
import net.aminecraftdev.cursecore.customenchants.handlers.customitems.CustomItemsOrderScroll;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

public class CustomItemOrderScrollEngine extends Engine {

    private static CustomItemOrderScrollEngine instance = new CustomItemOrderScrollEngine();
    public static CustomItemOrderScrollEngine get() { return instance; }

    @EventHandler
    public void onClick(InventoryClickEvent event) {
        if(event.getCurrentItem() == null || event.getCursor() == null) return;
        if(event.getCurrentItem().getType() == Material.AIR || event.getCursor().getType() == Material.AIR) return;

        ItemStack currentItem = event.getCurrentItem();
        ItemStack cursor = event.getCursor();

        if(!CustomItemsOrderScroll.get().isSimilar(cursor)) return;
        if(!CustomItemsBook.get().isSimilar(currentItem)) return;

        event.setCancelled(true);

        if(cursor.getAmount() > 1) {
            cursor.setAmount(cursor.getAmount() - 1);
        } else {
            event.setCursor(new ItemStack(Material.AIR));
        }

        event.setCurrentItem(CustomItemsOrderScroll.get().handleOrderScroll(currentItem));
    }
}
