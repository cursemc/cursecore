package net.aminecraftdev.cursecore.customenchants.engines.customitems;

import com.massivecraft.massivecore.Engine;
import com.massivecraft.massivecore.util.MUtil;
import net.aminecraftdev.cursecore.customenchants.handlers.customitems.CustomItemsBook;
import net.aminecraftdev.cursecore.customenchants.handlers.customitems.CustomItemsSuccessDust;
import net.aminecraftdev.cursecore.entity.LangConf;
import net.aminecraftdev.cursecore.utils.NumberUtils;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

public class CustomItemSuccessDustEngine extends Engine {

    private static CustomItemSuccessDustEngine instance = new CustomItemSuccessDustEngine();
    public static CustomItemSuccessDustEngine get() { return instance; }

    @EventHandler
    public void onClick(InventoryClickEvent event) {
        if(event.getCurrentItem() == null || event.getCursor() == null) return;
        if(event.getCurrentItem().getType() == Material.AIR || event.getCursor().getType() == Material.AIR) return;

        Player player = (Player) event.getWhoClicked();

        ItemStack currentItem = event.getCurrentItem();
        ItemStack cursor = event.getCursor();

        if(!CustomItemsSuccessDust.get().isSimilar(cursor)) return;
        if(!CustomItemsBook.get().isSimilar(currentItem)) return;

        event.setCancelled(true);

        int bookSuccess = CustomItemsBook.get().getSuccess(currentItem);

        if(bookSuccess >= 100) return;

        int dustSuccess = CustomItemsSuccessDust.get().getSuccessRate(cursor);
        int newSuccess = bookSuccess + dustSuccess;

        if(newSuccess > 100) newSuccess = 100;

        if(cursor.getAmount() > 1) {
            cursor.setAmount(cursor.getAmount() - 1);
        } else {
            event.setCursor(new ItemStack(Material.AIR));
        }

        event.setCurrentItem(CustomItemsBook.get().modifySuccessChance(currentItem, newSuccess));
        LangConf.get().customEnchantsIncreasedSuccess.msg(player, MUtil.map("{chance}", NumberUtils.get().formatNumber(dustSuccess), "{totalChance}", NumberUtils.get().formatNumber(newSuccess)));
    }

}
