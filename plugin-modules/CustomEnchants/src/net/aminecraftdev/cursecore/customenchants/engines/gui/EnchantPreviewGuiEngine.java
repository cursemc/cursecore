package net.aminecraftdev.cursecore.customenchants.engines.gui;

import com.massivecraft.massivecore.Engine;
import net.aminecraftdev.cursecore.customenchants.guis.EnchantPreviewGui;
import net.aminecraftdev.cursecore.storage.PluginStorage;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 05-Aug-19
 */
public class EnchantPreviewGuiEngine extends Engine {

    private static EnchantPreviewGuiEngine instance = new EnchantPreviewGuiEngine();
    public static EnchantPreviewGuiEngine get() { return instance; }

    @EventHandler
    public void onClose(InventoryCloseEvent event) {
        if (event.getInventory() == null) return;
        if (event.getInventory().getHolder() == null) return;
        if (!(event.getInventory().getHolder() instanceof EnchantPreviewGui)) return;

        EnchantPreviewGui enchantPreviewGui = (EnchantPreviewGui) event.getInventory().getHolder();

        Bukkit.getScheduler().runTaskLater(PluginStorage.get(), enchantPreviewGui::openPreviousGui, 2L);
    }

    @EventHandler
    public void onClick(InventoryClickEvent event) {
        if (event.getInventory() == null) return;
        if (event.getInventory().getHolder() == null) return;
        if (!(event.getInventory().getHolder() instanceof EnchantPreviewGui)) return;

        event.setCancelled(true);
    }

}
