package net.aminecraftdev.cursecore.customenchants.engines.gui;

import com.massivecraft.massivecore.Engine;
import com.massivecraft.massivecore.util.MUtil;
import net.aminecraftdev.cursecore.customenchants.engines.EnchantEngine;
import net.aminecraftdev.cursecore.customenchants.entity.EnchantConf;
import net.aminecraftdev.cursecore.customenchants.entity.wrapper.RarityWrapper;
import net.aminecraftdev.cursecore.customenchants.guis.EnchantPreviewGui;
import net.aminecraftdev.cursecore.customenchants.guis.EnchanterGui;
import net.aminecraftdev.cursecore.customenchants.guis.EnchantsGui;
import net.aminecraftdev.cursecore.customenchants.guis.TinkererGui;
import net.aminecraftdev.cursecore.customenchants.handlers.CEnchantment;
import net.aminecraftdev.cursecore.customenchants.handlers.ScrollType;
import net.aminecraftdev.cursecore.customenchants.handlers.customitems.CustomItemsBlackScroll;
import net.aminecraftdev.cursecore.customenchants.handlers.customitems.CustomItemsBook;
import net.aminecraftdev.cursecore.customenchants.handlers.customitems.CustomItemsOrderScroll;
import net.aminecraftdev.cursecore.customenchants.handlers.customitems.CustomItemsShieldScroll;
import net.aminecraftdev.cursecore.entity.LangConf;
import net.aminecraftdev.cursecore.storage.PluginStorage;
import net.aminecraftdev.cursecore.utils.ExpUtils;
import net.aminecraftdev.cursecore.utils.NumberUtils;
import net.aminecraftdev.cursecore.utils.RandomUtils;
import net.aminecraftdev.cursecore.utils.dependencies.VaultHelper;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

import java.util.List;

public class EnchanterGuiEngine extends Engine {

    private static EnchanterGuiEngine instance = new EnchanterGuiEngine();
    public static EnchanterGuiEngine get() { return instance; }


    @EventHandler
    public void onClick(InventoryClickEvent event) {
        if (event.getInventory() == null) return;
        if (event.getInventory().getHolder() == null) return;
        if (!(event.getInventory().getHolder() instanceof EnchanterGui)) return;

        EnchanterGui enchanterGui = (EnchanterGui) event.getInventory().getHolder();
        ItemStack itemStack = event.getCurrentItem();
        Player player = enchanterGui.getPlayer();
        ClickType clickType = event.getClick();
        int rawSlot = event.getRawSlot();

        event.setCancelled(true);

        if (itemStack == null) return;
        if (rawSlot >= enchanterGui.getInventory().getSize()) return;

        String action = enchanterGui.getAction(itemStack);

        switch (action) {
            case "ENCHANTS":
                new EnchantsGui(player).open();
                return;
            case "TINKERER":
                new TinkererGui(player).open();
                return;
            case "ORDER_SCROLL":
                attemptBuyScroll(ScrollType.ORDER, player);
                return;
            case "SHIELD_SCROLL":
                attemptBuyScroll(ScrollType.SHIELD, player);
                return;
            case "BLACK_SCROLL":
                attemptBuyScroll(ScrollType.BLACK, player);
                return;
        }

        action = enchanterGui.getActionSlot(rawSlot);

        RarityWrapper rarityWrapper = EnchantConf.get().getRarityWrapper(action);

        if(rarityWrapper == null) return;

        if(clickType.name().contains("RIGHT")) {
            List<CEnchantment> enchantments = EnchantEngine.get().getEnchantments(rarityWrapper);

            new EnchantPreviewGui(player, enchanterGui, enchantments, rarityWrapper.getFormattedName()).open();
            return;
        }

        int cost = rarityWrapper.getCost();
        int exp = ExpUtils.get().getTotalExperience(player);

        if(exp < cost) {
            LangConf.get().customEnchantsNotEnoughExp.msgError(player);
            return;
        }

        if(player.getInventory().firstEmpty() == -1) {
            LangConf.get().inventoryFull.msgError(player);
            return;
        }

        ExpUtils.get().setTotalExperience(player, (exp - cost));
        LangConf.get().customEnchantsBought.msg(player, MUtil.map("{rarity}", rarityWrapper.getName(), "{cost}", NumberUtils.get().formatNumber(rarityWrapper.getCost())));

        CEnchantment enchantment = EnchantEngine.get().getRandomEnchant(rarityWrapper);
        int randomLevel = RandomUtils.get().getRandomNumber(enchantment.getWrapper().getMaxLevel())+1;

        player.getInventory().addItem(CustomItemsBook.get().getCompleteItem(1, enchantment, randomLevel));
    }

    private void attemptBuyScroll(ScrollType scrollType, Player player) {
        double balance = VaultHelper.get().getBalance(player);
        double cost = 0;

        switch (scrollType) {
            case ORDER:
                cost = EnchantConf.get().orderScrollCost;
                break;
            case BLACK:
                cost = EnchantConf.get().blackScrollCost;
                break;
            case SHIELD:
                cost = EnchantConf.get().shieldScrollCost;
                break;
        }

        if(balance < cost) {
            LangConf.get().insufficientFunds.msgError(player, MUtil.map("{amount}", NumberUtils.get().formatNumber(cost - balance)));
            return;
        }

        ItemStack itemStack = new ItemStack(Material.AIR);

        switch (scrollType) {
            case ORDER:
                itemStack = CustomItemsOrderScroll.get().getCompleteItem(1);
                break;
            case BLACK:
                itemStack = CustomItemsBlackScroll.get().getCompleteItem(1);
                break;
            case SHIELD:
                itemStack = CustomItemsShieldScroll.get().getCompleteItem(1);
                break;
        }


        if(player.getInventory().firstEmpty() == -1) {
            player.getWorld().dropItemNaturally(player.getLocation(), itemStack);
        } else {
            player.getInventory().addItem(itemStack);
        }

        VaultHelper.get().withdrawBalance(player, cost);
        LangConf.get().customEnchantsItemPurchased.msg(player, MUtil.map("{scroll}", scrollType.name(), "{cost}", NumberUtils.get().formatNumber(cost)));
    }

}
