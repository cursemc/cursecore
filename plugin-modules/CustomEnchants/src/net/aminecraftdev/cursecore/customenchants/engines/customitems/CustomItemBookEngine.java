package net.aminecraftdev.cursecore.customenchants.engines.customitems;

import com.massivecraft.massivecore.Engine;
import com.massivecraft.massivecore.util.MUtil;
import net.aminecraftdev.cursecore.customenchants.handlers.CEnchantment;
import net.aminecraftdev.cursecore.customenchants.handlers.customitems.CustomItemsBook;
import net.aminecraftdev.cursecore.customenchants.handlers.customitems.CustomItemsShieldScroll;
import net.aminecraftdev.cursecore.entity.LangConf;
import net.aminecraftdev.cursecore.utils.RandomUtils;
import net.aminecraftdev.cursecore.utils.RomanNumberUtils;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

public class CustomItemBookEngine extends Engine {

    private static CustomItemBookEngine instance = new CustomItemBookEngine();
    public static CustomItemBookEngine get() { return instance; }

    @EventHandler(ignoreCancelled = true)
    public void onClick(InventoryClickEvent event) {
        if(event.getCurrentItem() == null || event.getCursor() == null) return;
        if(event.getCurrentItem().getType() == Material.AIR || event.getCursor().getType() == Material.AIR) return;

        Player player = (Player) event.getWhoClicked();

        ItemStack currentItem = event.getCurrentItem();
        ItemStack cursor = event.getCursor();

        if(!CustomItemsBook.get().isSimilar(cursor)) return;

        CEnchantment enchantment = CustomItemsBook.get().getEnchant(cursor);
        int level = CustomItemsBook.get().getLevel(cursor);

        event.setCancelled(true);

        if(!CustomItemsBook.get().canApply(currentItem, enchantment)) return;

        if(enchantment.hasEnchant(currentItem)) {
            LangConf.get().customEnchantsAlreadyHasEnchant.msgError(player);
            return;
        }

        int successRate = CustomItemsBook.get().getSuccess(cursor);
        int destroyRate = CustomItemsBook.get().getDestroy(cursor);
        boolean isProtected = CustomItemsShieldScroll.get().isProtected(currentItem);

        if(canExecute(successRate)) {
            event.setCurrentItem(enchantment.addToItem(currentItem, level));

            LangConf.get().customEnchantsAddedEnchant.msg(player, MUtil.map(
                    "{enchant}", enchantment.getFormattedName(),
                    "{level}", RomanNumberUtils.get().getRomanNumber(level)
            ));
        } else {
            if(canExecute(destroyRate)) {
                if(isProtected) {
                    event.setCurrentItem(CustomItemsShieldScroll.get().removeProtection(currentItem));
                    LangConf.get().customEnchantsFailedProtected.msgError(player);
                } else {
                    event.setCurrentItem(new ItemStack(Material.AIR));
                    LangConf.get().customEnchantsFailedDestroyed.msgError(player);
                }
            } else {
                LangConf.get().customEnchantsFailedNotDestroy.msgError(player);
            }
        }

        if(cursor.getAmount() > 1) {
            cursor.setAmount(cursor.getAmount() - 1);
        } else {
            event.setCursor(new ItemStack(Material.AIR));
        }

        player.updateInventory();
    }

    private boolean canExecute(double rate) {
        if(rate <= 0) return false;

        return RandomUtils.get().getRandomBetweenHundred() <= rate;
    }
}
