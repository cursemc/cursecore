package net.aminecraftdev.cursecore.customenchants.engines.customitems;

import com.massivecraft.massivecore.Engine;
import com.massivecraft.massivecore.util.MUtil;
import net.aminecraftdev.cursecore.customenchants.entity.EnchantConf;
import net.aminecraftdev.cursecore.customenchants.handlers.DustType;
import net.aminecraftdev.cursecore.customenchants.handlers.customitems.CustomItemsDestroyDust;
import net.aminecraftdev.cursecore.customenchants.handlers.customitems.CustomItemsMysteryDust;
import net.aminecraftdev.cursecore.customenchants.handlers.customitems.CustomItemsSuccessDust;
import net.aminecraftdev.cursecore.entity.LangConf;
import net.aminecraftdev.cursecore.utils.RandomUtils;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

public class CustomItemMysteryDustEngine extends Engine {

    private static CustomItemMysteryDustEngine instance = new CustomItemMysteryDustEngine();
    public static CustomItemMysteryDustEngine get() { return instance; }

    @EventHandler
    public void onInteract(PlayerInteractEvent event) {
        if(!event.getAction().name().startsWith("RIGHT_CLICK")) return;

        ItemStack itemStack = event.getItem();

        if(itemStack == null || itemStack.getType() == Material.AIR) return;
        if(!CustomItemsMysteryDust.get().isSimilar(itemStack)) return;

        event.setCancelled(true);
        event.setUseItemInHand(Event.Result.DENY);

        Player player = event.getPlayer();

        if(itemStack.getAmount() > 1) {
            itemStack.setAmount(itemStack.getAmount() - 1);
        } else {
            player.setItemInHand(new ItemStack(Material.AIR));
        }

        int random = RandomUtils.get().getRandomBetweenHundred();
        DustType dustType = null;

        if(random < 33) {
            int randomPercent = RandomUtils.get().getRandomBetweenHundred();

            player.getInventory().addItem(CustomItemsDestroyDust.get().getCompleteItem(1, randomPercent));
            dustType = DustType.DESTROY;
        } else if(random < 66) {
            int randomPercent = RandomUtils.get().getRandomBetweenHundred();

            player.getInventory().addItem(CustomItemsSuccessDust.get().getCompleteItem(1, randomPercent));
            dustType = DustType.SUCCESS;
        } else {
            player.getInventory().addItem(EnchantConf.get().failedDust.toItemStack());
        }

        LangConf.get().customEnchantsDustFound.msg(player, MUtil.map("{dustType}", dustType != null? dustType.name() : "Failed"));
    }


}
