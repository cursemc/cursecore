package net.aminecraftdev.cursecore.customenchants.engines;

import com.massivecraft.massivecore.Engine;
import net.aminecraftdev.cursecore.customenchants.entity.wrapper.RarityWrapper;
import net.aminecraftdev.cursecore.customenchants.events.ArmorEquipEvent;
import net.aminecraftdev.cursecore.customenchants.events.enchants.*;
import net.aminecraftdev.cursecore.customenchants.events.potion.ArmorPotionEquipEvent;
import net.aminecraftdev.cursecore.customenchants.events.potion.ArmorPotionUnequipEvent;
import net.aminecraftdev.cursecore.customenchants.events.potion.ItemPotionEquipEvent;
import net.aminecraftdev.cursecore.customenchants.events.potion.ItemPotionUnequipEvent;
import net.aminecraftdev.cursecore.customenchants.handlers.CEnchantment;
import net.aminecraftdev.cursecore.customenchants.utils.EnchantmentType;
import net.aminecraftdev.cursecore.storage.PluginStorage;
import net.aminecraftdev.cursecore.utils.RandomUtils;
import net.aminecraftdev.cursecore.utils.RomanNumberUtils;
import net.aminecraftdev.cursecore.utils.StringUtils;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerItemHeldEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.*;
import java.util.stream.Collectors;

public class EnchantEngine extends Engine {

    private static EnchantEngine instance = new EnchantEngine();
    public static EnchantEngine get() { return instance; }

    private static final Map<String, CEnchantment> ENCHANTMENT_MAP = new HashMap<>();

    public Collection<CEnchantment> getEnchantments() {
        return new ArrayList<>(ENCHANTMENT_MAP.values());
    }

    public Map<CEnchantment, Integer> getCurrentEnchantments(ItemStack input) {
        Map<CEnchantment, Integer> enchants = new HashMap<>();

        if(input == null) return enchants;

        ItemMeta itemMeta = input.getItemMeta();

        if(itemMeta == null) return enchants;
        if(!itemMeta.hasLore()) return enchants;

        List<String> lore = itemMeta.getLore();

        for(String line : lore) {
            if(!line.contains(" ")) continue;

            String stripped = ChatColor.stripColor(StringUtils.get().substringBeforeLast(line, " "));

            if(stripped == null) continue;

            int level = RomanNumberUtils.get().getValueOf(StringUtils.get().substringBeforeLast(line, " "));

            if(level <= 0) continue;

            CEnchantment enchantment = getByName(stripped);

            if(enchantment != null && enchantment.getWrapper().isEnabled()) {
                enchants.put(enchantment, level);
            }
        }

        return enchants;
    }

    public CEnchantment getByName(String enchantment) {
        return ENCHANTMENT_MAP.getOrDefault(enchantment, null);
    }

    public void registerCustomEnchant(CEnchantment enchantment) {
        if(ENCHANTMENT_MAP.containsKey(enchantment.getName())) {
            throw new IllegalArgumentException("Cannot override an already existing enchantment with name '" + enchantment.getName() + "'.");
        } else {
            ENCHANTMENT_MAP.put(enchantment.getName(), enchantment);
        }
    }

    public void unregisterCustomEnchant(String enchantment) {
        ENCHANTMENT_MAP.remove(enchantment);
    }

    public boolean hasEnchants(ItemStack itemStack) {
        return !getCurrentEnchantments(itemStack).isEmpty();
    }

    public int getEnchantsAmount(ItemStack itemStack) {
        return getCurrentEnchantments(itemStack).size();
    }

    public List<CEnchantment> getEnchantments(RarityWrapper rarityWrapper) {
        return getEnchantments().stream().filter(CEnchantment::isEnabled).filter(enchant -> enchant.getWrapper().getRarity().equals(rarityWrapper.getName())).collect(Collectors.toList());
    }

    public List<CEnchantment> getEnchantments(EnchantmentType enchantmentType) {
        return getEnchantments().stream().filter(CEnchantment::isEnabled).filter(enchant -> enchant.getEnchantType() == enchantmentType).collect(Collectors.toList());
    }

    public CEnchantment getRandomEnchant(EnchantmentType enchantmentType) {
        List<CEnchantment> enchantments = getEnchantments(enchantmentType);
        int random = RandomUtils.get().getRandomNumber(enchantments.size());

        return enchantments.get(random);
    }

    public CEnchantment getRandomEnchant(RarityWrapper rarityWrapper) {
        List<CEnchantment> enchantments = getEnchantments(rarityWrapper);
        int random = RandomUtils.get().getRandomNumber(enchantments.size());

        return enchantments.get(random);
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    protected void onKillEnchant(EntityDeathEvent event) {
        LivingEntity livingEntity = event.getEntity();

        if(livingEntity.getKiller() == null) return;

        Player player = livingEntity.getKiller();

        KillEnchantProcEvent enchantProcEvent = new KillEnchantProcEvent(player, livingEntity, livingEntity instanceof Player, event.getDroppedExp(), event.getDrops());

        Bukkit.getPluginManager().callEvent(enchantProcEvent);

        event.setDroppedExp(enchantProcEvent.getDroppedExp());
        event.getDrops().clear();
        event.getDrops().addAll(enchantProcEvent.getDrops());
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    protected void onWeaponEnchant(EntityDamageByEntityEvent event) {
        if(!(event.getDamager() instanceof Player)) return;
        if(!(event.getEntity() instanceof LivingEntity)) return;

        Player player = (Player) event.getDamager();
        LivingEntity livingEntity = (LivingEntity) event.getEntity();
        double damage = event.getDamage();

        WeaponEnchantProcEvent enchantProcEvent = new WeaponEnchantProcEvent(player, livingEntity, damage, event.getEntity() instanceof Player);

        Bukkit.getPluginManager().callEvent(enchantProcEvent);

        event.setCancelled(enchantProcEvent.isCancelled());
        event.setDamage(enchantProcEvent.getDamage());
        event.setDamage(EntityDamageEvent.DamageModifier.ARMOR, event.getDamage(EntityDamageEvent.DamageModifier.ARMOR) * enchantProcEvent.getArmorDamageMultiplier());
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    protected void onArmourEnchant(EntityDamageByEntityEvent event) {
        if(!(event.getEntity() instanceof Player)) return;
        if(!(event.getDamager() instanceof LivingEntity)) return;

        Player player = (Player) event.getEntity();
        LivingEntity livingEntity = (LivingEntity) event.getDamager();
        double damage = event.getDamage();

        ArmorEnchantProcEvent enchantProcEvent = new ArmorEnchantProcEvent(player, livingEntity, damage, event.getDamager() instanceof Player);

        Bukkit.getPluginManager().callEvent(enchantProcEvent);

        event.setCancelled(enchantProcEvent.isCancelled());
        event.setDamage(enchantProcEvent.getDamage());
        event.setDamage(EntityDamageEvent.DamageModifier.ARMOR, event.getDamage(EntityDamageEvent.DamageModifier.ARMOR) * enchantProcEvent.getArmorDamageMultiplier());
    }


    @EventHandler(priority = EventPriority.MONITOR)
    protected void onToolEnchant(BlockBreakEvent event) {
        if(event.isCancelled()) return;

        Player player = event.getPlayer();
        List<Block> blocks = new ArrayList<>();
        int expToDrop = event.getExpToDrop();
        ItemStack itemStack = player.getItemInHand();

        if(player.getGameMode() == GameMode.CREATIVE) return;
        if(!isTool(itemStack)) return;

        blocks.add(event.getBlock());

        ToolEnchantProcEvent toolEnchantProcEvent = new ToolEnchantProcEvent(player, blocks, expToDrop);

        Bukkit.getPluginManager().callEvent(toolEnchantProcEvent);

        event.setCancelled(toolEnchantProcEvent.isCancelled());
        event.setExpToDrop(toolEnchantProcEvent.getExpToDrop());

        if(toolEnchantProcEvent.isCancelled()) return;

        event.setCancelled(true);

        for(Block block : toolEnchantProcEvent.getBlocks()) {
            if(toolEnchantProcEvent.getDrops().containsKey(block)) {
                if(toolEnchantProcEvent.getExpDrop().containsKey(block)) {
                    ((ExperienceOrb) (block.getWorld().spawnEntity(block.getLocation(), EntityType.EXPERIENCE_ORB))).setExperience(toolEnchantProcEvent.getExpDrop().get(block));
                }

                block.setType(Material.AIR);

                block.getWorld().playEffect(block.getLocation(), Effect.STEP_SOUND, block.getType());
                block.getWorld().dropItemNaturally(block.getLocation(), toolEnchantProcEvent.getDrops().get(block));
            } else {
                block.breakNaturally();
            }

            if(canHurtItem(itemStack)) {
                if(itemStack.getDurability() == itemStack.getType().getMaxDurability()) {
                    player.setItemInHand(new ItemStack(Material.AIR));
                    break;
                } else {
                    itemStack.setDurability((short) (itemStack.getDurability() + 1));
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    protected void onBowEnchant(EntityDamageByEntityEvent event) {
        if(event.isCancelled()) return;
        if(event.getCause() != EntityDamageEvent.DamageCause.PROJECTILE) return;
        if(!(event.getEntity() instanceof LivingEntity)) return;

        Projectile projectile = (Projectile) event.getDamager();
        LivingEntity shooter = (LivingEntity) projectile.getShooter();

        if(!(shooter instanceof Player)) return;

        Player player = (Player) shooter;
        LivingEntity target = (LivingEntity) event.getEntity();
        double damage = event.getDamage();

        BowEnchantProcEvent bowEnchantProcEvent = new BowEnchantProcEvent(player, target, damage, projectile, target instanceof Player);

        Bukkit.getPluginManager().callEvent(bowEnchantProcEvent);

        event.setCancelled(bowEnchantProcEvent.isCancelled());
        event.setDamage(bowEnchantProcEvent.getDamage());
    }

    @EventHandler(ignoreCancelled = true)
    public void onEquip(ArmorEquipEvent event) {
        Player player = event.getPlayer();
        ItemStack newItem = event.getNewArmorPiece(), oldItem = event.getOldArmorPiece();

        if(hasEnchants(oldItem)) {
            ArmorPotionUnequipEvent armorPotionEvent = new ArmorPotionUnequipEvent(player, oldItem);

            Bukkit.getPluginManager().callEvent(armorPotionEvent);

            event.setCancelled(armorPotionEvent.isCancelled());
        }

        if(event.isCancelled()) return;

        if(hasEnchants(newItem)) {
            ArmorPotionEquipEvent armorPotionEvent = new ArmorPotionEquipEvent(player, newItem);

            Bukkit.getPluginManager().callEvent(armorPotionEvent);

            event.setCancelled(armorPotionEvent.isCancelled());
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onClick(InventoryClickEvent event) {
        Player player = (Player) event.getWhoClicked();
        ItemStack itemInHand = player.getItemInHand();
        ItemStack currentItem = event.getCurrentItem();
        ItemStack cursorItem = event.getCursor();

        if(hasEnchants(currentItem)) {
            if(itemInHand.equals(currentItem)) {
                PluginStorage.get().log("ItemPotionUnequipEvent called");
                ItemPotionUnequipEvent itemPotionEvent = new ItemPotionUnequipEvent(player, currentItem);

                Bukkit.getPluginManager().callEvent(itemPotionEvent);
                event.setCancelled(itemPotionEvent.isCancelled());
            }
        }

        if(event.isCancelled()) return;

        if(hasEnchants(cursorItem)) {
            PluginStorage.get().log("ItemPotionEquipEvent called");
            ItemPotionEquipEvent itemPotionEvent = new ItemPotionEquipEvent(player, cursorItem);

            Bukkit.getPluginManager().callEvent(itemPotionEvent);
            event.setCancelled(itemPotionEvent.isCancelled());
        }
    }

    @EventHandler(ignoreCancelled = true)
    public void onToggle(PlayerItemHeldEvent event) {
        Player player = event.getPlayer();
        int newSlot = event.getNewSlot(), oldSlot = event.getPreviousSlot();
        ItemStack newItem = player.getInventory().getItem(newSlot), oldItem = player.getInventory().getItem(oldSlot);

        if(hasEnchants(oldItem)) {
            PluginStorage.get().log("ItemPotionUnequipEvent called");
            ItemPotionUnequipEvent itemPotionEvent = new ItemPotionUnequipEvent(player, oldItem);

            Bukkit.getPluginManager().callEvent(itemPotionEvent);
            event.setCancelled(itemPotionEvent.isCancelled());
        }

        if(event.isCancelled()) return;

        if(hasEnchants(newItem)) {
            PluginStorage.get().log("ItemPotionEquipEvent called");
            ItemPotionEquipEvent itemPotionEvent = new ItemPotionEquipEvent(player, newItem);

            Bukkit.getPluginManager().callEvent(itemPotionEvent);
            event.setCancelled(itemPotionEvent.isCancelled());
        }
    }

    private boolean isTool(ItemStack itemStack) {
        return EnchantmentType.Tool.doesMaterialEndWith(itemStack.getType());
    }

    private boolean canHurtItem(ItemStack itemStack) {
        if(!itemStack.getItemMeta().hasEnchant(Enchantment.DURABILITY)) return true;

        int level = itemStack.getEnchantmentLevel(Enchantment.DURABILITY);
        int randomNumber = RandomUtils.get().getRandomBetweenHundred();

        return randomNumber < (100 - (10 * level));
    }

}
