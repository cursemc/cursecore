package net.aminecraftdev.cursecore.customenchants.engines.customitems;

import com.massivecraft.massivecore.Engine;
import net.aminecraftdev.cursecore.customenchants.handlers.customitems.CustomItemsShieldScroll;
import net.aminecraftdev.cursecore.customenchants.utils.EnchantmentType;
import net.aminecraftdev.cursecore.entity.LangConf;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

public class CustomItemShieldScrollEngine extends Engine {

    private static CustomItemShieldScrollEngine instance = new CustomItemShieldScrollEngine();
    public static CustomItemShieldScrollEngine get() { return instance; }

    @EventHandler(ignoreCancelled = true)
    public void onClick(InventoryClickEvent event) {
        if(event.getCurrentItem() == null || event.getCursor() == null) return;
        if(event.getCurrentItem().getType() == Material.AIR || event.getCursor().getType() == Material.AIR) return;

        Player player = (Player) event.getWhoClicked();

        ItemStack currentItem = event.getCurrentItem();
        ItemStack cursor = event.getCursor();

        if(!CustomItemsShieldScroll.get().isSimilar(cursor)) return;
        if(CustomItemsShieldScroll.get().isProtected(currentItem)) return;
        if(EnchantmentType.getType(currentItem) == null) return;

        event.setCancelled(true);

        if(cursor.getAmount() > 1) {
            cursor.setAmount(cursor.getAmount() - 1);
        } else {
            event.setCursor(new ItemStack(Material.AIR));
        }

        CustomItemsShieldScroll.get().addProtection(currentItem);
        LangConf.get().customEnchantsShieldApplied.msg(player);
    }

    @EventHandler(priority = EventPriority.HIGH, ignoreCancelled = true)
    public void onInteract(PlayerInteractEvent event) {
        if(!event.getAction().name().startsWith("RIGHT_CLICK")) return;

        Player player = event.getPlayer();
        ItemStack itemStack = player.getItemInHand();

        if(itemStack == null || itemStack.getType() == Material.AIR || !CustomItemsShieldScroll.get().isSimilar(itemStack)) return;

        event.setUseItemInHand(Event.Result.DENY);
        event.setCancelled(true);
    }

}
