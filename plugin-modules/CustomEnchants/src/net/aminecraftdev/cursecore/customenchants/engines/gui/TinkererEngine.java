package net.aminecraftdev.cursecore.customenchants.engines.gui;

import com.massivecraft.massivecore.Engine;
import net.aminecraftdev.cursecore.customenchants.engines.EnchantEngine;
import net.aminecraftdev.cursecore.customenchants.entity.EnchantInventoryConf;
import net.aminecraftdev.cursecore.customenchants.guis.TinkererGui;
import net.aminecraftdev.cursecore.customenchants.handlers.customitems.CustomItemsBook;
import net.aminecraftdev.cursecore.customenchants.handlers.customitems.CustomItemsMysteryDust;
import net.aminecraftdev.cursecore.entity.LangConf;
import net.aminecraftdev.cursecore.handlers.Gui;
import net.aminecraftdev.cursecore.storage.PluginStorage;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.Map;

public class TinkererEngine extends Engine {

    private static TinkererEngine instance = new TinkererEngine();
    public static TinkererEngine get() { return instance; }

    @EventHandler
    public void onClose(InventoryCloseEvent event) {
        if (event.getInventory() == null) return;
        if (event.getInventory().getHolder() == null) return;
        if (!(event.getInventory().getHolder() instanceof TinkererGui)) return;

        TinkererGui tinkererGui = (TinkererGui) event.getInventory().getHolder();

        if(tinkererGui.isClosed()) return;

        tinkererGui.cancel();
    }

    @EventHandler
    public void onClick(InventoryClickEvent event) {
        if (event.getInventory() == null) return;
        if (event.getInventory().getHolder() == null) return;
        if (!(event.getInventory().getHolder() instanceof TinkererGui)) return;

        TinkererGui tinkererGui = (TinkererGui) event.getInventory().getHolder();

        event.setCancelled(true);

        ItemStack itemStack = event.getCurrentItem();

        if(itemStack == null || itemStack.getType() == Material.AIR) return;

        int rawSlot = event.getRawSlot();
        String action = tinkererGui.getAction(itemStack);

        if(action.equals("CONFIRM")) {
            handleConfirmButton(tinkererGui);
            return;
        }

        if(rawSlot >= tinkererGui.getInventory().getSize()) {
            event.setCancelled(false);
            handleTinkererAddition(tinkererGui, itemStack, event);
        } else {
            handleReturnItem(tinkererGui, rawSlot);
        }
    }

    private void handleConfirmButton(TinkererGui tinkererGui) {
        Map<Integer, ItemStack> rewards = tinkererGui.getRewardSlotMap();
        Inventory inventory = tinkererGui.getInventory();
        Player player = tinkererGui.getPlayer();

        rewards.keySet().forEach(slot -> inventory.setItem(slot, new ItemStack(Material.AIR)));
        rewards.values().forEach(reward -> player.getInventory().addItem(reward.clone()));
        tinkererGui.resetRewardSlot();
        tinkererGui.resetGivenSlotMap();
        player.updateInventory();

        Bukkit.getScheduler().runTaskLater(PluginStorage.get(), () -> {
            tinkererGui.close(true);
            LangConf.get().customEnchantsTinkererAccept.msg(player);
        }, 1L);
    }

    private void handleTinkererAddition(TinkererGui tinkererGui, ItemStack itemStack, InventoryClickEvent event) {
        Player player = tinkererGui.getPlayer();

        if(EnchantEngine.get().getCurrentEnchantments(itemStack).isEmpty() && !CustomItemsBook.get().isSimilar(itemStack)) {
            LangConf.get().customEnchantsDoesntAccept.msgError(player);
            return;
        }

        Integer firstFree = firstFree(EnchantInventoryConf.get().tinkererUserSlots, tinkererGui);

        if(firstFree == null) return;

        tinkererGui.addGivenItem(firstFree, itemStack);
        event.setCurrentItem(new ItemStack(Material.AIR));

        ItemStack magicDust = CustomItemsMysteryDust.get().getCompleteItem(1);


        if(firstFree < 9) {
            tinkererGui.addRewardItem(firstFree+4, magicDust);
        } else {
            tinkererGui.addRewardItem(firstFree+5, magicDust);
        }
    }

    private void handleReturnItem(TinkererGui tinkererGui, int slot) {
        if(contains(EnchantInventoryConf.get().tinkererUserSlots, slot)) {
            if(!tinkererGui.getGivenSlotMap().containsKey(slot)) return;

            ItemStack itemStack = tinkererGui.getGivenItem(slot);

            tinkererGui.getPlayer().getInventory().addItem(itemStack);
            tinkererGui.removeGivenItem(slot);

            if(slot < 9) {
                tinkererGui.removeRewardItem(slot+4);
            } else {
                tinkererGui.removeRewardItem(slot+5);
            }
        }
    }

    private boolean contains(int[] array, int slot) {
        for(int i : array) {
            if(i == slot) return true;
        }

        return false;
    }

    private Integer firstFree(int[] array, Gui gui) {
        for(int i : array) {
            if(gui.getInventory().getItem(i) == null) return i;
        }

        return null;
    }

}
