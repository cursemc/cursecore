package net.aminecraftdev.cursecore.customenchants.engines.customitems;

import com.massivecraft.massivecore.Engine;
import net.aminecraftdev.cursecore.customenchants.engines.EnchantEngine;
import net.aminecraftdev.cursecore.customenchants.handlers.customitems.CustomItemsBlackScroll;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

public class CustomItemBlackScrollEngine extends Engine {

    private static CustomItemBlackScrollEngine instance = new CustomItemBlackScrollEngine();
    public static CustomItemBlackScrollEngine get() { return instance; }

    @EventHandler(ignoreCancelled = true)
    public void onClick(InventoryClickEvent event) {
        if (event.getCurrentItem() == null || event.getCursor() == null) return;
        if (event.getCurrentItem().getType() == Material.AIR || event.getCursor().getType() == Material.AIR) return;

        Player player = (Player) event.getWhoClicked();

        ItemStack currentItem = event.getCurrentItem();
        ItemStack cursor = event.getCursor();

        if (!CustomItemsBlackScroll.get().isSimilar(cursor)) return;
        if (EnchantEngine.get().getCurrentEnchantments(currentItem).isEmpty()) return;

        event.setCancelled(true);

        if (cursor.getAmount() > 1) {
            cursor.setAmount(cursor.getAmount() - 1);
        } else {
            event.setCursor(new ItemStack(Material.AIR));
        }

        event.setCurrentItem(CustomItemsBlackScroll.get().handleBlackScroll(player, currentItem));
    }

}
