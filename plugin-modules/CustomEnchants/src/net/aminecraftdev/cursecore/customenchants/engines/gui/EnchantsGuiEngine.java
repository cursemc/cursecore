package net.aminecraftdev.cursecore.customenchants.engines.gui;

import com.massivecraft.massivecore.Engine;
import net.aminecraftdev.cursecore.customenchants.engines.EnchantEngine;
import net.aminecraftdev.cursecore.customenchants.guis.EnchantPreviewGui;
import net.aminecraftdev.cursecore.customenchants.guis.EnchantsGui;
import net.aminecraftdev.cursecore.customenchants.utils.EnchantmentType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

public class EnchantsGuiEngine extends Engine {

    private static EnchantsGuiEngine instance = new EnchantsGuiEngine();
    public static EnchantsGuiEngine get() { return instance; }

    @EventHandler
    public void onClick(InventoryClickEvent event) {
        if (event.getInventory() == null) return;
        if (event.getInventory().getHolder() == null) return;
        if (!(event.getInventory().getHolder() instanceof EnchantsGui)) return;

        EnchantsGui enchantsGui = (EnchantsGui) event.getInventory().getHolder();
        Player player = enchantsGui.getPlayer();

        event.setCancelled(true);

        ItemStack itemStack = event.getCurrentItem();

        if (itemStack == null) return;

        String action = enchantsGui.getAction(itemStack);

        if (action.equals("FILLER")) return;

        EnchantmentType enchantmentType = EnchantmentType.getByName(action);

        if(enchantmentType == null) return;

        new EnchantPreviewGui(player, enchantsGui, EnchantEngine.get().getEnchantments(enchantmentType), enchantmentType.name()).open();
    }

}
