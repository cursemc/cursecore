package net.aminecraftdev.cursecore.customenchants.engines.customitems;

import com.massivecraft.massivecore.Engine;
import com.massivecraft.massivecore.util.MUtil;
import net.aminecraftdev.cursecore.customenchants.handlers.customitems.CustomItemsBook;
import net.aminecraftdev.cursecore.customenchants.handlers.customitems.CustomItemsDestroyDust;
import net.aminecraftdev.cursecore.entity.LangConf;
import net.aminecraftdev.cursecore.utils.NumberUtils;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

public class CustomItemDestroyDustEngine extends Engine {

    private static CustomItemDestroyDustEngine instance = new CustomItemDestroyDustEngine();
    public static CustomItemDestroyDustEngine get() { return instance; }

    @EventHandler
    public void onClick(InventoryClickEvent event) {
        if(event.getCurrentItem() == null || event.getCursor() == null) return;
        if(event.getCurrentItem().getType() == Material.AIR || event.getCursor().getType() == Material.AIR) return;

        Player player = (Player) event.getWhoClicked();

        ItemStack currentItem = event.getCurrentItem();
        ItemStack cursor = event.getCursor();

        if(!CustomItemsDestroyDust.get().isSimilar(cursor)) return;
        if(!CustomItemsBook.get().isSimilar(currentItem)) return;

        event.setCancelled(true);

        int bookDestroy = CustomItemsBook.get().getDestroy(currentItem);

        if(bookDestroy <= 0) return;

        int dustDestroy = CustomItemsDestroyDust.get().getDestroyRate(cursor);
        int newDestroy = bookDestroy - dustDestroy;

        if(newDestroy < 0) newDestroy = 0;

        if(cursor.getAmount() > 1) {
            cursor.setAmount(cursor.getAmount() - 1);
        } else {
            event.setCursor(new ItemStack(Material.AIR));
        }

        event.setCurrentItem(CustomItemsBook.get().modifyDestroyChance(currentItem, newDestroy));
        LangConf.get().customEnchantsDecreasedDestroy.msg(player, MUtil.map("{chance}", NumberUtils.get().formatNumber(dustDestroy), "{totalChance}", NumberUtils.get().formatNumber(newDestroy)));
    }
}
