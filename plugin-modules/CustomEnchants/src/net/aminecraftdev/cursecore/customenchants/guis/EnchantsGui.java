package net.aminecraftdev.cursecore.customenchants.guis;

import net.aminecraftdev.cursecore.customenchants.entity.EnchantInventoryConf;
import net.aminecraftdev.cursecore.handlers.Gui;
import org.bukkit.entity.Player;

import java.util.Map;

public class EnchantsGui extends Gui {

    public EnchantsGui(Player player) {
        super(player, EnchantInventoryConf.get().enchantsGui);
    }

    @Override
    public void init() {

    }

    @Override
    public Map<String, String> getReplaceMap() {
        return null;
    }
}
