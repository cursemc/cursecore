package net.aminecraftdev.cursecore.customenchants.guis;

import net.aminecraftdev.cursecore.customenchants.entity.EnchantInventoryConf;
import net.aminecraftdev.cursecore.handlers.Gui;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;
import java.util.Map;

public class TinkererGui extends Gui {

    private Map<Integer, ItemStack> rewardSlotMap = new HashMap<>();
    private Map<Integer, ItemStack> givenSlotMap = new HashMap<>();

    public TinkererGui(Player player) {
        super(player, EnchantInventoryConf.get().tinkererGui);
    }

    @Override
    public void init() {

    }
    @Override
    public Map<String, String> getReplaceMap() {
        return null;
    }

    public void cancel() {
        getGivenSlotMap().values().forEach(itemStack -> getPlayer().getInventory().addItem(itemStack));
    }

    public void addGivenItem(int slot, ItemStack itemStack) {
        this.givenSlotMap.put(slot, itemStack);
        getInventory().setItem(slot, itemStack);
    }

    public void addRewardItem(int slot, ItemStack itemStack) {
        this.rewardSlotMap.put(slot, itemStack);
        getInventory().setItem(slot, itemStack);
    }

    public Map<Integer, ItemStack> getRewardSlotMap() {
        return new HashMap<>(this.rewardSlotMap);
    }

    public Map<Integer, ItemStack> getGivenSlotMap() {
        return new HashMap<>(this.givenSlotMap);
    }

    public void resetGivenSlotMap() {
        this.givenSlotMap.clear();
    }

    public void resetRewardSlot() {
        this.rewardSlotMap.clear();
    }

    public ItemStack getGivenItem(int slot) {
        return this.givenSlotMap.getOrDefault(slot, new ItemStack(Material.AIR));
    }

    public ItemStack getRewardItem(int slot) {
        return this.rewardSlotMap.getOrDefault(slot, new ItemStack(Material.AIR));
    }

    public void removeGivenItem(int slot) {
        this.givenSlotMap.remove(slot);
        getInventory().setItem(slot, new ItemStack(Material.AIR));
    }

    public void removeRewardItem(int slot) {
        this.rewardSlotMap.remove(slot);
        getInventory().setItem(slot, new ItemStack(Material.AIR));
    }

}
