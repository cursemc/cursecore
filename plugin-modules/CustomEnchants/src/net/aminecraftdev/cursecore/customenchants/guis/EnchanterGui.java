package net.aminecraftdev.cursecore.customenchants.guis;

import com.massivecraft.massivecore.util.MUtil;
import net.aminecraftdev.cursecore.customenchants.entity.EnchantConf;
import net.aminecraftdev.cursecore.customenchants.entity.EnchantInventoryConf;
import net.aminecraftdev.cursecore.customenchants.entity.wrapper.RarityWrapper;
import net.aminecraftdev.cursecore.handlers.Gui;
import net.aminecraftdev.cursecore.utils.NumberUtils;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.List;
import java.util.Map;

public class EnchanterGui extends Gui {

    public EnchanterGui(Player player) {
        super(player, EnchantInventoryConf.get().enchanterGui);
    }

    @Override
    public void init() {
        for(RarityWrapper rarityWrapper : EnchantConf.get().rarityWrappers) {
            handleSlot(getActionSlots(rarityWrapper.getName().toUpperCase()), rarityWrapper);
        }
    }

    @Override
    public Map<String, String> getReplaceMap() {
        return MUtil.map(
                "{orderCost}", NumberUtils.get().formatNumber(EnchantConf.get().orderScrollCost),
                "{shieldCost}", NumberUtils.get().formatNumber(EnchantConf.get().shieldScrollCost),
                "{blackScrollCost}", NumberUtils.get().formatNumber(EnchantConf.get().blackScrollCost)
        );
    }

    private void handleSlot(List<Integer> slots, RarityWrapper rarityWrapper) {
        Map<String, String> replaceMap = MUtil.map(
                "{formattedRarity}", rarityWrapper.getFormattedName(),
                "{cost}", NumberUtils.get().formatNumber(rarityWrapper.getCost())
        );

        ItemStack itemStack = EnchantInventoryConf.get().enchanterBookIcon.toItemStack(replaceMap);

        slots.forEach(slot -> getInventory().setItem(slot, itemStack.clone()));
    }
}
