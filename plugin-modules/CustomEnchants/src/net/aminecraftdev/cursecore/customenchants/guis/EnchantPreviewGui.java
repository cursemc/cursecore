package net.aminecraftdev.cursecore.customenchants.guis;

import com.massivecraft.massivecore.util.MUtil;
import com.massivecraft.massivecore.util.Txt;
import net.aminecraftdev.cursecore.customenchants.entity.EnchantInventoryConf;
import net.aminecraftdev.cursecore.customenchants.entity.wrapper.CEnchantWrapper;
import net.aminecraftdev.cursecore.customenchants.handlers.CEnchantment;
import net.aminecraftdev.cursecore.handlers.Gui;
import net.aminecraftdev.cursecore.utils.RomanNumberUtils;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.*;

public class EnchantPreviewGui extends Gui {

    private List<CEnchantment> enchantments;
    private String categoryName;
    private Gui previousGui;

    public EnchantPreviewGui(Player player, Gui previousGui, List<CEnchantment> enchantments, String categoryName) {
        super(player);

        this.previousGui = previousGui;
        this.categoryName = categoryName;
        this.enchantments = enchantments;

        setInventory(EnchantInventoryConf.get().enchantPreviewGui);
    }

    @Override
    public void init() {
        this.enchantments.sort(Comparator.comparing(CEnchantment::getName));

        for(CEnchantment enchantment : this.enchantments) {
            if(!enchantment.isEnabled()) continue;
            CEnchantWrapper wrapper = enchantment.getWrapper();

            Map<String, String> replaceMap = MUtil.map(
                    "{formattedName}", enchantment.getFormattedName(),
                    "{name}", enchantment.getName(),
                    "{maxLevel}", RomanNumberUtils.get().getRomanNumber(wrapper.getMaxLevel()),
                    "{enchantType}", wrapper.getEnchantmentType().name().toUpperCase(),
                    "{rarity}", wrapper.getRarity().toUpperCase()
            );

            ItemStack itemStack = EnchantInventoryConf.get().enchantPreviewIcon.toItemStack(replaceMap);
            ItemMeta itemMeta = itemStack.getItemMeta();
            List<String> lore = itemMeta.getLore();
            List<String> newLore = new ArrayList<>();

            for(String s : lore) {
                if(s.contains("{description}")) {
                    newLore.addAll(wrapper.getDescription());
                } else {
                    newLore.add(s);
                }
            }

            newLore.replaceAll(Txt::parse);
            itemMeta.setLore(newLore);
            itemStack.setItemMeta(itemMeta);

            getInventory().addItem(itemStack);
        }
    }

    @Override
    public Map<String, String> getReplaceMap() {
        return MUtil.map(
                "{category}", this.categoryName
        );
    }

    public void openPreviousGui() {
        if(this.previousGui == null) {
            close(true);
        } else {
            this.previousGui.open();
        }
    }
}
