package net.aminecraftdev.cursecore.customenchants.utils;

import lombok.Getter;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

public enum EnchantmentType {

    Helmet("_HELMET"),
    Chestplate("_CHESTPLATE"),
    Leggings("_LEGGINGS"),
    Boots("_BOOTS"),
    Armour(Helmet.getEndsWith(), Chestplate.getEndsWith(), Leggings.getEndsWith(), Boots.getEndsWith()),

    Sword("_SWORD"),
    Axe("_AXE"),
    Bow("BOW"),
    Weapon(Sword.getEndsWith(), Axe.getEndsWith(), Bow.getEndsWith()),

    Pickaxe("_PICKAXE"),
    Shovel("_SPADE"),
    Tool(Pickaxe.getEndsWith(), Shovel.getEndsWith()),

    All(Armour.getEndsWith(), Weapon.getEndsWith(), Tool.getEndsWith());

    @Getter private List<String> endsWith = new ArrayList<>();

    EnchantmentType(String endsWith) {
        this.endsWith.add(endsWith);
    }

    @SafeVarargs
    EnchantmentType(List<String>... endsWithList) {
        for(List<String> s : endsWithList) {
            this.endsWith.addAll(s);
        }
    }

    public boolean doesMaterialEndWith(Material material) {
        for(String s : this.endsWith) {
            if(material.toString().endsWith(s)) return true;
        }

        return false;
    }

    public static EnchantmentType getType(ItemStack itemStack) {
        Material material = itemStack.getType();

        for(EnchantmentType enchantmentType : values()) {
            if(enchantmentType.doesMaterialEndWith(material)) return enchantmentType;
        }

        return null;
    }

    public static EnchantmentType getByName(String name) {
        for(EnchantmentType enchantmentType : values()) {
            if(enchantmentType.name().equalsIgnoreCase(name)) return enchantmentType;
        }

        return null;
    }

}
