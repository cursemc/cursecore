package net.aminecraftdev.cursecore.customenchants.tasks;

import com.massivecraft.massivecore.ModuloRepeatTask;
import net.aminecraftdev.cursecore.customenchants.entity.RagePlayer;

public class TaskRagePlayer extends ModuloRepeatTask {

    private static TaskRagePlayer instance = new TaskRagePlayer();
    public static TaskRagePlayer get() { return instance; }

    @Override
    public long getDelayMillis() {
        return 1000;
    }

    @Override
    public void invoke(long l) {
        RagePlayer.getInstances().iterator().forEachRemaining(ragePlayer -> {
            if(!ragePlayer.isInCombo()) return;

            if(System.currentTimeMillis() > ragePlayer.getResetAt()) {
                ragePlayer.resetMultiplier();
                ragePlayer.setInCombo(false);
            }
        });
    }
}
