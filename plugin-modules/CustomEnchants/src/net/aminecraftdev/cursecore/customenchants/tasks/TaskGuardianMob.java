package net.aminecraftdev.cursecore.customenchants.tasks;

import lombok.Getter;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Creature;
import org.bukkit.entity.IronGolem;
import org.bukkit.entity.LivingEntity;

public class TaskGuardianMob implements Runnable {

    @Getter private LivingEntity livingEntity, target;
    private Location spawnLocation;

    public TaskGuardianMob(Location spawnLocation, LivingEntity target) {
        this.spawnLocation = spawnLocation;
        this.target = target;
    }

    @Override
    public void run() {
        if(this.livingEntity == null || this.livingEntity.isDead() || !this.livingEntity.isValid()) return;

        handleDeath();
    }

    public void spawnGuardianMob() {
        if(this.target == null || this.target.isDead() || !this.target.isValid()) return;

        this.livingEntity = this.spawnLocation.getWorld().spawn(this.spawnLocation, IronGolem.class);
        playEntityEffect(this.spawnLocation);

        Creature creature = (Creature) this.livingEntity;

        creature.setTarget(this.target);
    }

    public void handleDeath() {
        playEntityEffect(this.livingEntity.getLocation());
        this.livingEntity.remove();
    }

    private void playEntityEffect(Location location) {
        World world = location.getWorld();

        world.spigot().playEffect(location, Effect.LAVA_POP, 0, 0, 1f, 1f, 1f, 1f, 100, 32);
    }
}
