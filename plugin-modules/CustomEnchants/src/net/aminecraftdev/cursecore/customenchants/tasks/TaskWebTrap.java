package net.aminecraftdev.cursecore.customenchants.tasks;

import net.aminecraftdev.cursecore.customenchants.entity.EnchantsConf;
import net.aminecraftdev.cursecore.storage.PluginStorage;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.LivingEntity;

public class TaskWebTrap implements Runnable {

    private Material originalMaterial;
    private Location location;

    public TaskWebTrap(LivingEntity target) {
        this.location = target.getLocation();
    }

    public void begin() {
        if(this.location.getBlock().getType() == Material.AIR) {
            this.originalMaterial = this.location.getBlock().getType();
            this.location.getBlock().setType(Material.WEB);
        }

        Bukkit.getScheduler().runTaskLater(PluginStorage.get(), this, EnchantsConf.get().webTrapDelay*20);
    }

    @Override
    public void run() {
        if(this.originalMaterial == null) return;

        this.location.getBlock().setType(this.originalMaterial);
    }
}
