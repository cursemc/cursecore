package net.aminecraftdev.cursecore.customenchants.tasks;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.BlockState;

public class TaskResetBlock implements Runnable {

    private BlockState blockState;
    private Location location;

    public TaskResetBlock(Location location, BlockState blockState) {
        this.location = location;
        this.blockState = blockState;
    }

    @Override
    public void run() {
        if(this.location.getBlock().getType() == Material.AIR) return;

        World world = this.location.getWorld();

        world.getBlockAt(this.location).setType(this.blockState.getType());
        world.getBlockAt(this.location).setData(this.blockState.getData().getData());
    }
}
