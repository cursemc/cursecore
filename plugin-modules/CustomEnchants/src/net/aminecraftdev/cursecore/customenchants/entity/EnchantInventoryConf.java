package net.aminecraftdev.cursecore.customenchants.entity;

import com.massivecraft.massivecore.store.Entity;
import com.massivecraft.massivecore.util.MUtil;
import com.massivecraft.massivecore.xlib.guava.collect.Lists;
import net.aminecraftdev.cursecore.storage.Icon;
import net.aminecraftdev.cursecore.storage.InventoryStorage;
import org.bukkit.Material;

public class EnchantInventoryConf extends Entity<EnchantInventoryConf> {

    private static EnchantInventoryConf instance;
    public static EnchantInventoryConf get() { return instance; }
    public static void set(EnchantInventoryConf inventoryConf) { instance = inventoryConf; }

    public int[] tinkererUserSlots = {1, 2, 3, 9, 10, 11, 12, 18, 19, 20, 21, 27,
            28, 29, 30, 36, 37, 38, 39, 45, 46, 47, 48};

    public Icon enchantPreviewIcon = new Icon(Material.ENCHANTED_BOOK, 1, (short) 0, "{formattedName} I-{maxLevel}", MUtil.list(
            "{description}",
            " ",
            "&3Rarity: &f{rarity}",
            "&3Enchant Type: &f{enchantType}"
    ), false, "EMPTY");

    public Icon enchanterBookIcon = new Icon(Material.BOOK, 1, (short) 0, "{formattedRarity} Enchantments", MUtil.list(
            "&7Left-Click to purhcase an enchant",
            "&7book, or Right-Click to view the",
            "&7obtainable enchantments in this",
            "&7rarity.",
            " ",
            "&eCost: &f{cost} EXP"
    ), true, "EMPTY");

    public InventoryStorage enchantPreviewGui = new InventoryStorage("&8Enchants (&a{category}&8)", 45, MUtil.map(
            36, new Icon(Material.STAINED_GLASS_PANE, 1, (short) 15, true),
            37, new Icon(Material.STAINED_GLASS_PANE, 1, (short) 15, true),
            38, new Icon(Material.STAINED_GLASS_PANE, 1, (short) 15, true),
            39, new Icon(Material.STAINED_GLASS_PANE, 1, (short) 15, true),
            40, new Icon(Material.STAINED_GLASS_PANE, 1, (short) 15, true),
            41, new Icon(Material.STAINED_GLASS_PANE, 1, (short) 15, true),
            42, new Icon(Material.STAINED_GLASS_PANE, 1, (short) 15, true),
            43, new Icon(Material.STAINED_GLASS_PANE, 1, (short) 15, true),
            44, new Icon(Material.STAINED_GLASS_PANE, 1, (short) 15, true)
    ));

    public InventoryStorage enchantsGui = new InventoryStorage("&8Enchantment Categories", 36, MUtil.map(
            -1, new Icon(Material.STAINED_GLASS_PANE, 1, (short) 5, true),
            10, new Icon(Material.DIAMOND_HELMET, 1, (short) 0, "&e&lHelmet Enchantments", MUtil.list(
                    "&7Click here to view the Helmet",
                    "&7Enchantments."
            ), true, "HELMET"),
            11, new Icon(Material.DIAMOND_CHESTPLATE, 1, (short) 0, "&e&lChestplate Enchantments", MUtil.list(
                    "&7Click here to view the Chestplate",
                    "&7Enchantments."
            ), true, "CHESTPLATE"),
            12, new Icon(Material.DIAMOND_LEGGINGS, 1, (short) 0, "&e&lLegging Enchantments", MUtil.list(
                    "&7Click here to view the Legging",
                    "&7Enchantments."
            ), true, "LEGGINGS"),
            13, new Icon(Material.DIAMOND_BOOTS, 1, (short) 0, "&e&lBoot Enchantments", MUtil.list(
                    "&7Click here to view the Boot",
                    "&7Enchantments."
            ), true, "BOOTS"),
            14, new Icon(Material.DIAMOND_SWORD, 1, (short) 0, "&b&lSword Enchantments", MUtil.list(
                    "&7Click here to view the Sword",
                    "&7Enchantments."
            ), true, "SWORD"),
            15, new Icon(Material.BOW, 1, (short) 0, "&b&lBow Enchantments", MUtil.list(
                    "&7Click here to view the Bow",
                    "&7Enchantments."
            ), true, "BOW"),
            16, new Icon(Material.DIAMOND_AXE, 1, (short) 0, "&b&lAxe Enchantments", MUtil.list(
                    "&7Click here to view the Axe",
                    "&7Enchantments."
            ), true, "AXE"),
            19, new Icon(Material.DIAMOND_PICKAXE, 1, (short) 0, "&a&lPickaxe Enchantments", MUtil.list(
                    "&7Click here to view the Pickaxe",
                    "&7Enchantments."
            ), true, "PICKAXE"),
            20, new Icon(Material.DIAMOND_HOE, 1, (short) 0, "&a&lHoe Enchantments", MUtil.list(
                    "&7Click here to view the Hoe",
                    "&7Enchantments."
            ), true, "HOE"),
            21, new Icon(Material.DIAMOND_SPADE, 1, (short) 0, "&a&lShovel Enchantments", MUtil.list(
                    "&7Click here to view the Shovel",
                    "&7Enchantments."
            ), true, "SHOVEL"),
            22, new Icon(Material.BOOK, 1, (short) 0, "&d&lGlobal Enchantments", MUtil.list(
                    "&7Click here to view the Global",
                    "&7Enchantments.",
                    " ",
                    "&dNOTE",
                    "&7&oThis does not mean it will",
                    "&7&oshow every enchant in this GUI.",
                    "&7&oGlobal Enchants are an enchant",
                    "&7&othat can be applied to anything."
            ), true, "ALL"),
            23, new Icon(Material.GOLD_PICKAXE, 1, (short) 0, "&a&lTool Enchantments", MUtil.list(
                    "&7Click here to view all Tool",
                    "&7Enchantments."
            ), true, "TOOL"),
            24, new Icon(Material.GOLD_SWORD, 1, (short) 0, "&b&lWeapon Enchantments", MUtil.list(
                    "&7Click here to view all Weapon",
                    "&7Enchantments."
            ), true, "WEAPON"),
            25, new Icon(Material.GOLD_HELMET, 1, (short) 0, "&e&lArmour Enchantments", MUtil.list(
                    "&7Click here to view all Armour",
                    "&7Enchantments."
            ), true, "ARMOUR")
    ));

    public InventoryStorage enchanterGui = new InventoryStorage("&8Enchanter", 27, MUtil.map(
            -1, new Icon(Material.STAINED_GLASS_PANE, 1, (short) 5, true),
            0, new Icon(Material.COMPASS, 1, (short) 0, "&e&lEnchantment Information", MUtil.list(
                    "&7Click to open the Enchants GUI to",
                    "&7view information about every currently",
                    "&7activated enchantment."
            ), true, "ENCHANTS"),
            8, new Icon(Material.SULPHUR, 1, (short) 0, "&e&lTinkerer", MUtil.list(
                    "&7Click to open the Tinkerer GUI where",
                    "&7you can exchange enchantment books",
                    "&7for dust which can either increase the",
                    "&7success rate, decrease the destroy",
                    "&7rate, or turn in to failed dust."
            ), true, "TINKERER"),
            11, new Icon("BAD"),
            12, new Icon("GOOD"),
            13, new Icon("EPIC"),
            14, new Icon("SUPER"),
            15, new Icon("MYTHICAL"),
            18, new Icon(Material.QUARTZ, 1, (short) 0, "&e&lOrder Scroll", MUtil.list(
                    "&7Click to buy an &eOrder Scroll&7 which",
                    "&7can be used to organise enchants",
                    "&7on your item from lowest rarity at the",
                    "&7bottom to highest rarity at the top.",
                    " ",
                    "&eCost: &a$&f{orderCost}"
            ), true, "ORDER_SCROLL"),
            22, new Icon(Material.EMPTY_MAP, 1, (short) 0, "&e&lShield Scroll", MUtil.list(
                    "&7Click to buy a &eShield Scroll&7 which",
                    "&7will act as a protection agent against",
                    "&7failure while applying enchants to",
                    "&7an item.",
                    " ",
                    "&eCost: &a$&f{shieldCost}"
            ), true, "SHIELD_SCROLL"),
            26, new Icon(Material.INK_SACK, 1, (short) 0, "&e&lBlack Scroll", MUtil.list(
                    "&7Click to buy a &eBlack Scroll&7 which",
                    "&7can be used to withdraw a random",
                    "&7enchantment from an item and put",
                    "&7it in to an enchantment book.",
                    " ",
                    "&eCost: &a$&f{blackScrollCost}"
            ), true, "BLACK_SCROLL")
    ));


    public InventoryStorage tinkererGui = new InventoryStorage("&8Tinkerer", 54, MUtil.map(
            0, new Icon(Material.STAINED_GLASS_PANE, 1, (short) 5, "&a&lAccept Trade", Lists.newArrayList(), false, "CONFIRM"),
            4, new Icon(Material.STAINED_GLASS_PANE, 1, (short) 15, " ", Lists.newArrayList(), false, "FILLER"),
            8, new Icon(Material.STAINED_GLASS_PANE, 1, (short) 5, "&a&lAccept Trade", Lists.newArrayList(), false, "CONFIRM"),
            13, new Icon(Material.STAINED_GLASS_PANE, 1, (short) 15, " ", Lists.newArrayList(), false, "FILLER"),
            22, new Icon(Material.STAINED_GLASS_PANE, 1, (short) 15, " ", Lists.newArrayList(), false, "FILLER"),
            31, new Icon(Material.STAINED_GLASS_PANE, 1, (short) 15, " ", Lists.newArrayList(), false, "FILLER"),
            40, new Icon(Material.STAINED_GLASS_PANE, 1, (short) 15, " ", Lists.newArrayList(), false, "FILLER"),
            49, new Icon(Material.STAINED_GLASS_PANE, 1, (short) 15, " ", Lists.newArrayList(), false, "FILLER")
    ));

}
