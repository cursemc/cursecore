package net.aminecraftdev.cursecore.customenchants.entity;

import com.massivecraft.massivecore.store.Entity;
import com.massivecraft.massivecore.util.MUtil;
import net.aminecraftdev.cursecore.customenchants.entity.wrapper.CEnchantWrapper;
import net.aminecraftdev.cursecore.entity.wrapper.SoundWrapper;
import net.aminecraftdev.cursecore.customenchants.utils.EnchantmentType;
import org.bukkit.Sound;

import java.util.Map;

public class EnchantsConf extends Entity<EnchantsConf> {

    private static EnchantsConf instance;
    public static EnchantsConf get() { return instance; }
    public static void set(EnchantsConf enchantConf) { instance = enchantConf; }

    public SoundWrapper screamSound = new SoundWrapper(10, 1, Sound.ANVIL_LAND);
    public int blackOutDuration = 3, webTrapDelay = 5, blastMineRadius = 3, boomMultiplier = 2, directRadius = 15, mobDuration = 10, shatterCooldown = 15, superStrikeCooldown = 10;
    public double bluntMultiplier = 0.25, reduceReduction = 0.25, mcmmoMultiplier = 0.5, ployReduction = 0.10, moreXpMultiplier = 0.5, soulMultiplier = 0.5;
    public int maxMoneyStealAmount = 10000;
    public String headRipName = "&6&l{name}'s Skull";

    public Map<String, CEnchantWrapper> enchantWrappers = MUtil.map(
            //BAD
            "Blackout", new CEnchantWrapper("Blackout", 3, EnchantmentType.Bow, "Bad", 10.0, true, MUtil.list(
                    "&7Blind your enemies while your shooting",
                    "&7them down with a bow."
            )),
            "Fish", new CEnchantWrapper("Fish", 1, EnchantmentType.Helmet, "Bad", 100.0, true, MUtil.list(
                    "&7Gives you the ability to breath under",
                    "&7water while this enchant is active."
            )),
            "RetainXP", new CEnchantWrapper("RetainXP", 5, EnchantmentType.All, "Bad", 100.0, true, MUtil.list(
                    "&7Gives you a chance to keep your exp",
                    "&7on death while you have this on any",
                    "&7of your active items."
            )),
            "Scream", new CEnchantWrapper("Scream", 3, EnchantmentType.Sword, "Bad", 5.0, true, MUtil.list(
                    "&7Blast your enemies headphones with",
                    "&7a deafening sound."
            )),
            "Vision", new CEnchantWrapper("Vision", 2, EnchantmentType.Helmet, "Bad", 200.0, true, MUtil.list(
                    "&7Gives you the ability to see in the",
                    "&7dark."
            )),

            //GOOD
            "Arrow", new CEnchantWrapper("Arrow", 5, EnchantmentType.Bow, "Good", 9.0, true, MUtil.list(
                    "&7Shoots multiple arrows at once randomly."
            )),
            "BlastMine", new CEnchantWrapper("BlastMine", 3, EnchantmentType.Pickaxe, "Good", 45.0, true, MUtil.list(
                    "&7Mines specific blocks around whenever",
                    "&7you break a block."
            )),
            "Blunt", new CEnchantWrapper("Blunt", 4, EnchantmentType.Leggings, "Good", 20.0, true, MUtil.list(
                    "&7Deals more durability damage to your",
                    "&7opponents armor when they attack you."
            )),
            "Bunny", new CEnchantWrapper("Bunny", 2, EnchantmentType.Boots, "Good", 200.0, true, MUtil.list(
                    "&7Gives you the ability to jump extra",
                    "&7high."
            )),
            "CloudWalker", new CEnchantWrapper("CloudWalker", 2, EnchantmentType.Boots, "Good", 200.0, true, MUtil.list(
                    "&7Reduce fall damage while this",
                    "&7enchant is active."
            )),
            "Explode", new CEnchantWrapper("Explode", 3, EnchantmentType.Bow, "Good", 15.0, true, MUtil.list(
                    "&7Explode your enemies while firing your",
                    "&7bow at them."
            )),
            "GreenGas", new CEnchantWrapper("GreenGas", 4, EnchantmentType.Sword, "Good", 5.0, true, MUtil.list(
                    "&7Gives you a chance to poison your",
                    "&7enemy."
            )),
            "NoHunger", new CEnchantWrapper("NoHunger", 1, EnchantmentType.Chestplate, "Good", 100, true, MUtil.list(
                    "&7Removes hunger draining."
            )),
            "ObsidianDestroyer", new CEnchantWrapper("ObsidianDestroyer", 3, EnchantmentType.Pickaxe, "Good", 50.0, true, MUtil.list(
                    "&7Break obsidian in one hit, randomly."
            )),
            "Reduce", new CEnchantWrapper("Reduce", 4, EnchantmentType.Helmet, "Good", 10.0, true, MUtil.list(
                    "&7This will reduce the incoming",
                    "&7durability damage to your armor."
            )),
            "WebTrap", new CEnchantWrapper("WebTrap", 3, EnchantmentType.Weapon, "Good", 7.5, true, MUtil.list(
                    "&7Traps enemy in cobweb for a few",
                    "&7seconds."
            )),

            //EPIC
            "Boom", new CEnchantWrapper("Boom", 4, EnchantmentType.Axe, "Epic", 10.0, true, MUtil.list(
                    "&7Shockwave your enemies away a few",
                    "&7blocks."
            )),
            "Defend", new CEnchantWrapper("Defend", 2, EnchantmentType.Weapon, "Epic", 10.0, true, MUtil.list(
                    "&7Gives your enemy Mining Fatigue",
                    "&7for 1 - 10 seconds randomly."
            )),
            "ExtraHealth", new CEnchantWrapper("ExtraHealth", 3, EnchantmentType.Armour, "Epic", 300.0, true, MUtil.list(
                    "&7Gives extra hearts based on the",
                    "&7level of the enchant."
            )),
            "Fast", new CEnchantWrapper("Fast", 3, EnchantmentType.Boots, "Epic", 300.0, true, MUtil.list(
                    "&7Gives speed based on the level",
                    "&7of the enchant you hold."
            )),
            "FastBreak", new CEnchantWrapper("FastBreak", 3, EnchantmentType.Pickaxe, "Epic", 300.0, true, MUtil.list(
                    "&7Gives yourself Haste while mining."
            )),
            "McMMO", new CEnchantWrapper("McMMO", 5, EnchantmentType.Tool, "Epic", 10.0, true, MUtil.list(
                    "&7Gives extra McMMO exp with this",
                    "&7enchant equipped."
            )),
            "Ploy", new CEnchantWrapper("Ploy", 3, EnchantmentType.Armour, "Epic", 25.0, true, MUtil.list(
                    "&7This enchant will reduce your",
                    "&7incoming damage slightly."
            )),

            //SUPER
            "AntiSteal", new CEnchantWrapper("AntiSteal", 2, EnchantmentType.Armour, "Super", 80.0, true, MUtil.list(
                    "&7Has a high chance to stop MoneySteal",
                    "&7and ItemSteal enchants from affecting",
                    "&7you."
            )),
            "AttackDeflect", new CEnchantWrapper("AttackDeflect", 2, EnchantmentType.Sword, "Super", 40.0, true, MUtil.list(
                    "&7Blocking while being hit with this",
                    "&7enchant on your sword will allow the",
                    "&7damage to be deflected."
            )),
            "BullDog", new CEnchantWrapper("BullDog", 3, EnchantmentType.Armour, "Super", 10.0, true, MUtil.list(
                    "&7Give yourself random Damage Resistance",
                    "&7for &f1-5 seconds&7 randomly."
            )),
            "Damage", new CEnchantWrapper("Damage", 6, EnchantmentType.Sword, "Super", 25.0, true, MUtil.list(
                    "&7Stack damage multipliers to strike",
                    "&7the enemy with more damage when you",
                    "&7get continuous hit streaks."
            )),
            "Decay", new CEnchantWrapper("Decay", 3, EnchantmentType.Sword, "Super", 15.0, true, MUtil.list(
                    "&7Gives wither and dizzy effect",
                    "&7to your enemy."
            )),
            "Direct", new CEnchantWrapper("Direct", 1, EnchantmentType.Pickaxe, "Super", 100.0, true, MUtil.list(
                    "&7Right clicking will tell you the",
                    "&7nearby diamonds are."
            )),
            "ExtraOres", new CEnchantWrapper("ExtraOres", 1, EnchantmentType.Pickaxe, "Super", 10.0, true, MUtil.list(
                    "&7Occasionally turns blocks that you",
                    "&7touch into ores for 3 seconds unless",
                    "&7you mine them."
            )),
            "FireProtect", new CEnchantWrapper("FireProtect", 1, EnchantmentType.Armour, "Super", 100.0, true, MUtil.list(
                    "&7Protects yourself from any",
                    "&7fire or lava damage."
            )),
            "HeadRip", new CEnchantWrapper("HeadRip", 2, EnchantmentType.Sword, "Super", 25.0, true, MUtil.list(
                    "&7Has a chance to drop your target's",
                    "&7skull when you kill them."
            )),
            "Hurt", new CEnchantWrapper("Hurt", 5, EnchantmentType.Sword, "Super", 5.0, true, MUtil.list(
                    "&7Has a possibility to give your",
                    "&7target Weakness."
            )),
            "ItemSteal", new CEnchantWrapper("ItemSteal", 1, EnchantmentType.Sword, "Super", 1.0, true, MUtil.list(
                    "&7Has the possibility to steal",
                    "&7an item from your opponents",
                    "&7inventory."
            )),
            "Lifeless", new CEnchantWrapper("Lifeless", 2, EnchantmentType.Sword, "Super", 15.0, true, MUtil.list(
                    "&7Steal your opponents hearts in",
                    "&7PvP and PvE fights."
            )),
            "LifeSaver", new CEnchantWrapper("LifeSaver", 2, EnchantmentType.Helmet, "Super", 200.0, true, MUtil.list(
                    "&7Gives you permanent regen while",
                    "&7this enchant is applied."
            )),
            "Mob", new CEnchantWrapper("Mob", 1, EnchantmentType.Leggings, "Super", 1.0, true, MUtil.list(
                    "&7Spawn a guardian Iron Golem to",
                    "&7attack your attacker at a low",
                    "&7chance."
            )),
            "MoneySteal", new CEnchantWrapper("MoneySteal", 1, EnchantmentType.Axe, "Super", 1.0, true, MUtil.list(
                    "&7Has the possibility to steal",
                    "&7money from your opponents."
            )),
            "MoreXP", new CEnchantWrapper("MoreXP", 4, EnchantmentType.Sword, "Super", 25.0, true, MUtil.list(
                    "&7Extra XP levels when grinding",
                    "&7mobs."
            )),
            "Shatter", new CEnchantWrapper("Shatter", 3, EnchantmentType.Bow, "Super", 10.0, true, MUtil.list(
                    "&7Lowers your enemies health by",
                    "&71-7 hearts randomly."
            )),
            "Soul", new CEnchantWrapper("Soul", 2, EnchantmentType.Sword, "Super", 25.0, true, MUtil.list(
                    "&7Applies an additional 50% exp",
                    "&7multiplier when farming mobs,",
                    "&7which stacks with MoreXP."
            )),
            "SuperStrike", new CEnchantWrapper("SuperStrike", 4, EnchantmentType.Sword, "Super", 400.0, true, MUtil.list(
                    "&7Deals double damage every 10",
                    "&7seconds and stacks on top of",
                    "&7Damage."
            )),
            "Tough", new CEnchantWrapper("Tough", 4, EnchantmentType.Chestplate, "Super", 400.0, true, MUtil.list(
                    "&7Gives slowness, mining fatigue",
                    "&7and strength depending on the",
                    "&7level."
            )),
            "Voltage", new CEnchantWrapper("Voltage", 2, EnchantmentType.Weapon, "Super", 10.0, true, MUtil.list(
                    "&7Strike your enemy with lightning",
                    "&7when you hit them with this",
                    "&7enchant."
            ))
    );

}
