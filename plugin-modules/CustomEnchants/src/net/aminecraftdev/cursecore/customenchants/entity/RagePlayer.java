package net.aminecraftdev.cursecore.customenchants.entity;

import com.massivecraft.massivecore.xlib.guava.collect.ImmutableSet;
import net.aminecraftdev.cursecore.storage.PluginStorage;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.*;

public class RagePlayer {

    private static Set<RagePlayer> RAGE_PLAYERS = new HashSet<>();

    private double multiplier;
    private boolean inCombo;
    private long resetAt;
    private UUID uuid;

    public RagePlayer(Player player) {
        this.uuid = player.getUniqueId();

        updateResetAt();
        resetMultiplier();
        setInCombo(true);

        RAGE_PLAYERS.add(this);
    }

    public UUID getUUID() {
        return uuid;
    }

    public long getResetAt() {
        return resetAt;
    }

    public void updateResetAt() {
        this.resetAt = System.currentTimeMillis() + (10 * 1000);
    }

    public double getMultiplier() {
        return multiplier;
    }

    public double getMultipliedDamage(double damage) {
        return damage * multiplier;
    }

    public void increaseMultiplier() {
        this.multiplier += 0.1;
    }

    public void resetMultiplier() {
        this.multiplier = 1.0;
    }

    public boolean isInCombo() {
        return inCombo;
    }

    public void setInCombo(boolean bool) {
        this.inCombo = bool;
    }

    public static RagePlayer get(Player player) {
        for(RagePlayer ragePlayer : new ArrayList<>(RAGE_PLAYERS)) {
            if(ragePlayer.getUUID().equals(player.getUniqueId())) return ragePlayer;
        }

        return new RagePlayer(player);
    }

    public static Set<RagePlayer> getInstances() {
        return ImmutableSet.copyOf(RAGE_PLAYERS);
    }

}
