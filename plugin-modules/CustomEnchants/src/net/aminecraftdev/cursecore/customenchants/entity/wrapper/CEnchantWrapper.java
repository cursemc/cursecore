package net.aminecraftdev.cursecore.customenchants.entity.wrapper;

import lombok.Getter;
import net.aminecraftdev.cursecore.customenchants.utils.EnchantmentType;

import java.util.ArrayList;
import java.util.List;

public class CEnchantWrapper {

    private List<String> description;

    @Getter private EnchantmentType enchantmentType;
    @Getter private String rarity, name;
    @Getter private double occurrence;
    @Getter private boolean enabled;
    @Getter private int maxLevel;

    public CEnchantWrapper(String name, int maxLevel, EnchantmentType enchantmentType, String rarity, double maxOccurrence, boolean enabled, List<String> description) {
        this.name = name;
        this.maxLevel = maxLevel;
        this.rarity = rarity;
        this.occurrence = maxOccurrence;
        this.enchantmentType = enchantmentType;
        this.enabled = enabled;
        this.description = description;
    }

    public List<String> getDescription() {
        return new ArrayList<>(this.description);
    }

}
