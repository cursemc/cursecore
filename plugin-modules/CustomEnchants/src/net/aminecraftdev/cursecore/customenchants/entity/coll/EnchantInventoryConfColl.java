package net.aminecraftdev.cursecore.customenchants.entity.coll;

import com.massivecraft.massivecore.MassiveCore;
import com.massivecraft.massivecore.store.Coll;
import com.massivecraft.massivecore.store.MStore;
import net.aminecraftdev.cursecore.customenchants.entity.EnchantInventoryConf;
import net.aminecraftdev.cursecore.storage.PluginStorage;

public class EnchantInventoryConfColl extends Coll<EnchantInventoryConf> {

    private static EnchantInventoryConfColl instance = new EnchantInventoryConfColl();
    public static EnchantInventoryConfColl get() { return instance; }

    private EnchantInventoryConfColl() {
        super("customenchants_inventoryconf", EnchantInventoryConf.class, MStore.getDb(), PluginStorage.get());
    }

    @Override
    public void setActive(boolean active) {
        super.setActive(active);

        if (!active) return;

        EnchantInventoryConf.set(this.get(MassiveCore.INSTANCE, true));
    }
}
