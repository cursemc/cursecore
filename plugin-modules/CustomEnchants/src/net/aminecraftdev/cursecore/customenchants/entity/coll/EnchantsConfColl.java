package net.aminecraftdev.cursecore.customenchants.entity.coll;

import com.massivecraft.massivecore.MassiveCore;
import com.massivecraft.massivecore.store.Coll;
import com.massivecraft.massivecore.store.MStore;
import net.aminecraftdev.cursecore.customenchants.entity.EnchantsConf;
import net.aminecraftdev.cursecore.storage.PluginStorage;

public class EnchantsConfColl extends Coll<EnchantsConf> {

    private static EnchantsConfColl instance = new EnchantsConfColl();
    public static EnchantsConfColl get() { return instance; }

    private EnchantsConfColl() {
        super("customenchants_enchants", EnchantsConf.class, MStore.getDb(), PluginStorage.get());
    }

    @Override
    public void setActive(boolean active) {
        super.setActive(active);

        if (!active) return;

        EnchantsConf.set(this.get(MassiveCore.INSTANCE, true));
    }
}
