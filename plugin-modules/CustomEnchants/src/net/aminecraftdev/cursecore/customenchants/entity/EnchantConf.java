package net.aminecraftdev.cursecore.customenchants.entity;

import com.massivecraft.massivecore.store.Entity;
import com.massivecraft.massivecore.util.MUtil;
import com.massivecraft.massivecore.xlib.guava.collect.Lists;
import net.aminecraftdev.cursecore.customenchants.entity.wrapper.RarityWrapper;
import net.aminecraftdev.cursecore.storage.Icon;
import org.bukkit.Material;

import java.util.ArrayList;
import java.util.List;

public class EnchantConf extends Entity<EnchantConf> {

    private static EnchantConf instance;
    public static EnchantConf get() { return instance; }
    public static void set(EnchantConf enchantConf) { instance = enchantConf; }

    public List<String> disabledRegions = MUtil.list(
            "spawn",
            "shop"
    );

    public List<RarityWrapper> rarityWrappers = MUtil.list(
            new RarityWrapper("Bad", "&f", 500, 1),
            new RarityWrapper("Good", "&a", 2000, 2),
            new RarityWrapper("Epic", "&b", 5000, 3),
            new RarityWrapper("Super", "&e", 15000, 4),
            new RarityWrapper("Mythical", "&6&l", 30000, 5)
    );

    public int maxEnchants = 5;

    public double orderScrollCost = 7500000.0;
    public double shieldScrollCost = 10000000.0;
    public double blackScrollCost = 10000000.0;

    public String shieldScrollString = "&6&l(&k*&6&l)&e&l PROTECTED &6&l(&kI&6&l)";

    public Icon enchantBook = new Icon(Material.ENCHANTED_BOOK, 1, (short) 0, "{rarityColor}&l&n{enchantName} {enchantLevel}", MUtil.list(
            "&a{success}% Success Rate",
            "&c{destroy}% Destroy Rate",
            " ",
            "{description}",
            " ",
            "&7{type} Enchant",
            "&8Drag n' Drop on to an item to enchant."
    ), false, "EMPTY");

    public Icon blackScroll = new Icon(Material.INK_SACK, 1, (short) 0, "&b&lBlack Scroll", MUtil.list(
            "&7Apply this to an item with custom",
            "&7enchants and it can withdraw a random",
            "&7enchantment and put it in to an",
            "&7enchantment book.",
            " ",
            "&8Drag n' Drop on to an item to apply."
    ), false, "EMPTY");

    public Icon shieldScroll = new Icon(Material.EMPTY_MAP, 1, (short) 0, "&e&lShield Scroll", MUtil.list(
            "&7Apply this to an item to protect",
            "&7it against destruction on enchant",
            "&7application.",
            " ",
            "&8Drag n' Drop on to an item to apply."
    ), false, "EMPTY");

    public Icon orderScroll = new Icon(Material.QUARTZ, 1, (short) 0, "&e&lOrder Scroll", MUtil.list(
            "&7Apply this to an item to correctly",
            "&7order the enchants on the item",
            "&7from highest rarity at the top to",
            "&7lowest rarity at the bottom.",
            " ",
            "&8Drag n' Drop on to an item to apply."
    ), false, "EMPTY");

    public Icon successDust = new Icon(Material.GLOWSTONE_DUST, 1, (short) 0, "&a&lSuccess Dust &8&l[&f&l+{chance}%&8&l]", MUtil.list(
            "&7Apply this to an enchantment book",
            "&7and it will increase it's success",
            "&7chances.",
            " ",
            "&8Drag n' Drop on to an item to apply."
    ), true, "EMPTY");

    public Icon destroyDust = new Icon(Material.SULPHUR, 1, (short) 0, "&c&lDestroy Dust &8&l[&f&l-{chance}%&8&l]", MUtil.list(
            "&7Apply this to an enchantment book",
            "&7and it will decrease it's destroy",
            "&7chances.",
            " ",
            "&8Drag n' Drop on to an item to apply."
    ), true, "EMPTY");

    public Icon mysteryDust = new Icon(Material.FIREWORK_CHARGE, 1, (short) 0, "&e&lMystery Dust &8(Right-Click)", MUtil.list(
            "&7Discover the item hidden within this",
            "&7Mystery Dust by Right-Clicking it in",
            "&7your hand.",
            " ",
            "&8Right-Click to uncover this dust."
    ), false, "EMPTY");

    public Icon failedDust = new Icon(Material.INK_SACK, 1, (short) 11, "&7Failed Dust", Lists.newArrayList(), false, "EMPTY");

    public RarityWrapper getRarityWrapper(String name) {
        for(RarityWrapper rarityWrapper : new ArrayList<>(this.rarityWrappers)) {
            if(rarityWrapper.getName().equalsIgnoreCase(name)) return rarityWrapper;
        }

        return null;
    }

}
