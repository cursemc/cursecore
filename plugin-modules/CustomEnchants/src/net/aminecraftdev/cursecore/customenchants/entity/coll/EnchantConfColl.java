package net.aminecraftdev.cursecore.customenchants.entity.coll;

import com.massivecraft.massivecore.MassiveCore;
import com.massivecraft.massivecore.store.Coll;
import com.massivecraft.massivecore.store.MStore;
import net.aminecraftdev.cursecore.customenchants.entity.EnchantConf;
import net.aminecraftdev.cursecore.storage.PluginStorage;

public class EnchantConfColl extends Coll<EnchantConf> {

    private static EnchantConfColl instance = new EnchantConfColl();
    public static EnchantConfColl get() { return instance; }

    private EnchantConfColl() {
        super("customenchants_conf", EnchantConf.class, MStore.getDb(), PluginStorage.get());
    }

    @Override
    public void setActive(boolean active) {
        super.setActive(active);

        if (!active) return;

        EnchantConf.set(this.get(MassiveCore.INSTANCE, true));
    }
}
