package net.aminecraftdev.cursecore.customenchants.entity.wrapper;

import lombok.Getter;
import net.aminecraftdev.cursecore.customenchants.entity.EnchantConf;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class RarityWrapper {

    @Getter private String prefix, name;
    @Getter private int cost, weight;

    public RarityWrapper(String name, String prefix, int cost, int weight) {
        this.name = name;
        this.prefix = prefix;
        this.cost = cost;
        this.weight = weight;
    }

    public String getFormattedName() {
        return getPrefix() + getName();
    }

    public static List<RarityWrapper> getSortedWrappers() {
        List<RarityWrapper> newList = new ArrayList<>(EnchantConf.get().rarityWrappers);

        newList.sort(Comparator.comparingInt(RarityWrapper::getWeight));
        Collections.reverse(newList);

        return newList;
    }
}
