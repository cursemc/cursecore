package net.aminecraftdev.cursecore.customenchants.enchants.good;

import net.aminecraftdev.cursecore.customenchants.entity.EnchantsConf;
import net.aminecraftdev.cursecore.customenchants.events.enchants.ToolEnchantProcEvent;
import net.aminecraftdev.cursecore.customenchants.handlers.CEnchantment;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.inventory.ItemStack;

public class BlastMine extends CEnchantment {

    @Override
    public String getName() {
        return "BlastMine";
    }

    @EventHandler
    public void onMine(ToolEnchantProcEvent event) {
        Player player = event.getPlayer();
        ItemStack itemStack = player.getItemInHand();
        int level = getEnchantLevel(player);

        if(!hasEnchant(itemStack) || !canExecute(player, level)) return;

        Block block = event.getBlocks().get(0);
        int radius = EnchantsConf.get().blastMineRadius;

        for(int x = -radius; x <= radius; x++) {
            for(int z = -radius; z <= radius; z++) {
                for(int y = -radius; y <= radius; y++) {
                    Location location = block.getLocation().clone().add(x, y, z);

                    if(!canExecute(player, location, level)) continue;

                    event.addBlock(location.getBlock());
                }
            }
        }
    }
}
