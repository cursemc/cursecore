package net.aminecraftdev.cursecore.customenchants.enchants.supers;

import net.aminecraftdev.cursecore.entity.wrapper.PotionEffectWrapper;
import net.aminecraftdev.cursecore.customenchants.events.potion.ArmorPotionEquipEvent;
import net.aminecraftdev.cursecore.customenchants.events.potion.ArmorPotionUnequipEvent;
import net.aminecraftdev.cursecore.customenchants.handlers.CEnchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.potion.PotionEffectType;

public class FireProtect extends CEnchantment {

    @Override
    public String getName() {
        return "FireProtect";
    }

    @EventHandler
    public void onEquip(ArmorPotionEquipEvent event) {
        Player player = event.getPlayer();
        int enchantLevel = getEnchantLevel(player);

        if(!hasEnchant(player) || !canExecute(player, enchantLevel)) return;

        PotionEffectWrapper potionEffectWrapper = new PotionEffectWrapper(PotionEffectType.FIRE_RESISTANCE, enchantLevel, -1);

        potionEffectWrapper.addEffect(player);
    }

    @EventHandler
    public void onUnequip(ArmorPotionUnequipEvent event) {
        if(!hasEnchant(event.getPlayer())) return;

        event.getPlayer().removePotionEffect(PotionEffectType.FIRE_RESISTANCE);
    }
}
