package net.aminecraftdev.cursecore.customenchants.enchants.good;

import net.aminecraftdev.cursecore.customenchants.handlers.CEnchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageEvent;

public class CloudWalker extends CEnchantment {

    @Override
    public String getName() {
        return "CloudWalker";
    }

    @EventHandler
    public void onEntityDamage(EntityDamageEvent event) {
        if(!(event.getEntity() instanceof Player)) return;
        if(event.getCause() != EntityDamageEvent.DamageCause.FALL) return;

        Player player = (Player) event.getEntity();
        int level = getEnchantLevel(player);

        if(!hasEnchant(player) || !canExecute(player, level)) return;

        event.setCancelled(true);
    }
}
