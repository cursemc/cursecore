package net.aminecraftdev.cursecore.customenchants.enchants.supers;

import net.aminecraftdev.cursecore.entity.wrapper.PotionEffectWrapper;
import net.aminecraftdev.cursecore.customenchants.events.enchants.WeaponEnchantProcEvent;
import net.aminecraftdev.cursecore.customenchants.handlers.CEnchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffectType;

public class Hurt extends CEnchantment {

    @Override
    public String getName() {
        return "Hurt";
    }

    @EventHandler
    public void onPvP(WeaponEnchantProcEvent event) {
        if(!event.isPvp()) return;

        Player player = event.getPlayer();
        Player target = (Player) event.getTarget();
        ItemStack itemStack = player.getItemInHand();
        int enchantLevel = getEnchantLevel(itemStack);
        PotionEffectWrapper potionEffectWrapper = new PotionEffectWrapper(PotionEffectType.WEAKNESS, 2, enchantLevel, true);

        if(!hasEnchant(itemStack) || !canExecute(player, enchantLevel)) return;

        potionEffectWrapper.addEffect(target);
    }
}
