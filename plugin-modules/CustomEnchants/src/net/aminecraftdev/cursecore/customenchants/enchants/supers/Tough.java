package net.aminecraftdev.cursecore.customenchants.enchants.supers;

import com.massivecraft.massivecore.util.MUtil;
import net.aminecraftdev.cursecore.customenchants.events.potion.ArmorPotionEquipEvent;
import net.aminecraftdev.cursecore.customenchants.events.potion.ArmorPotionUnequipEvent;
import net.aminecraftdev.cursecore.customenchants.handlers.CEnchantment;
import net.aminecraftdev.cursecore.entity.wrapper.PotionEffectWrapper;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffectType;

import java.util.List;
import java.util.Map;

public class Tough extends CEnchantment {

    private Map<Integer, List<PotionEffectWrapper>> enchantMap = MUtil.map(
            1, MUtil.list(
                    new PotionEffectWrapper(PotionEffectType.INCREASE_DAMAGE, 1, Integer.MAX_VALUE, true),
                    new PotionEffectWrapper(PotionEffectType.SLOW_DIGGING, 1, Integer.MAX_VALUE, true),
                    new PotionEffectWrapper(PotionEffectType.SLOW, 1, Integer.MAX_VALUE, true)
            ),
            2, MUtil.list(
                    new PotionEffectWrapper(PotionEffectType.INCREASE_DAMAGE, 2, Integer.MAX_VALUE, true),
                    new PotionEffectWrapper(PotionEffectType.SLOW, 2, Integer.MAX_VALUE, true),
                    new PotionEffectWrapper(PotionEffectType.SLOW_DIGGING, 2, Integer.MAX_VALUE, true)
            ),
            3, MUtil.list(
                    new PotionEffectWrapper(PotionEffectType.INCREASE_DAMAGE, 3, Integer.MAX_VALUE, true),
                    new PotionEffectWrapper(PotionEffectType.SLOW_DIGGING, 3, Integer.MAX_VALUE, true),
                    new PotionEffectWrapper(PotionEffectType.SLOW, 3, Integer.MAX_VALUE, true)
            ),
            4, MUtil.list(
                    new PotionEffectWrapper(PotionEffectType.INCREASE_DAMAGE, 3, Integer.MAX_VALUE, true),
                    new PotionEffectWrapper(PotionEffectType.SLOW_DIGGING, 2, Integer.MAX_VALUE, true),
                    new PotionEffectWrapper(PotionEffectType.SLOW, 2, Integer.MAX_VALUE, true)
            )
    );

    @Override
    public String getName() {
        return "Tough";
    }

    @EventHandler
    public void onEquip(ArmorPotionEquipEvent event) {
        Player player = event.getPlayer();
        ItemStack itemStack = event.getItemStack();
        int enchantLevel = getEnchantLevel(itemStack);

        if(!hasEnchant(itemStack) || !canExecute(player, enchantLevel)) return;
        if(!this.enchantMap.containsKey(enchantLevel)) return;

        this.enchantMap.get(enchantLevel).forEach(potionEffectWrapper -> potionEffectWrapper.addEffect(player));
    }

    @EventHandler
    public void onUnequip(ArmorPotionUnequipEvent event) {
        Player player = event.getPlayer();
        ItemStack itemStack = event.getItemStack();
        int enchantLevel = getEnchantLevel(itemStack);

        if(!hasEnchant(itemStack) || !canExecute(player, enchantLevel)) return;

        this.enchantMap.get(enchantLevel).forEach(potionEffectWrapper -> potionEffectWrapper.removeEffect(player));
    }
}
