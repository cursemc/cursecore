package net.aminecraftdev.cursecore.customenchants.enchants.supers;

import net.aminecraftdev.cursecore.customenchants.events.enchants.WeaponEnchantProcEvent;
import net.aminecraftdev.cursecore.customenchants.handlers.CEnchantment;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.inventory.ItemStack;

public class Lifeless extends CEnchantment {

    @Override
    public String getName() {
        return "Lifeless";
    }

    @EventHandler
    public void onPvP(WeaponEnchantProcEvent event) {
        Player player = event.getPlayer();
        ItemStack itemStack = player.getItemInHand();
        int enchantLevel = getEnchantLevel(itemStack);
        LivingEntity target = event.getTarget();

        if(!hasEnchant(itemStack) || !canExecute(player, enchantLevel)) return;

        double playerHealth = player.getHealth();
        double maxPlayerHealth = player.getMaxHealth();

        double targetHealth = target.getHealth();

        player.setHealth(Math.min((playerHealth + enchantLevel), maxPlayerHealth));
        target.setHealth(Math.max((targetHealth - enchantLevel), 0));
    }
}
