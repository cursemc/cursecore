package net.aminecraftdev.cursecore.customenchants.enchants.good;

import net.aminecraftdev.cursecore.customenchants.events.enchants.BowEnchantProcEvent;
import net.aminecraftdev.cursecore.customenchants.handlers.CEnchantment;
import org.bukkit.Effect;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;

public class Explode extends CEnchantment {

    @Override
    public String getName() {
        return "Explode";
    }

    @EventHandler
    public void onBowPvP(BowEnchantProcEvent event) {
        Player player = event.getPlayer();
        int level = getEnchantLevel(player);

        if(!hasEnchant(player) || !canExecute(player, level)) return;

        LivingEntity livingEntity = event.getTarget();

        livingEntity.getWorld().playEffect(livingEntity.getLocation(), Effect.STEP_SOUND, Effect.LARGE_SMOKE);
    }
}
