package net.aminecraftdev.cursecore.customenchants.enchants.supers;

import net.aminecraftdev.cursecore.customenchants.events.enchants.BowEnchantProcEvent;
import net.aminecraftdev.cursecore.customenchants.events.enchants.WeaponEnchantProcEvent;
import net.aminecraftdev.cursecore.customenchants.handlers.CEnchantment;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.inventory.ItemStack;

public class Voltage extends CEnchantment {

    @Override
    public String getName() {
        return "Voltage";
    }

    @EventHandler
    public void onWeaponProc(WeaponEnchantProcEvent event) {
        Player player = event.getPlayer();
        LivingEntity target = event.getTarget();
        ItemStack itemStack = player.getItemInHand();
        int enchantLevel = getEnchantLevel(itemStack);

        if(!hasEnchant(itemStack) || !canExecute(player, enchantLevel)) return;

        target.getWorld().strikeLightningEffect(target.getLocation());
        target.damage(2.0, player);
    }

    @EventHandler
    public void onBowProc(BowEnchantProcEvent event) {
        Player player = event.getPlayer();
        LivingEntity target = event.getTarget();
        ItemStack itemStack = player.getItemInHand();
        int enchantLevel = getEnchantLevel(itemStack);

        if(!hasEnchant(itemStack) || !canExecute(player, enchantLevel)) return;

        target.getWorld().strikeLightningEffect(target.getLocation());
        target.damage(2.0, player);
    }
}
