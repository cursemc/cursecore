package net.aminecraftdev.cursecore.customenchants.enchants.good;

import net.aminecraftdev.cursecore.customenchants.entity.EnchantsConf;
import net.aminecraftdev.cursecore.customenchants.events.enchants.ArmorEnchantProcEvent;
import net.aminecraftdev.cursecore.customenchants.handlers.CEnchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;

public class Blunt extends CEnchantment {

    @Override
    public String getName() {
        return "Blunt";
    }

    @EventHandler
    public void onArmorProc(ArmorEnchantProcEvent event) {
        Player player = event.getPlayer();
        int level = getEnchantLevel(player);

        if(!event.isPvp()) return;
        if(!hasEnchant(player) || !canExecute(player, level)) return;

        event.setArmorDamageMultiplier(event.getArmorDamageMultiplier() + EnchantsConf.get().bluntMultiplier);
    }
}
