package net.aminecraftdev.cursecore.customenchants.enchants.epic;

import net.aminecraftdev.cursecore.entity.wrapper.PotionEffectWrapper;
import net.aminecraftdev.cursecore.customenchants.events.enchants.BowEnchantProcEvent;
import net.aminecraftdev.cursecore.customenchants.events.enchants.WeaponEnchantProcEvent;
import net.aminecraftdev.cursecore.customenchants.handlers.CEnchantment;
import net.aminecraftdev.cursecore.utils.RandomUtils;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffectType;

public class Defend extends CEnchantment {

    @Override
    public String getName() {
        return "Defend";
    }

    @EventHandler
    public void onPvP(WeaponEnchantProcEvent event) {
        Player player = event.getPlayer();
        ItemStack itemStack = player.getItemInHand();
        int enchantLevel = getEnchantLevel(itemStack);
        PotionEffectWrapper potionEffectWrapper = new PotionEffectWrapper(PotionEffectType.SLOW_DIGGING, 1, RandomUtils.get().getRandomNumber(10)+1);

        if(!event.isPvp()) return;
        if(!hasEnchant(itemStack) || !canExecute(player, enchantLevel)) return;

        potionEffectWrapper.addEffect((Player) event.getTarget());
    }

    @EventHandler
    public void onBow(BowEnchantProcEvent event) {
        Player player = event.getPlayer();
        ItemStack itemStack = player.getItemInHand();
        int enchantLevel = getEnchantLevel(itemStack);
        PotionEffectWrapper potionEffectWrapper = new PotionEffectWrapper(PotionEffectType.SLOW_DIGGING, 1, RandomUtils.get().getRandomNumber(10)+1);

        if(!event.isPvp()) return;
        if(!hasEnchant(itemStack) || !canExecute(player, enchantLevel)) return;

        potionEffectWrapper.addEffect((Player) event.getTarget());
    }
}
