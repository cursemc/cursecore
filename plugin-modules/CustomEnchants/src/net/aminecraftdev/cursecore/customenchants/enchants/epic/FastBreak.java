package net.aminecraftdev.cursecore.customenchants.enchants.epic;

import net.aminecraftdev.cursecore.entity.wrapper.PotionEffectWrapper;
import net.aminecraftdev.cursecore.customenchants.events.potion.ItemPotionEquipEvent;
import net.aminecraftdev.cursecore.customenchants.events.potion.ItemPotionUnequipEvent;
import net.aminecraftdev.cursecore.customenchants.handlers.CEnchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.potion.PotionEffectType;

public class FastBreak extends CEnchantment {

    @Override
    public String getName() {
        return "FastBreak";
    }

    @EventHandler
    public void onEquip(ItemPotionEquipEvent event) {
        Player player = event.getPlayer();
        int level = getEnchantLevel(player);

        if(!hasEnchant(player) || !canExecute(player, level)) return;

        PotionEffectWrapper potionEffectWrapper = new PotionEffectWrapper(PotionEffectType.FAST_DIGGING, level, -1);

        potionEffectWrapper.addEffect(player);
    }

    @EventHandler
    public void onUnequip(ItemPotionUnequipEvent event) {
        if(!hasEnchant(event.getPlayer())) return;

        event.getPlayer().removePotionEffect(PotionEffectType.FAST_DIGGING);
    }
}
