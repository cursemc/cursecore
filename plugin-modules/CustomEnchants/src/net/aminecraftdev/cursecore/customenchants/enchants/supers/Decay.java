package net.aminecraftdev.cursecore.customenchants.enchants.supers;

import net.aminecraftdev.cursecore.entity.wrapper.PotionEffectWrapper;
import net.aminecraftdev.cursecore.customenchants.events.enchants.WeaponEnchantProcEvent;
import net.aminecraftdev.cursecore.customenchants.handlers.CEnchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffectType;

public class Decay extends CEnchantment {

    @Override
    public String getName() {
        return "Decay";
    }

    @EventHandler
    public void onWeapon(WeaponEnchantProcEvent event) {
        Player player = event.getPlayer();
        ItemStack itemStack = player.getItemInHand();
        int enchantLevel = getEnchantLevel(itemStack);

        if(!event.isPvp()) return;
        if(!hasEnchant(itemStack) || !canExecute(player, enchantLevel)) return;

        PotionEffectWrapper wither = new PotionEffectWrapper(PotionEffectType.WITHER, 1, enchantLevel*3);
        PotionEffectWrapper dizzy = new PotionEffectWrapper(PotionEffectType.CONFUSION, 1, enchantLevel*3);
        Player target = (Player) event.getTarget();

        wither.addEffect(target);
        dizzy.addEffect(target);
    }
}
