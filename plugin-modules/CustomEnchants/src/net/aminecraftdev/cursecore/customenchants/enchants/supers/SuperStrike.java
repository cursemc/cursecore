package net.aminecraftdev.cursecore.customenchants.enchants.supers;

import net.aminecraftdev.cursecore.customenchants.entity.EnchantsConf;
import net.aminecraftdev.cursecore.customenchants.events.enchants.WeaponEnchantProcEvent;
import net.aminecraftdev.cursecore.customenchants.handlers.CEnchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.inventory.ItemStack;

public class SuperStrike extends CEnchantment {

    @Override
    public String getName() {
        return "SuperStrike";
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onWeapon(WeaponEnchantProcEvent event) {
        Player player = event.getPlayer();
        ItemStack itemStack = player.getItemInHand();
        int enchantLevel = getEnchantLevel(itemStack);

        if(!hasEnchant(itemStack) || !canExecute(player, enchantLevel)) return;

        addCooldown(player.getUniqueId(), EnchantsConf.get().superStrikeCooldown);
        event.setDamage(event.getDamage() * 2);
    }
}
