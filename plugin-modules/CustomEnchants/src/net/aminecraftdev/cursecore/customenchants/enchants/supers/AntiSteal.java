package net.aminecraftdev.cursecore.customenchants.enchants.supers;

import net.aminecraftdev.cursecore.customenchants.events.enchants.ItemStealEnchantProcEvent;
import net.aminecraftdev.cursecore.customenchants.events.enchants.MoneyStealEnchantProcEvent;
import net.aminecraftdev.cursecore.customenchants.handlers.CEnchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;

public class AntiSteal extends CEnchantment {

    @Override
    public String getName() {
        return "AntiSteal";
    }

    @EventHandler
    public void onMoneySteal(MoneyStealEnchantProcEvent event) {
        Player target = event.getTarget();
        int enchantLevel = getEnchantLevel(target);

        if(!hasEnchant(target) || !canExecute(target, enchantLevel)) return;

        event.setCancelled(true);
    }

    @EventHandler
    public void onItemSteal(ItemStealEnchantProcEvent event) {
        Player target = event.getTarget();
        int enchantLevel = getEnchantLevel(target);

        if(!hasEnchant(target) || !canExecute(target, enchantLevel)) return;

        event.setCancelled(true);
    }
}
