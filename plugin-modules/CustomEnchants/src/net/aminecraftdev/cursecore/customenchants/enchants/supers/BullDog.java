package net.aminecraftdev.cursecore.customenchants.enchants.supers;

import net.aminecraftdev.cursecore.entity.wrapper.PotionEffectWrapper;
import net.aminecraftdev.cursecore.customenchants.events.enchants.ArmorEnchantProcEvent;
import net.aminecraftdev.cursecore.customenchants.handlers.CEnchantment;
import net.aminecraftdev.cursecore.utils.RandomUtils;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.potion.PotionEffectType;

public class BullDog extends CEnchantment {

    @Override
    public String getName() {
        return "BullDog";
    }

    @EventHandler
    public void onArmorProc(ArmorEnchantProcEvent event) {
        Player player = event.getPlayer();
        int enchantLevel = getEnchantLevel(player);

        int duration = RandomUtils.get().getRandomNumber(5)+1;
        PotionEffectWrapper potionEffectWrapper = new PotionEffectWrapper(PotionEffectType.DAMAGE_RESISTANCE, enchantLevel, duration);

        if(!hasEnchant(player) || !canExecute(player, enchantLevel)) return;

        potionEffectWrapper.addEffect(player);
    }
}
