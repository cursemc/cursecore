package net.aminecraftdev.cursecore.customenchants.enchants.epic;

import net.aminecraftdev.cursecore.customenchants.entity.EnchantsConf;
import net.aminecraftdev.cursecore.customenchants.events.enchants.WeaponEnchantProcEvent;
import net.aminecraftdev.cursecore.customenchants.handlers.CEnchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

public class Boom extends CEnchantment {

    @Override
    public String getName() {
        return "Boom";
    }

    @EventHandler
    public void onWeapon(WeaponEnchantProcEvent event) {
        Player player = event.getPlayer();
        ItemStack itemStack = player.getItemInHand();
        int level = getEnchantLevel(itemStack);

        if(!event.isPvp()) return;
        if(!hasEnchant(itemStack) || !canExecute(player, level)) return;

        Player target = (Player) event.getTarget();
        Vector vector = target.getLocation().toVector().subtract(player.getLocation().toVector()).normalize();

        target.setVelocity(vector.multiply(EnchantsConf.get().boomMultiplier));
    }
}
