package net.aminecraftdev.cursecore.customenchants.enchants.good;

import net.aminecraftdev.cursecore.customenchants.events.enchants.BowEnchantProcEvent;
import net.aminecraftdev.cursecore.customenchants.handlers.CEnchantment;
import net.aminecraftdev.cursecore.utils.RandomUtils;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;

public class Arrow extends CEnchantment {

    @Override
    public String getName() {
        return "Arrow";
    }

    @EventHandler
    public void onBow(BowEnchantProcEvent event) {
        Player player = event.getPlayer();
        int level = getEnchantLevel(player);

        if(!hasEnchant(player) || !canExecute(player, level)) return;

        Projectile projectile = event.getProjectile();
        int amount = RandomUtils.get().getRandomNumber(25)+1;

        for(int i = 1; i <= amount; i++) {
            double x = RandomUtils.get().getRandomWholeDouble(3);
            double z = RandomUtils.get().getRandomWholeDouble(3);

            Location location = projectile.getLocation().clone().add(RandomUtils.get().canExecute()? x : -x, 0, RandomUtils.get().canExecute()? z : -z);

            projectile.getWorld().spawnArrow(location, projectile.getVelocity(), (float) 0.6, (float) 12);
        }

        projectile.getWorld().strikeLightningEffect(projectile.getLocation());
    }
}
