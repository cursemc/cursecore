package net.aminecraftdev.cursecore.customenchants.enchants.bad;

import net.aminecraftdev.cursecore.entity.wrapper.PotionEffectWrapper;
import net.aminecraftdev.cursecore.customenchants.events.potion.ArmorPotionEquipEvent;
import net.aminecraftdev.cursecore.customenchants.events.potion.ArmorPotionUnequipEvent;
import net.aminecraftdev.cursecore.customenchants.handlers.CEnchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.potion.PotionEffectType;

public class Fish extends CEnchantment {

    @Override
    public String getName() {
        return "Fish";
    }

    @EventHandler
    public void onEquip(ArmorPotionEquipEvent event) {
        PotionEffectWrapper potionEffectWrapper = new PotionEffectWrapper(PotionEffectType.WATER_BREATHING, 1, -1);
        Player player = event.getPlayer();
        int level = getEnchantLevel(player);

        if(!hasEnchant(player) || !canExecute(player, level)) return;

        potionEffectWrapper.addEffect(player);
    }

    @EventHandler
    public void onUnequip(ArmorPotionUnequipEvent event) {
        if(!hasEnchant(event.getItemStack())) return;

        event.getPlayer().removePotionEffect(PotionEffectType.WATER_BREATHING);
    }
}
