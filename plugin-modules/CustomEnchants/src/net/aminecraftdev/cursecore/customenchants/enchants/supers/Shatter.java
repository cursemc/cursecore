package net.aminecraftdev.cursecore.customenchants.enchants.supers;

import net.aminecraftdev.cursecore.customenchants.entity.EnchantsConf;
import net.aminecraftdev.cursecore.customenchants.events.enchants.BowEnchantProcEvent;
import net.aminecraftdev.cursecore.customenchants.handlers.CEnchantment;
import net.aminecraftdev.cursecore.utils.RandomUtils;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.inventory.ItemStack;

public class Shatter extends CEnchantment {

    @Override
    public String getName() {
        return "Shatter";
    }

    @EventHandler
    public void onBowProc(BowEnchantProcEvent event) {
        if(!event.isPvp()) return;

        Player player = event.getPlayer();
        Player target = (Player) event.getTarget();
        ItemStack itemStack = player.getItemInHand();
        int enchantLevel = getEnchantLevel(itemStack);

        if(!hasEnchant(player) || !canExecute(player, enchantLevel)) return;

        int hearts = RandomUtils.get().getRandomNumber(7)+1;
        double currentHearts = target.getHealth();

        addCooldown(player.getUniqueId(), EnchantsConf.get().shatterCooldown);

        target.setHealth(Math.max(currentHearts - hearts, 0));
    }
}
