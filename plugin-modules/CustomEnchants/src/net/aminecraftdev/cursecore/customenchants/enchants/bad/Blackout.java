package net.aminecraftdev.cursecore.customenchants.enchants.bad;

import net.aminecraftdev.cursecore.customenchants.entity.EnchantsConf;
import net.aminecraftdev.cursecore.customenchants.events.enchants.BowEnchantProcEvent;
import net.aminecraftdev.cursecore.customenchants.handlers.CEnchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class Blackout extends CEnchantment {

    @Override
    public String getName() {
        return "Blackout";
    }

    @EventHandler
    public void onProc(BowEnchantProcEvent event) {
        Player player = event.getPlayer();
        ItemStack itemStack = player.getItemInHand();
        int level = getEnchantLevel(itemStack);

        if(!hasEnchant(itemStack) || !canExecute(player, level)) return;

        event.getTarget().addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, (EnchantsConf.get().blackOutDuration*level)*20, 0));
    }
}
