package net.aminecraftdev.cursecore.customenchants.enchants.good;

import net.aminecraftdev.cursecore.entity.wrapper.PotionEffectWrapper;
import net.aminecraftdev.cursecore.customenchants.events.enchants.WeaponEnchantProcEvent;
import net.aminecraftdev.cursecore.customenchants.handlers.CEnchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffectType;

public class GreenGas extends CEnchantment {

    @Override
    public String getName() {
        return "GreenGas";
    }

    @EventHandler
    public void onPvP(WeaponEnchantProcEvent event) {
        Player player = event.getPlayer();
        ItemStack itemStack = player.getItemInHand();
        int level = getEnchantLevel(itemStack);

        if(!event.isPvp()) return;
        if(!hasEnchant(itemStack) || !canExecute(player, level)) return;

        Player target = (Player) event.getTarget();
        PotionEffectWrapper potionEffectWrapper = new PotionEffectWrapper(PotionEffectType.POISON, level, level*2);

        potionEffectWrapper.addEffect(target);
    }
}
