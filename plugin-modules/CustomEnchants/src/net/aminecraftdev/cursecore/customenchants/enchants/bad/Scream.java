package net.aminecraftdev.cursecore.customenchants.enchants.bad;

import net.aminecraftdev.cursecore.customenchants.entity.EnchantsConf;
import net.aminecraftdev.cursecore.customenchants.events.enchants.WeaponEnchantProcEvent;
import net.aminecraftdev.cursecore.customenchants.handlers.CEnchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.inventory.ItemStack;

public class Scream extends CEnchantment {

    @Override
    public String getName() {
        return "Scream";
    }

    @EventHandler
    public void onPvP(WeaponEnchantProcEvent event) {
        Player player = event.getPlayer();
        ItemStack itemStack = player.getItemInHand();
        int level = getEnchantLevel(itemStack);

        if(!event.isPvp()) return;
        if(!hasEnchant(itemStack) || !canExecute(player, level)) return;

        Player target = (Player) event.getTarget();

        EnchantsConf.get().screamSound.playSound(target);
    }
}
