package net.aminecraftdev.cursecore.customenchants.enchants.epic;

import com.gmail.nossr50.events.experience.McMMOPlayerXpGainEvent;
import net.aminecraftdev.cursecore.customenchants.entity.EnchantsConf;
import net.aminecraftdev.cursecore.customenchants.handlers.CEnchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.inventory.ItemStack;

public class McMMO extends CEnchantment {

    @Override
    public String getName() {
        return "McMMO";
    }

    @EventHandler
    public void onMcMMOExpGain(McMMOPlayerXpGainEvent event) {
        Player player = event.getPlayer();
        ItemStack itemStack = player.getItemInHand();
        int enchantLevel = getEnchantLevel(itemStack);

        if(!hasEnchant(itemStack) || !canExecute(player, enchantLevel)) return;

        float rawExp = event.getRawXpGained();

        event.setRawXpGained(rawExp + (float) (rawExp * EnchantsConf.get().mcmmoMultiplier));
    }
}
