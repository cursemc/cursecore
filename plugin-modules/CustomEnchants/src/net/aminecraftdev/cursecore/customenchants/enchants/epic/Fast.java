package net.aminecraftdev.cursecore.customenchants.enchants.epic;

import net.aminecraftdev.cursecore.entity.wrapper.PotionEffectWrapper;
import net.aminecraftdev.cursecore.customenchants.events.potion.ArmorPotionEquipEvent;
import net.aminecraftdev.cursecore.customenchants.events.potion.ArmorPotionUnequipEvent;
import net.aminecraftdev.cursecore.customenchants.handlers.CEnchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.potion.PotionEffectType;

public class Fast extends CEnchantment {

    @Override
    public String getName() {
        return "Fast";
    }

    @EventHandler
    public void onEquip(ArmorPotionEquipEvent event) {
        Player player = event.getPlayer();
        int level = getEnchantLevel(player);

        if(!hasEnchant(player) || !canExecute(player, level)) return;

        PotionEffectWrapper potionEffectWrapper = new PotionEffectWrapper(PotionEffectType.SPEED, level, -1);

        potionEffectWrapper.addEffect(player);
    }

    @EventHandler
    public void onUnequip(ArmorPotionUnequipEvent event) {
        if(!hasEnchant(event.getPlayer())) return;

        event.getPlayer().removePotionEffect(PotionEffectType.SPEED);
    }
}
