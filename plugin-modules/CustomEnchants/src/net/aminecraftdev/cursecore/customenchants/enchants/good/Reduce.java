package net.aminecraftdev.cursecore.customenchants.enchants.good;

import net.aminecraftdev.cursecore.customenchants.entity.EnchantsConf;
import net.aminecraftdev.cursecore.customenchants.events.enchants.ArmorEnchantProcEvent;
import net.aminecraftdev.cursecore.customenchants.handlers.CEnchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;

public class Reduce extends CEnchantment {

    @Override
    public String getName() {
        return "Reduce";
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onArmor(ArmorEnchantProcEvent event) {
        Player player = event.getPlayer();
        int level = getEnchantLevel(player);

        if(!hasEnchant(player) || !canExecute(player, level)) return;

        event.setArmorDamageMultiplier(event.getArmorDamageMultiplier() - EnchantsConf.get().reduceReduction);
    }

}
