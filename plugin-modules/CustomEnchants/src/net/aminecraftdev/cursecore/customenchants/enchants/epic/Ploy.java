package net.aminecraftdev.cursecore.customenchants.enchants.epic;

import net.aminecraftdev.cursecore.customenchants.entity.EnchantsConf;
import net.aminecraftdev.cursecore.customenchants.events.enchants.ArmorEnchantProcEvent;
import net.aminecraftdev.cursecore.customenchants.handlers.CEnchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;

public class Ploy extends CEnchantment {

    @Override
    public String getName() {
        return "Ploy";
    }

    @EventHandler
    public void onArmor(ArmorEnchantProcEvent event) {
        Player player = event.getPlayer();
        int enchantLevel = getEnchantLevel(player);

        if(!hasEnchant(player) || !canExecute(player, enchantLevel)) return;

        event.setDamage(event.getDamage() - (event.getDamage() * EnchantsConf.get().ployReduction));
    }
}
