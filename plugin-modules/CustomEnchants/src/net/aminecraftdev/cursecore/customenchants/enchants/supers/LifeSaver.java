package net.aminecraftdev.cursecore.customenchants.enchants.supers;

import net.aminecraftdev.cursecore.customenchants.events.potion.ArmorPotionEquipEvent;
import net.aminecraftdev.cursecore.customenchants.events.potion.ArmorPotionUnequipEvent;
import net.aminecraftdev.cursecore.customenchants.handlers.CEnchantment;
import net.aminecraftdev.cursecore.entity.wrapper.PotionEffectWrapper;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffectType;

public class LifeSaver extends CEnchantment {

    @Override
    public String getName() {
        return "LifeSaver";
    }

    @EventHandler
    public void onEquip(ArmorPotionEquipEvent event) {
        Player player = event.getPlayer();
        ItemStack itemStack = event.getItemStack();
        int enchantLevel = getEnchantLevel(itemStack);
        PotionEffectWrapper potionEffectWrapper = new PotionEffectWrapper(PotionEffectType.REGENERATION, enchantLevel, Integer.MAX_VALUE);

        if(!hasEnchant(itemStack) || !canExecute(player, enchantLevel)) return;

        potionEffectWrapper.addEffect(player);
    }

    @EventHandler
    public void onUnequip(ArmorPotionUnequipEvent event) {
        Player player = event.getPlayer();
        ItemStack itemStack = event.getItemStack();
        int enchantLevel = getEnchantLevel(itemStack);

        if(!hasEnchant(itemStack) || !canExecute(player, enchantLevel)) return;

        player.removePotionEffect(PotionEffectType.REGENERATION);
    }
}
