package net.aminecraftdev.cursecore.customenchants.enchants.supers;

import net.aminecraftdev.cursecore.customenchants.events.enchants.BowEnchantProcEvent;
import net.aminecraftdev.cursecore.customenchants.events.enchants.WeaponEnchantProcEvent;
import net.aminecraftdev.cursecore.customenchants.handlers.CEnchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.EventHandler;

public class AttackDeflect extends CEnchantment {

    @Override
    public String getName() {
        return "AttackDeflect";
    }

    @EventHandler
    public void onPvP(WeaponEnchantProcEvent event) {
        if(!event.isPvp()) return;

        Player target = (Player) event.getTarget();

        handleEvent(target, event);
    }

    @EventHandler
    public void onBow(BowEnchantProcEvent event) {
        if(!event.isPvp()) return;

        Player target = (Player) event.getTarget();

        handleEvent(target, event);
    }

    private void handleEvent(Player target, Cancellable event) {
        int enchantLevel = getEnchantLevel(target);

        if(!target.isBlocking()) return;
        if(!hasEnchant(target) || !canExecute(target, enchantLevel)) return;

        event.setCancelled(true);
    }
}
