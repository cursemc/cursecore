package net.aminecraftdev.cursecore.customenchants.enchants.supers;

import com.massivecraft.massivecore.util.MUtil;
import net.aminecraftdev.cursecore.customenchants.entity.EnchantsConf;
import net.aminecraftdev.cursecore.customenchants.handlers.CEnchantment;
import net.aminecraftdev.cursecore.entity.LangConf;
import net.aminecraftdev.cursecore.utils.NumberUtils;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

public class Direct extends CEnchantment {

    @Override
    public String getName() {
        return "Direct";
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent event) {
        if(!event.getAction().name().contains("RIGHT_CLICK")) return;

        Player player = event.getPlayer();
        ItemStack itemStack = player.getItemInHand();
        int enchantLevel = getEnchantLevel(itemStack);

        if(!hasEnchant(itemStack) || !canExecute(player, enchantLevel)) return;

        Location location = player.getLocation();
        int radius = EnchantsConf.get().directRadius;
        double d = Double.MAX_VALUE;
        Location diamondLocation = null;

        for(int x = -radius; x <= radius; x++) {
            for(int z = -radius; z <= radius; z++) {
                for(int y = -radius; y <= radius; y++) {
                    Location loc = location.getBlock().getRelative(x, y, z).getLocation();

                    if(loc.getBlock().getType() == Material.DIAMOND_ORE) {
                        double distance = loc.distanceSquared(location);

                        if(distance < d) {
                            d = distance;
                            diamondLocation = loc;
                        }
                    }
                }
            }
        }

        if(diamondLocation != null) {
            Vector vector = new Vector(diamondLocation.getX() - location.getX(), diamondLocation.getY() - 1.0, diamondLocation.getZ() - location.getZ());

            location.setDirection(vector);
            player.teleport(location);
        } else {
            LangConf.get().customEnchantsDirectNonNearby.msgError(player, MUtil.map(
                    "{radius}", NumberUtils.get().formatNumber(radius)
            ));
        }
    }
}
