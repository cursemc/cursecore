package net.aminecraftdev.cursecore.customenchants.enchants.supers;

import com.massivecraft.massivecore.util.MUtil;
import net.aminecraftdev.cursecore.customenchants.events.enchants.ItemStealEnchantProcEvent;
import net.aminecraftdev.cursecore.customenchants.events.enchants.WeaponEnchantProcEvent;
import net.aminecraftdev.cursecore.customenchants.handlers.CEnchantment;
import net.aminecraftdev.cursecore.entity.LangConf;
import net.aminecraftdev.cursecore.utils.RandomUtils;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.inventory.ItemStack;

import java.util.*;

public class ItemSteal extends CEnchantment {

    @Override
    public String getName() {
        return "ItemSteal";
    }

    @EventHandler
    public void onPvP(WeaponEnchantProcEvent event) {
        if(!event.isPvp()) return;

        Player player = event.getPlayer();
        Player target = (Player) event.getTarget();
        ItemStack itemStack = player.getItemInHand();
        int enchantLevel = getEnchantLevel(itemStack);

        if(player.getInventory().firstEmpty() == -1) return;
        if(!hasEnchant(itemStack) || !canExecute(player, enchantLevel)) return;

        Map<Integer, ItemStack> contents = new HashMap<>();

        for(int i = 0; i < target.getInventory().getSize(); i++) {
            ItemStack item = target.getInventory().getItem(i);

            if(Objects.isNull(item) || item.getType() == Material.AIR) continue;

            contents.put(i, item);
        }

        if(contents.isEmpty()) return;

        List<Map.Entry<Integer, ItemStack>> entries = new ArrayList<>(contents.entrySet());
        Map.Entry<Integer, ItemStack> entry = entries.get(RandomUtils.get().getRandomNumber(entries.size()));

        ItemStealEnchantProcEvent itemStealEnchantProcEvent = new ItemStealEnchantProcEvent(player, target, enchantLevel, entry.getValue());

        Bukkit.getPluginManager().callEvent(itemStealEnchantProcEvent);

        if(itemStealEnchantProcEvent.isCancelled()) {
            LangConf.get().customEnchantsItemStealBlocked.msg(target);
            return;
        }

        target.getInventory().setItem(entry.getKey(), new ItemStack(Material.AIR));
        player.getInventory().addItem(itemStealEnchantProcEvent.getItemStack());

        LangConf.get().customEnchantsItemStealDefender.msgError(target, MUtil.map(
                "{player}", player.getName()
        ));
        LangConf.get().customEnchantsItemStealAttacker.msg(player, MUtil.map(
                "{target}", target.getName()
        ));
    }
}
