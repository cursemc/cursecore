package net.aminecraftdev.cursecore.customenchants.enchants.supers;

import com.massivecraft.massivecore.xlib.guava.collect.ImmutableMap;
import net.aminecraftdev.cursecore.customenchants.entity.EnchantsConf;
import net.aminecraftdev.cursecore.customenchants.events.enchants.ArmorEnchantProcEvent;
import net.aminecraftdev.cursecore.customenchants.handlers.CEnchantment;
import net.aminecraftdev.cursecore.customenchants.tasks.TaskGuardianMob;
import net.aminecraftdev.cursecore.storage.PluginStorage;
import org.bukkit.Bukkit;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerTeleportEvent;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class Mob extends CEnchantment {

    private Map<UUID, TaskGuardianMob> guardianMobMap = new HashMap<>();

    @Override
    public String getName() {
        return "Mob";
    }

    @EventHandler
    public void onArmorProc(ArmorEnchantProcEvent event) {
        if(!event.isPvp()) return;

        Player player = event.getPlayer();
        int enchantLevel = getEnchantLevel(player);

        if(!hasEnchant(player) || !canExecute(player, enchantLevel)) return;
        if(this.guardianMobMap.containsKey(player.getUniqueId())) return;

        TaskGuardianMob taskGuardianMob = new TaskGuardianMob(player.getLocation(), event.getTarget());

        //TODO check if Target is friendly

        taskGuardianMob.spawnGuardianMob();
        Bukkit.getScheduler().runTaskLater(PluginStorage.get(), taskGuardianMob, EnchantsConf.get().mobDuration*20);
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onTeleport(PlayerTeleportEvent event) {
        Player player = event.getPlayer();

        removeGuardianMob(player.getUniqueId(), false);
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onEntityDeath(EntityDeathEvent event) {
        LivingEntity livingEntity = event.getEntity();

        ImmutableMap.copyOf(this.guardianMobMap).entrySet().stream()
                .filter(entry -> entry.getValue().getLivingEntity().getUniqueId().equals(livingEntity.getUniqueId()))
                .forEach(entry -> {
                    this.guardianMobMap.remove(entry.getKey());
                    event.setDroppedExp(0);
                    event.getDrops().clear();
                });
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onPlayerDeath(PlayerDeathEvent event) {
        Player player = event.getEntity();

        removeGuardianMob(player.getUniqueId(), false);
        removeGuardianMob(player.getUniqueId(), true);
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onQuit(PlayerQuitEvent event) {
        Player player = event.getPlayer();

        removeGuardianMob(player.getUniqueId(), false);
        removeGuardianMob(player.getUniqueId(), true);
    }

    private void removeGuardianMob(UUID uuid, boolean target) {
        ImmutableMap<UUID, TaskGuardianMob> mapCopy = ImmutableMap.copyOf(this.guardianMobMap);

        if(target) {
            mapCopy.entrySet().stream()
                    .filter(entry -> entry.getValue().getTarget().getUniqueId().equals(uuid))
                    .forEach(entry -> {
                        entry.getValue().handleDeath();
                        this.guardianMobMap.remove(entry.getKey());
                    });
        } else {
            mapCopy.entrySet().stream()
                    .filter(entry -> entry.getKey().equals(uuid))
                    .forEach(entry -> {
                        entry.getValue().handleDeath();
                        this.guardianMobMap.remove(entry.getKey());
                    });
        }
    }

}
