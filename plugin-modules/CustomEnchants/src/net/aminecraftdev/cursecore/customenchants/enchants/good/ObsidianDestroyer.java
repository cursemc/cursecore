package net.aminecraftdev.cursecore.customenchants.enchants.good;

import net.aminecraftdev.cursecore.customenchants.handlers.CEnchantment;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.BlockDamageEvent;
import org.bukkit.inventory.ItemStack;

public class ObsidianDestroyer extends CEnchantment {

    @Override
    public String getName() {
        return "ObsidianDestroyer";
    }

    @EventHandler
    public void onDamage(BlockDamageEvent event) {
        Player player = event.getPlayer();
        Block block = event.getBlock();
        ItemStack itemStack = player.getItemInHand();

        if(block.getType() != Material.OBSIDIAN) return;

        int enchantLevel = getEnchantLevel(itemStack);

        if(!hasEnchant(itemStack) || !canExecute(player, enchantLevel)) return;

        event.setInstaBreak(true);
    }
}
