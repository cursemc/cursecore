package net.aminecraftdev.cursecore.customenchants.enchants.supers;

import com.massivecraft.massivecore.money.MoneyMixinVault;
import com.massivecraft.massivecore.util.MUtil;
import net.aminecraftdev.cursecore.customenchants.entity.EnchantsConf;
import net.aminecraftdev.cursecore.customenchants.events.enchants.MoneyStealEnchantProcEvent;
import net.aminecraftdev.cursecore.customenchants.events.enchants.WeaponEnchantProcEvent;
import net.aminecraftdev.cursecore.customenchants.handlers.CEnchantment;
import net.aminecraftdev.cursecore.entity.LangConf;
import net.aminecraftdev.cursecore.utils.RandomUtils;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.inventory.ItemStack;

public class MoneySteal extends CEnchantment {

    @Override
    public String getName() {
        return "MoneySteal";
    }

    @EventHandler
    public void onProc(WeaponEnchantProcEvent event) {
        if(!event.isPvp()) return;

        Player player = event.getPlayer();
        Player target = (Player) event.getTarget();
        ItemStack itemStack = player.getItemInHand();
        int enchantLevel = getEnchantLevel(itemStack);

        if(!hasEnchant(itemStack) || !canExecute(player, enchantLevel)) return;

        double amount = RandomUtils.get().getRandomWholeDouble(EnchantsConf.get().maxMoneyStealAmount);

        MoneyStealEnchantProcEvent moneyStealEnchantProcEvent = new MoneyStealEnchantProcEvent(player, target, enchantLevel, amount);

        Bukkit.getPluginManager().callEvent(moneyStealEnchantProcEvent);

        if(moneyStealEnchantProcEvent.isCancelled()) {
            LangConf.get().customEnchantsMoneyStealBlocked.msg(target);
            return;
        }

        MoneyMixinVault.get().getEconomy().withdrawPlayer(target, amount);
        MoneyMixinVault.get().getEconomy().depositPlayer(player, amount);

        LangConf.get().customEnchantsMoneyStealDefender.msgError(target, MUtil.map(
                "{amount}", MoneyMixinVault.get().format(amount),
                "{player}", player.getName()
        ));
        LangConf.get().customEnchantsMoneyStealAttacker.msg(player, MUtil.map(
                "{amount}", MoneyMixinVault.get().format(amount),
                "{target}", target.getName()
        ));
    }
}
