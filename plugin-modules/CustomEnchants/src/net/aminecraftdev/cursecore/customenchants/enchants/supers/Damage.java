package net.aminecraftdev.cursecore.customenchants.enchants.supers;

import net.aminecraftdev.cursecore.customenchants.entity.RagePlayer;
import net.aminecraftdev.cursecore.customenchants.events.enchants.WeaponEnchantProcEvent;
import net.aminecraftdev.cursecore.customenchants.handlers.CEnchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.inventory.ItemStack;

public class Damage extends CEnchantment {

    public Damage() {
        super();
    }

    @Override
    public String getName() {
        return "Damage";
    }

    @EventHandler
    public void onWeapon(WeaponEnchantProcEvent event) {
        Player player = event.getPlayer();
        ItemStack itemStack = player.getItemInHand();
        int enchantLevel = getEnchantLevel(itemStack);

        if(!event.isPvp()) return;
        if(!hasEnchant(itemStack) || !canExecute(player, enchantLevel)) return;

        RagePlayer ragePlayer = RagePlayer.get(player);

        event.setDamage(ragePlayer.getMultipliedDamage(event.getDamage()));
        ragePlayer.increaseMultiplier();
    }
}
