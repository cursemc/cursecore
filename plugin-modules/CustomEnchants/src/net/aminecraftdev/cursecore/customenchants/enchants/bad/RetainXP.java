package net.aminecraftdev.cursecore.customenchants.enchants.bad;

import net.aminecraftdev.cursecore.customenchants.handlers.CEnchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.PlayerDeathEvent;

public class RetainXP extends CEnchantment {

    @Override
    public String getName() {
        return "RetainXP";
    }

    @EventHandler
    public void onDeath(PlayerDeathEvent event) {
        Player player = event.getEntity();
        int level = getEnchantLevel(player);

        if(!hasEnchant(player) || !canExecute(player, level)) return;

        event.setKeepLevel(true);
    }
}
