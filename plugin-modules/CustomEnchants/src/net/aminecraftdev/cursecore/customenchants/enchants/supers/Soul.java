package net.aminecraftdev.cursecore.customenchants.enchants.supers;

import net.aminecraftdev.cursecore.customenchants.entity.EnchantsConf;
import net.aminecraftdev.cursecore.customenchants.events.enchants.KillEnchantProcEvent;
import net.aminecraftdev.cursecore.customenchants.handlers.CEnchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.inventory.ItemStack;

public class Soul extends CEnchantment {

    @Override
    public String getName() {
        return "Soul";
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onKill(KillEnchantProcEvent event) {
        Player player = event.getPlayer();
        ItemStack itemStack = player.getItemInHand();
        int enchantLevel = getEnchantLevel(itemStack);

        if (event.isPvp()) return;
        if (!hasEnchant(itemStack) || !canExecute(player, enchantLevel)) return;

        event.setExpMultiplier(event.getExpMultiplier() + EnchantsConf.get().soulMultiplier);
    }
}
