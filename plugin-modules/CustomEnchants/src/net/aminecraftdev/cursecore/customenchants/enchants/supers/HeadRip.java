package net.aminecraftdev.cursecore.customenchants.enchants.supers;

import com.massivecraft.massivecore.util.Txt;
import net.aminecraftdev.cursecore.customenchants.entity.EnchantsConf;
import net.aminecraftdev.cursecore.customenchants.events.enchants.KillEnchantProcEvent;
import net.aminecraftdev.cursecore.customenchants.handlers.CEnchantment;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

public class HeadRip extends CEnchantment {

    @Override
    public String getName() {
        return "HeadRip";
    }

    @EventHandler
    public void onKill(KillEnchantProcEvent event) {
        if(!event.isPvp()) return;

        Player player = event.getPlayer();
        Player target = (Player) event.getTarget();
        ItemStack itemStack = player.getItemInHand();
        int enchantLevel = getEnchantLevel(itemStack);

        if(!hasEnchant(itemStack) || !canExecute(player, enchantLevel)) return;

        ItemStack skullItem = new ItemStack(Material.SKULL_ITEM, 1, (short) 3);
        SkullMeta skullMeta = (SkullMeta) skullItem.getItemMeta();

        skullMeta.setOwner(target.getName());
        skullMeta.setDisplayName(Txt.parse(EnchantsConf.get().headRipName.replace("{name}", target.getName())));
        skullItem.setItemMeta(skullMeta);

        event.addDrop(skullItem);
    }
}
