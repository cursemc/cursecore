package net.aminecraftdev.cursecore.customenchants.enchants.supers;

import com.massivecraft.massivecore.util.MUtil;
import net.aminecraftdev.cursecore.customenchants.handlers.CEnchantment;
import net.aminecraftdev.cursecore.customenchants.tasks.TaskResetBlock;
import net.aminecraftdev.cursecore.storage.PluginStorage;
import net.aminecraftdev.cursecore.utils.RandomUtils;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import java.util.List;

public class ExtraOres extends CEnchantment {

    private static List<Material> CHECKED_BLOCKS = MUtil.list(Material.STONE, Material.DIRT, Material.SAND, Material.GRASS, Material.GRAVEL, Material.NETHERRACK, Material.ENDER_STONE);

    @Override
    public String getName() {
        return "ExtraOres";
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent event) {
        Player player = event.getPlayer();
        ItemStack itemStack = player.getItemInHand();
        int enchantLevel = getEnchantLevel(itemStack);

        if(!hasEnchant(itemStack) || !canExecute(player, enchantLevel)) return;
        if(event.getAction() != Action.LEFT_CLICK_BLOCK) return;

        Block block = event.getClickedBlock();
        Material material = block.getType();

        if(!CHECKED_BLOCKS.contains(material)) return;

        World world = block.getWorld();
        Location location = block.getLocation();
        int random = RandomUtils.get().getRandomBetweenHundred();
        BlockState blockState = block.getState();
        Material newMaterial;

        if(random <= 25) {
            newMaterial = Material.COAL_ORE;
        } else if(random <= 45) {
            newMaterial = Material.IRON_ORE;
        } else if(random <= 60) {
            newMaterial = Material.LAPIS_ORE;
        } else if(random <= 80) {
            newMaterial = Material.GOLD_ORE;
        } else if(random <= 90) {
            newMaterial = Material.EMERALD_ORE;
        } else if(random <= 100) {
            newMaterial = Material.DIAMOND_ORE;
        } else {
            return;
        }

        Bukkit.getScheduler().runTaskLater(PluginStorage.get(), new TaskResetBlock(location, blockState), 60L);
        world.getBlockAt(location).setType(newMaterial);
    }
}
