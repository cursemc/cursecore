package net.aminecraftdev.cursecore.customenchants.enchants.good;

import net.aminecraftdev.cursecore.customenchants.events.enchants.BowEnchantProcEvent;
import net.aminecraftdev.cursecore.customenchants.events.enchants.WeaponEnchantProcEvent;
import net.aminecraftdev.cursecore.customenchants.handlers.CEnchantment;
import net.aminecraftdev.cursecore.customenchants.tasks.TaskWebTrap;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;

public class WebTrap extends CEnchantment {

    @Override
    public String getName() {
        return "WebTrap";
    }

    @EventHandler
    public void onPvP(WeaponEnchantProcEvent event) {
        Player player = event.getPlayer();
        int level = getEnchantLevel(player);

        if(!event.isPvp()) return;
        if(!hasEnchant(player) || !canExecute(player, level)) return;

        new TaskWebTrap(event.getTarget()).begin();
    }

    @EventHandler
    public void onBow(BowEnchantProcEvent event) {
        Player player = event.getPlayer();
        int level = getEnchantLevel(player);

        if(!hasEnchant(player) || !canExecute(player, level)) return;

        new TaskWebTrap(event.getTarget()).begin();
    }
}
