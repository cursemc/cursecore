package net.aminecraftdev.cursecore.customenchants.commands.types;

import com.massivecraft.massivecore.MassiveException;
import com.massivecraft.massivecore.command.type.TypeAbstract;
import net.aminecraftdev.cursecore.customenchants.handlers.DustType;
import net.aminecraftdev.cursecore.entity.LangConf;
import org.bukkit.command.CommandSender;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class TypeDust extends TypeAbstract<DustType> {

    private static TypeDust instance = new TypeDust();
    public static TypeDust get() { return instance; }

    private TypeDust() {
        super(DustType.class);
    }

    @Override
    public DustType read(String s, CommandSender commandSender) throws MassiveException {
        try {
            return DustType.valueOf(s.toUpperCase());
        } catch (Exception ex) {
            throw new MassiveException().addMessage(LangConf.get().customEnchantsDustNotFound.toExceptionString());
        }
    }

    @Override
    public Collection<String> getTabList(CommandSender commandSender, String s) {
        List<String> tabs = new ArrayList<>();

        Arrays.stream(DustType.values()).forEach(dustType -> tabs.add(dustType.name()));

        return tabs;
    }
}
