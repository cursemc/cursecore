package net.aminecraftdev.cursecore.customenchants.commands;

import com.massivecraft.massivecore.command.MassiveCommand;
import com.massivecraft.massivecore.command.requirement.RequirementHasPerm;
import net.aminecraftdev.cursecore.customenchants.commands.customitems.*;

public class CustomItemsCmd extends MassiveCommand {

    private static CustomItemsCmd instance = new CustomItemsCmd();
    public static CustomItemsCmd get() { return instance; }

    private CustomItemsCmd() {
        addAliases("customitems", "cusi");
        setDesc("The parent command for CustomItems manipulation on CustomEnchants.");

        addChild(new CmdCustomItemsBlackScroll());
        addChild(new CmdCustomItemsBook());
        addChild(new CmdCustomItemsDust());
        addChild(new CmdCustomItemsOrderScroll());
        addChild(new CmdCustomItemsShieldScroll());

        addRequirements(RequirementHasPerm.get("cc.enchants.admin"));
    }

}
