package net.aminecraftdev.cursecore.customenchants.commands;

import com.massivecraft.massivecore.command.MassiveCommand;
import com.massivecraft.massivecore.command.requirement.RequirementIsPlayer;

public class GKitCmd extends MassiveCommand {

    private static GKitCmd instance = new GKitCmd();
    public static GKitCmd get() { return instance; }

    private GKitCmd() {
        setAliases("gkit", "ckit", "ikit", "customkit");
        setDesc("The parent command for CustomEnchant GKits.");

        //TODO: Add Childs
    }

    @Override
    public void perform() {
        System.out.println(args.size());
        System.out.println(args);

        if(args.size() == 0) {
            if(RequirementIsPlayer.get().apply(this.sender, this)) {
                //TODO: OpenGUI
                return;
            }
        }

        this.getHelpCommand().execute(this.sender, this.getArgs());
    }
}
