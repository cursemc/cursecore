package net.aminecraftdev.cursecore.customenchants.commands;

import com.massivecraft.massivecore.command.MassiveCommand;
import com.massivecraft.massivecore.command.requirement.RequirementIsPlayer;
import net.aminecraftdev.cursecore.customenchants.guis.EnchanterGui;
import org.bukkit.entity.Player;

public class EnchanterCmd extends MassiveCommand {

    private static EnchanterCmd instance = new EnchanterCmd();
    public static EnchanterCmd get() { return instance; }

    private EnchanterCmd() {
        setAliases("enchanter", "ce", "ces");
        setDesc("Used to open the CustomEnchants Enchanter GUI.");

        addRequirements(RequirementIsPlayer.get());
    }

    @Override
    public void perform() {
        new EnchanterGui((Player) this.sender).open();
    }

}
