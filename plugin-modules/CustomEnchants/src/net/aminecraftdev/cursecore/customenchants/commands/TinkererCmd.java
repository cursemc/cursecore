package net.aminecraftdev.cursecore.customenchants.commands;

import com.massivecraft.massivecore.command.MassiveCommand;
import com.massivecraft.massivecore.command.requirement.RequirementIsPlayer;
import net.aminecraftdev.cursecore.customenchants.guis.TinkererGui;
import org.bukkit.entity.Player;

public class TinkererCmd extends MassiveCommand {

    private static TinkererCmd instance = new TinkererCmd();
    public static TinkererCmd get() { return instance; }

    private TinkererCmd() {
        setAliases("tinkerer", "tinkey");
        setDesc("Used to open the CustomEnchants Tinkerer GUI.");

        addRequirements(RequirementIsPlayer.get());
    }

    @Override
    public void perform() {
        new TinkererGui((Player) this.sender).open();
    }

}
