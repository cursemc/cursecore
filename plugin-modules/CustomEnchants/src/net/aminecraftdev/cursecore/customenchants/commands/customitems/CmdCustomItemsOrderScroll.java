package net.aminecraftdev.cursecore.customenchants.commands.customitems;

import com.massivecraft.massivecore.MassiveException;
import com.massivecraft.massivecore.command.MassiveCommand;
import com.massivecraft.massivecore.command.type.primitive.TypeInteger;
import com.massivecraft.massivecore.command.type.sender.TypePlayer;
import com.massivecraft.massivecore.util.MUtil;
import net.aminecraftdev.cursecore.customenchants.handlers.customitems.CustomItemsShieldScroll;
import net.aminecraftdev.cursecore.entity.LangConf;
import net.aminecraftdev.cursecore.utils.NumberUtils;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class CmdCustomItemsOrderScroll extends MassiveCommand {

    public CmdCustomItemsOrderScroll() {
        setAliases("order", "orderscroll", "orderboi");
        setDesc("Give a player a CEnchantment Order Scroll.");

        addParameter(TypePlayer.get(), "target");
        addParameter(TypeInteger.get(), "amount");
    }

    @Override
    public void perform() throws MassiveException {
        Player target = this.readArg();
        int amount = this.readArg();

        ItemStack itemStack = CustomItemsShieldScroll.get().getCompleteItem(amount);

        target.getInventory().addItem(itemStack);
        LangConf.get().customEnchantsShieldScrollReceived.msg(target, MUtil.map("{amount}", NumberUtils.get().formatNumber(amount)));
    }

}
