package net.aminecraftdev.cursecore.customenchants.commands.types;

import com.massivecraft.massivecore.MassiveException;
import com.massivecraft.massivecore.command.type.TypeAbstract;
import net.aminecraftdev.cursecore.customenchants.engines.EnchantEngine;
import net.aminecraftdev.cursecore.customenchants.handlers.CEnchantment;
import net.aminecraftdev.cursecore.entity.LangConf;
import org.bukkit.command.CommandSender;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class TypeCEnchantment extends TypeAbstract<CEnchantment> {

    private static TypeCEnchantment instance = new TypeCEnchantment();
    public static TypeCEnchantment get() { return instance; }

    private TypeCEnchantment() {
        super(CEnchantment.class);
    }

    @Override
    public CEnchantment read(String s, CommandSender commandSender) throws MassiveException {
        CEnchantment enchantment = EnchantEngine.get().getByName(s);

        if(enchantment == null) throw new MassiveException().addMessage(LangConf.get().customEnchantsEnchantNotFound.toExceptionString());

        return enchantment;
    }

    @Override
    public Collection<String> getTabList(CommandSender commandSender, String s) {
        List<String> tabs = new ArrayList<>();

        EnchantEngine.get().getEnchantments().stream().filter(CEnchantment::isEnabled).forEach(enchantment -> tabs.add(enchantment.getName()));
        Collections.sort(tabs);

        return tabs;
    }
}
