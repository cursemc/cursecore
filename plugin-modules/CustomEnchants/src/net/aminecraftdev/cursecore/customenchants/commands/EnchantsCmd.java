package net.aminecraftdev.cursecore.customenchants.commands;

import com.massivecraft.massivecore.command.MassiveCommand;
import com.massivecraft.massivecore.command.requirement.RequirementIsPlayer;
import net.aminecraftdev.cursecore.customenchants.guis.EnchantsGui;
import org.bukkit.entity.Player;

public class EnchantsCmd extends MassiveCommand {

    private static EnchantsCmd instance = new EnchantsCmd();
    public static EnchantsCmd get() { return instance; }

    private EnchantsCmd() {
        setAliases("enchants", "viewenchants");
        setDesc("Used to view all Custom Enchants.");

        addRequirements(RequirementIsPlayer.get());
    }

    @Override
    public void perform() {
        new EnchantsGui((Player) this.sender).open();
    }

}
