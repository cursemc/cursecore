package net.aminecraftdev.cursecore.customenchants.commands.customitems;

import com.massivecraft.massivecore.MassiveException;
import com.massivecraft.massivecore.command.MassiveCommand;
import com.massivecraft.massivecore.command.type.primitive.TypeInteger;
import com.massivecraft.massivecore.command.type.sender.TypePlayer;
import com.massivecraft.massivecore.util.MUtil;
import net.aminecraftdev.cursecore.customenchants.commands.types.TypeDust;
import net.aminecraftdev.cursecore.customenchants.handlers.DustType;
import net.aminecraftdev.cursecore.customenchants.handlers.customitems.CustomItemsDestroyDust;
import net.aminecraftdev.cursecore.customenchants.handlers.customitems.CustomItemsSuccessDust;
import net.aminecraftdev.cursecore.entity.LangConf;
import net.aminecraftdev.cursecore.utils.NumberUtils;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class CmdCustomItemsDust extends MassiveCommand {

    public CmdCustomItemsDust() {
        setAliases("dust", "dustyboi");
        setDesc("Give a player a CEnchantment book dust.");

        addParameter(TypePlayer.get(), "target");
        addParameter(TypeInteger.get(), "amount");
        addParameter(TypeDust.get(), "type");
        addParameter(TypeInteger.get(), "dust chance");
    }

    @Override
    public void perform() throws MassiveException {
        Player target = this.readArg();
        int amount = this.readArg();
        DustType dustType = this.readArg();
        int chance = this.readArg();

        ItemStack itemStack = new ItemStack(Material.AIR);

        switch (dustType) {
            case SUCCESS:
                itemStack = CustomItemsSuccessDust.get().getCompleteItem(amount, chance);
                break;
            case DESTROY:
                itemStack = CustomItemsDestroyDust.get().getCompleteItem(amount, chance);
                break;
        }

        target.getInventory().addItem(itemStack);
        LangConf.get().customEnchantsDustReceived.msg(target, MUtil.map("{type}", dustType.name(), "{rate}", NumberUtils.get().formatNumber(chance)));
    }

}
