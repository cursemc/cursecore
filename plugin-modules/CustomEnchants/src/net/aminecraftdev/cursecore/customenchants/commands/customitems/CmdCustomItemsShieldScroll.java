package net.aminecraftdev.cursecore.customenchants.commands.customitems;

import com.massivecraft.massivecore.MassiveException;
import com.massivecraft.massivecore.command.MassiveCommand;
import com.massivecraft.massivecore.command.type.primitive.TypeInteger;
import com.massivecraft.massivecore.command.type.sender.TypePlayer;
import com.massivecraft.massivecore.util.MUtil;
import net.aminecraftdev.cursecore.customenchants.handlers.customitems.CustomItemsOrderScroll;
import net.aminecraftdev.cursecore.entity.LangConf;
import net.aminecraftdev.cursecore.utils.NumberUtils;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class CmdCustomItemsShieldScroll extends MassiveCommand {

    public CmdCustomItemsShieldScroll() {
        setAliases("shieldscroll", "whitescroll", "ss", "ws", "shieldboi");
        setDesc("Give a player a CEnchantment Shield Scroll.");

        addParameter(TypePlayer.get(), "target");
        addParameter(TypeInteger.get(), "amount");
    }

    @Override
    public void perform() throws MassiveException {
        Player target = this.readArg();
        int amount = this.readArg();

        ItemStack itemStack = CustomItemsOrderScroll.get().getCompleteItem(amount);

        target.getInventory().addItem(itemStack);
        LangConf.get().customEnchantsOrderScrollReceived.msg(target, MUtil.map("{amount}", NumberUtils.get().formatNumber(amount)));
    }

}
