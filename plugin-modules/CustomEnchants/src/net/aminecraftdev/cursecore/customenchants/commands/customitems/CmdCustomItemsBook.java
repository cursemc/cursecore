package net.aminecraftdev.cursecore.customenchants.commands.customitems;

import com.massivecraft.massivecore.MassiveException;
import com.massivecraft.massivecore.command.MassiveCommand;
import com.massivecraft.massivecore.command.type.primitive.TypeInteger;
import com.massivecraft.massivecore.command.type.sender.TypePlayer;
import net.aminecraftdev.cursecore.customenchants.commands.types.TypeCEnchantment;
import net.aminecraftdev.cursecore.customenchants.handlers.CEnchantment;
import net.aminecraftdev.cursecore.customenchants.handlers.customitems.CustomItemsBook;
import net.aminecraftdev.cursecore.entity.LangConf;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class CmdCustomItemsBook extends MassiveCommand {

    public CmdCustomItemsBook() {
        setAliases("book");
        setDesc("Give a player a CEnchantment book.");

        addParameter(TypePlayer.get(), "target");
        addParameter(TypeCEnchantment.get(), "enchantment");
        addParameter(TypeInteger.get(), "level");
        addParameter(TypeInteger.get(), "amount");
    }

    @Override
    public void perform() throws MassiveException {
        Player target = this.readArg();
        CEnchantment enchantment = this.readArg();
        int level = this.readArg();
        int amount = this.readArg();

        ItemStack itemStack = CustomItemsBook.get().getCompleteItem(amount, enchantment, level);

        target.getInventory().addItem(itemStack);
        LangConf.get().customEnchantsBookReceived.msg(target);
    }

}
