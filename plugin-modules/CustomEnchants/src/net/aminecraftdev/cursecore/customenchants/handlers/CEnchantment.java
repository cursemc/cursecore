package net.aminecraftdev.cursecore.customenchants.handlers;

import com.massivecraft.massivecore.Engine;
import com.massivecraft.massivecore.util.MUtil;
import com.massivecraft.massivecore.util.Txt;
import com.massivecraft.massivecore.xlib.guava.collect.Lists;
import net.aminecraftdev.cursecore.customenchants.engines.EnchantEngine;
import net.aminecraftdev.cursecore.customenchants.entity.EnchantConf;
import net.aminecraftdev.cursecore.customenchants.entity.EnchantsConf;
import net.aminecraftdev.cursecore.customenchants.entity.wrapper.CEnchantWrapper;
import net.aminecraftdev.cursecore.customenchants.entity.wrapper.RarityWrapper;
import net.aminecraftdev.cursecore.customenchants.events.EnchantUseEvent;
import net.aminecraftdev.cursecore.customenchants.utils.EnchantmentType;
import net.aminecraftdev.cursecore.utils.RandomUtils;
import net.aminecraftdev.cursecore.utils.RomanNumberUtils;
import net.aminecraftdev.cursecore.utils.dependencies.WorldGuardHelper;
import net.aminecraftdev.cursecore.utils.time.TimeUnit;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffectType;

import java.util.*;

public abstract class CEnchantment extends Engine {

    private static Map<Material, Material> ORES_MAP = MUtil.map(
            Material.COAL_ORE, Material.COAL,
            Material.QUARTZ_ORE, Material.QUARTZ,
            Material.IRON_ORE, Material.IRON_INGOT,
            Material.GOLD_ORE, Material.GOLD_INGOT,
            Material.DIAMOND_ORE, Material.DIAMOND,
            Material.EMERALD_ORE, Material.EMERALD,
            Material.REDSTONE_ORE, Material.REDSTONE,
            Material.GLOWING_REDSTONE_ORE, Material.REDSTONE,
            Material.LAPIS_ORE, new ItemStack(Material.INK_SACK, 1, (short) 4).getType()
    );

    private static List<Material> FORTUNE_POSSIBLE = MUtil.list(
            Material.COAL,
            Material.DIAMOND,
            Material.EMERALD,
            Material.REDSTONE,
            Material.LAPIS_ORE
    );

    private static List<PotionEffectType> BAD_EFFECT_TYPES = MUtil.list(
            PotionEffectType.BLINDNESS,
            PotionEffectType.CONFUSION,
            PotionEffectType.HUNGER,
            PotionEffectType.POISON,
            PotionEffectType.SLOW,
            PotionEffectType.SLOW_DIGGING,
            PotionEffectType.WEAKNESS,
            PotionEffectType.WITHER
    );

    private Map<UUID, Long> enchantCooldowns = new HashMap<>();

    public abstract String getName();

    public String getFormattedName() {
        RarityWrapper rarityWrapper = getRarityWrapper();
        String rarityPrefix = "";

        if(rarityWrapper != null) {
            rarityPrefix = rarityWrapper.getPrefix();
        }

        return Txt.parse(rarityPrefix + getName());
    }

    public String getCompleteLoreLine(int level) {
        return Txt.parse(getFormattedName() + " " + RomanNumberUtils.get().getRomanNumber(level));
    }

    public CEnchantWrapper getWrapper() {
        return EnchantsConf.get().enchantWrappers.getOrDefault(getName(), null);
    }

    public RarityWrapper getRarityWrapper() {
        return EnchantConf.get().getRarityWrapper(getWrapper().getRarity());
    }

    public boolean isEnabled() {
        return getWrapper().isEnabled();
    }

    public EnchantmentType getEnchantType() {
        return getWrapper().getEnchantmentType();
    }

    public ItemStack addToItem(ItemStack itemStack, int level) {
        if(level < 1 || level > getWrapper().getMaxLevel()) return itemStack;

        ItemMeta itemMeta = itemStack.getItemMeta();

        if(itemMeta == null) {
            itemMeta = Bukkit.getServer().getItemFactory().getItemMeta(itemStack.getType());
        }

        List<String> lore = itemMeta.getLore() == null? Lists.newArrayList() : itemMeta.getLore();
        List<String> newLore = new ArrayList<>();

        newLore.add(getCompleteLoreLine(level));
        newLore.addAll(lore);

        itemMeta.setLore(newLore);
        itemStack.setItemMeta(itemMeta);

        return itemStack;
    }

    public ItemStack removeFromItem(ItemStack itemStack) {
        ItemMeta itemMeta = itemStack.getItemMeta();

        if(itemMeta == null || !itemMeta.hasLore()) return itemStack;

        List<String> lore = itemMeta.getLore();
        Map<CEnchantment, Integer> enchants = EnchantEngine.get().getCurrentEnchantments(itemStack);

        for(Map.Entry<CEnchantment, Integer> entry : enchants.entrySet()) {
            CEnchantment enchantment = entry.getKey();
            int level = entry.getValue();

            if(enchantment.getName().equals(this.getName())) {
                lore.removeIf(s -> s.endsWith(this.getFormattedName() + " " + RomanNumberUtils.get().getRomanNumber(level)))
;            }
        }

        itemMeta.setLore(lore);
        itemStack.setItemMeta(itemMeta);

        return itemStack;
    }

    public boolean canEnchant(ItemStack itemStack) {
        if(itemStack == null || itemStack.getType() == Material.AIR) return false;

        return getWrapper().getEnchantmentType().doesMaterialEndWith(itemStack.getType()) && itemStack.getAmount() == 1;
    }

    public boolean hasEnchant(Player player) {
        for(ItemStack itemStack : player.getInventory().getArmorContents()) {
            if(hasEnchant(itemStack)) return true;
        }

        return hasEnchant(player.getItemInHand());
    }

    public boolean hasEnchant(ItemStack itemStack) {
        if(itemStack == null || itemStack.getType() == Material.AIR || !getWrapper().getEnchantmentType().doesMaterialEndWith(itemStack.getType())) return false;

        Map<CEnchantment, Integer> enchants = EnchantEngine.get().getCurrentEnchantments(itemStack);

        return enchants.containsKey(this);
    }

    public int getEnchantLevel(Player player) {
        int max = 0;

        for(ItemStack itemStack : player.getInventory().getArmorContents()) {
            if(itemStack == null || itemStack.getType() == Material.AIR) continue;
            if(!hasEnchant(itemStack)) continue;
            if(max > getEnchantLevel(itemStack)) continue;

            max = getEnchantLevel(itemStack);
        }

        int itemInHand = getEnchantLevel(player.getItemInHand());

        if(max < itemInHand) max = itemInHand;
        return max;
    }

    public int getEnchantLevel(ItemStack itemStack) {
        if(itemStack == null || itemStack.getType() == Material.AIR || !getWrapper().getEnchantmentType().doesMaterialEndWith(itemStack.getType())) return -1;

        return EnchantEngine.get().getCurrentEnchantments(itemStack).getOrDefault(this, -1);
    }

    public int getAmountOfPiecesWithEnchant(Player player) {
        int amount = 0;

        for(ItemStack itemStack : player.getInventory().getArmorContents()) {
            if(!hasEnchant(itemStack)) continue;

            amount++;
        }

        if(hasEnchant(player.getItemInHand())) amount++;

        return amount;
    }

    public int getStackedLevel(Player player) {
        int level = 0;

        for(ItemStack itemStack : player.getInventory().getArmorContents()) {
            if(!hasEnchant(itemStack)) continue;

            level += getEnchantLevel(itemStack);
        }

        level += getEnchantLevel(player.getItemInHand());

        return level;
    }

    public boolean isInCooldown(UUID uuid) {
        Long time = this.enchantCooldowns.getOrDefault(uuid, 0L);

        if(time < System.currentTimeMillis()) {
            enchantCooldowns.remove(uuid);
            return false;
        }

        return true;
    }

    public void addCooldown(UUID uuid, int seconds) {
        long newTime = System.currentTimeMillis() + (long) TimeUnit.SECONDS.to(TimeUnit.MILLISECONDS, seconds);

        this.enchantCooldowns.put(uuid, newTime);
    }

    protected boolean canExecute(Player player, int level) {
        return canExecute(player, player.getLocation(), level);
    }

    protected boolean canExecute(Player player, int level, double extraChance) {
        return canExecute(player, player.getLocation(), level, extraChance);
    }

    protected boolean canExecute(Player player, Location location, int level) {
        return canExecute(player, location, level, 0.0D);
    }

    protected boolean canExecute(Player player, Location location, int level, double extraChance) {
        if(isInCooldown(player.getUniqueId()) || !canExecuteLocation(player, location) || !canExecuteChance(level, extraChance)) return false;

        EnchantUseEvent enchantUseEvent = new EnchantUseEvent(player, this, level);
        Bukkit.getPluginManager().callEvent(enchantUseEvent);

        return !enchantUseEvent.isCancelled();
    }

    private boolean canExecuteLocation(Player player, Location location) {
        List<String> currentRegions = WorldGuardHelper.get().getRegionNames(location);

        for(String region : new ArrayList<>(EnchantConf.get().disabledRegions)) {
            if(region == null) continue;

            if(currentRegions.contains(region)) return false;
        }

        //TODO: Faction check
        return true;
    }

    private boolean canExecuteChance(int level, double extraChance) {
        CEnchantWrapper wrapper = getWrapper();

        double maxChance = wrapper.getOccurrence();
        double perLevelChance = maxChance / wrapper.getMaxLevel();
        double chance = perLevelChance * level;
        double randomChance = RandomUtils.get().getRandomBetweenHundred();

        randomChance += RandomUtils.get().getRandomDouble();
        chance += extraChance;

        return randomChance <= chance;
    }

}
