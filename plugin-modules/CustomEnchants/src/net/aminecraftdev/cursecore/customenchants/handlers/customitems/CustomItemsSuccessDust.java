package net.aminecraftdev.cursecore.customenchants.handlers.customitems;

import com.massivecraft.massivecore.util.MUtil;
import net.aminecraftdev.cursecore.customenchants.entity.EnchantConf;
import net.aminecraftdev.cursecore.utils.ISimilar;
import net.aminecraftdev.cursecore.utils.NbtFactory;
import net.aminecraftdev.cursecore.utils.NumberUtils;
import net.aminecraftdev.cursecore.utils.RandomUtils;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import java.util.Map;

public class CustomItemsSuccessDust implements ISimilar<ItemStack> {

    private static String DUST_PATH = "CEnchantment.SuccessDust";

    private static CustomItemsSuccessDust instance = new CustomItemsSuccessDust();
    public static CustomItemsSuccessDust get() { return instance; }

    @Override
    public boolean isSimilar(ItemStack comparable) {
        ItemStack base = getItemStack();

        if(base == null || base.getType() == Material.AIR) return false;
        if(comparable == null || comparable.getType() == Material.AIR) return false;

        NbtFactory.NbtCompound nbtCompound = NbtFactory.fromItemTag(NbtFactory.getCraftItemStack(comparable.clone()));

        return nbtCompound.containsKey(DUST_PATH);
    }

    public ItemStack getCompleteItem(int amount) {
        return getCompleteItem(amount, RandomUtils.get().getRandomBetweenHundred());
    }

    public ItemStack getCompleteItem(int amount, int destroyRate) {
        Map<String, String> replaceMap = MUtil.map("{chance}", NumberUtils.get().formatNumber(destroyRate));
        ItemStack itemStack = EnchantConf.get().successDust.toItemStack(replaceMap);

        itemStack.setAmount(amount);

        ItemStack craftStack = NbtFactory.getCraftItemStack(itemStack);
        NbtFactory.NbtCompound nbtCompound = NbtFactory.fromItemTag(craftStack);

        nbtCompound.put(DUST_PATH, destroyRate);

        return craftStack;
    }

    public int getSuccessRate(ItemStack itemStack) {
        NbtFactory.NbtCompound nbtCompound = NbtFactory.fromItemTag(NbtFactory.getCraftItemStack(itemStack));

        return nbtCompound.getInteger(DUST_PATH, 0);
    }

    private ItemStack getItemStack() {
        return EnchantConf.get().successDust.toItemStack();
    }
}
