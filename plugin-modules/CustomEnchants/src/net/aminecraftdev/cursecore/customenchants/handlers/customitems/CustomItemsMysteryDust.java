package net.aminecraftdev.cursecore.customenchants.handlers.customitems;

import net.aminecraftdev.cursecore.customenchants.entity.EnchantConf;
import net.aminecraftdev.cursecore.utils.ISimilar;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class CustomItemsMysteryDust implements ISimilar<ItemStack> {

    private static CustomItemsMysteryDust instance = new CustomItemsMysteryDust();
    public static CustomItemsMysteryDust get() { return instance; }

    @Override
    public boolean isSimilar(ItemStack comparable) {
        return getCompleteItem(1).isSimilar(comparable);
    }

    public ItemStack getCompleteItem(int amount) {
        ItemStack itemStack = EnchantConf.get().mysteryDust.toItemStack();
        ItemMeta itemMeta = itemStack.getItemMeta();

        itemMeta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES, ItemFlag.HIDE_PLACED_ON, ItemFlag.HIDE_POTION_EFFECTS);
        itemStack.setItemMeta(itemMeta);
        itemStack.setAmount(amount);

        return itemStack;
    }
}
