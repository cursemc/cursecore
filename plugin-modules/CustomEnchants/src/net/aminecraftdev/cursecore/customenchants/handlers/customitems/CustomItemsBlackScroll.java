package net.aminecraftdev.cursecore.customenchants.handlers.customitems;

import net.aminecraftdev.cursecore.customenchants.engines.EnchantEngine;
import net.aminecraftdev.cursecore.customenchants.entity.EnchantConf;
import net.aminecraftdev.cursecore.customenchants.handlers.CEnchantment;
import net.aminecraftdev.cursecore.utils.ISimilar;
import net.aminecraftdev.cursecore.utils.RandomUtils;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class CustomItemsBlackScroll implements ISimilar<ItemStack> {

    private static CustomItemsBlackScroll instance = new CustomItemsBlackScroll();
    public static CustomItemsBlackScroll get() { return instance; }

    @Override
    public boolean isSimilar(ItemStack comparable) {
        return getCompleteItem(1).isSimilar(comparable);
    }

    public ItemStack getCompleteItem(int amount) {
        ItemStack itemStack = EnchantConf.get().blackScroll.toItemStack();

        itemStack.setAmount(amount);
        return itemStack;
    }

    public ItemStack handleBlackScroll(Player player, ItemStack itemStack) {
        Map<CEnchantment, Integer> currentEnchants = EnchantEngine.get().getCurrentEnchantments(itemStack);

        if(currentEnchants.isEmpty()) return itemStack;

        List<CEnchantment> enchantments = new ArrayList<>(currentEnchants.keySet());
        int random = RandomUtils.get().getRandomNumber(enchantments.size());
        CEnchantment randomEnchant = enchantments.get(random);
        int level = currentEnchants.get(randomEnchant);
        ItemStack book = CustomItemsBook.get().getCompleteItem(1, randomEnchant, level);

        itemStack = itemStack.clone();
        randomEnchant.removeFromItem(itemStack);

        if(player.getInventory().firstEmpty() == -1) {
            player.getWorld().dropItemNaturally(player.getLocation(), book);
        } else {
            player.getInventory().addItem(book);
        }

        return itemStack;
    }

}
