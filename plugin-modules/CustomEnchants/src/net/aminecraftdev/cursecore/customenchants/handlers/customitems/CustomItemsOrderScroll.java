package net.aminecraftdev.cursecore.customenchants.handlers.customitems;

import net.aminecraftdev.cursecore.customenchants.engines.EnchantEngine;
import net.aminecraftdev.cursecore.customenchants.entity.EnchantConf;
import net.aminecraftdev.cursecore.customenchants.entity.wrapper.RarityWrapper;
import net.aminecraftdev.cursecore.customenchants.handlers.CEnchantment;
import net.aminecraftdev.cursecore.utils.ISimilar;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.*;

public class CustomItemsOrderScroll implements ISimilar<ItemStack> {

    private static CustomItemsOrderScroll instance = new CustomItemsOrderScroll();
    public static CustomItemsOrderScroll get() { return instance; }

    @Override
    public boolean isSimilar(ItemStack comparable) {
        return getCompleteItem(1).isSimilar(comparable);
    }

    public ItemStack getCompleteItem(int amount) {
        ItemStack itemStack = EnchantConf.get().orderScroll.toItemStack();

        itemStack.setAmount(amount);

        return itemStack;
    }

    public ItemStack handleOrderScroll(ItemStack itemStack) {
        Map<CEnchantment, Integer> currentEnchants = EnchantEngine.get().getCurrentEnchantments(itemStack);

        if(currentEnchants.isEmpty()) return itemStack;

        Map<String, TreeMap<String, String>> formattedMap = new HashMap<>();
        ItemMeta itemMeta = itemStack.getItemMeta();

        if(!itemMeta.hasLore()) return itemStack;

        List<String> currentLore = itemMeta.getLore();

        for(Map.Entry<CEnchantment, Integer> entry : currentEnchants.entrySet()) {
            CEnchantment enchantment = entry.getKey();
            int level = entry.getValue();

            String rarity = enchantment.getWrapper().getRarity().toUpperCase();
            String completeLine = enchantment.getCompleteLoreLine(level);

            if(level < 1) continue;
            if(!currentLore.contains(completeLine)) continue;

            currentLore.remove(completeLine);

            TreeMap<String, String> map = formattedMap.getOrDefault(rarity, new TreeMap<>());

            map.put(enchantment.getName(), enchantment.getCompleteLoreLine(level));
            formattedMap.put(rarity, map);
        }

        List<String> newLore = new ArrayList<>();

        for(RarityWrapper rarityWrapper : RarityWrapper.getSortedWrappers()) {
            handleRarityLore(formattedMap.getOrDefault(rarityWrapper.getName().toUpperCase(), new TreeMap<>()), newLore);
        }

        if(!currentLore.isEmpty()) newLore.addAll(currentLore);

        itemMeta.setLore(newLore);
        itemStack.setItemMeta(itemMeta);

        return itemStack;
    }

    private void handleRarityLore(Map<String, String> entryMap, List<String> lore) {
        entryMap.forEach((s, display) -> lore.add(display));
    }
}
