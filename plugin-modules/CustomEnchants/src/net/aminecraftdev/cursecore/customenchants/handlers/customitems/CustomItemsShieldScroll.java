package net.aminecraftdev.cursecore.customenchants.handlers.customitems;

import com.massivecraft.massivecore.util.Txt;
import net.aminecraftdev.cursecore.customenchants.entity.EnchantConf;
import net.aminecraftdev.cursecore.utils.ISimilar;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

public class CustomItemsShieldScroll implements ISimilar<ItemStack> {

    private static CustomItemsShieldScroll instance = new CustomItemsShieldScroll();
    public static CustomItemsShieldScroll get() { return instance; }

    @Override
    public boolean isSimilar(ItemStack comparable) {
        return getCompleteItem(1).isSimilar(comparable);
    }

    public ItemStack getCompleteItem(int amount) {
        ItemStack itemStack = EnchantConf.get().shieldScroll.toItemStack();

        itemStack.setAmount(amount);

        return itemStack;
    }

    public boolean isProtected(ItemStack itemStack) {
        if(!itemStack.hasItemMeta()) return false;

        ItemMeta itemMeta = itemStack.getItemMeta();

        if(!itemMeta.hasLore()) return false;

        for(String line : itemMeta.getLore()) {
            if(line.equalsIgnoreCase(getProtectedString())) return true;
        }

        return false;
    }

    public void addProtection(ItemStack input) {
        ItemMeta itemMeta = input.getItemMeta();
        List<String> currentLore = itemMeta.getLore();
        List<String> newLore = new ArrayList<>(currentLore);

        newLore.add(getProtectedString());

        itemMeta.setLore(newLore);
        input.setItemMeta(itemMeta);
    }

    public ItemStack removeProtection(ItemStack input) {
        ItemMeta itemMeta = input.getItemMeta();
        List<String> currentLore = itemMeta.getLore();
        List<String> newLore = new ArrayList<>();

        for(String s : currentLore) {
            if(s.equalsIgnoreCase(getProtectedString())) continue;

            newLore.add(s);
        }

        itemMeta.setLore(newLore);
        input.setItemMeta(itemMeta);

        return input;
    }

    private String getProtectedString() {
        return Txt.parse(EnchantConf.get().shieldScrollString);
    }
}
