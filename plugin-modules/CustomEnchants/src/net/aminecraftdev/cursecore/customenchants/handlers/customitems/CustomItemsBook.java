package net.aminecraftdev.cursecore.customenchants.handlers.customitems;

import com.massivecraft.massivecore.util.MUtil;
import com.massivecraft.massivecore.util.Txt;
import net.aminecraftdev.cursecore.customenchants.engines.EnchantEngine;
import net.aminecraftdev.cursecore.customenchants.entity.EnchantConf;
import net.aminecraftdev.cursecore.customenchants.entity.wrapper.CEnchantWrapper;
import net.aminecraftdev.cursecore.customenchants.entity.wrapper.RarityWrapper;
import net.aminecraftdev.cursecore.customenchants.handlers.CEnchantment;
import net.aminecraftdev.cursecore.utils.*;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class CustomItemsBook implements ISimilar<ItemStack> {

    private static final String ENCHANT_NAME = "CEnchantment.Name", ENCHANT_LEVEL = "CEnchantment.Level", ENCHANT_SUCCESS = "CEnchantment.Success", ENCHANT_DESTROY = "CEnchantment.Destroy";

    private static CustomItemsBook instance = new CustomItemsBook();
    public static CustomItemsBook get() { return instance; }

    @Override
    public boolean isSimilar(ItemStack comparable) {
        ItemStack base = getItemStack();

        if(base == null || base.getType() == Material.AIR) return false;
        if(comparable == null || comparable.getType() == Material.AIR) return false;

        NbtFactory.NbtCompound nbtCompound = NbtFactory.fromItemTag(NbtFactory.getCraftItemStack(comparable.clone()));

        return nbtCompound.containsKey(ENCHANT_NAME) && nbtCompound.containsKey(ENCHANT_LEVEL) && nbtCompound.containsKey(ENCHANT_SUCCESS) && nbtCompound.containsKey(ENCHANT_DESTROY);
    }

    public ItemStack modifyDestroyChance(ItemStack itemStack, int newDestroyRate) {
        int successRate = getSuccess(itemStack);
        CEnchantment enchantment = getEnchant(itemStack);
        int level = getLevel(itemStack);

        return modifyEnchantBook(successRate, newDestroyRate, enchantment, level);
    }

    public ItemStack modifySuccessChance(ItemStack itemStack, int newSuccessRate) {
        int destroyRate = getDestroy(itemStack);
        CEnchantment enchantment = getEnchant(itemStack);
        int level = getLevel(itemStack);

        return modifyEnchantBook(newSuccessRate, destroyRate, enchantment, level);
    }

    public ItemStack getCompleteItem(int amount, CEnchantment enchantment, int level) {
        return getCompleteItem(amount, enchantment, level, RandomUtils.get().getRandomBetweenHundred(), RandomUtils.get().getRandomBetweenHundred());
    }

    public ItemStack getCompleteItem(int amount, CEnchantment enchantment, int level, int success, int destroy) {
        ItemStack itemStack = modifyEnchantBook(success, destroy, enchantment, level);

        itemStack.setAmount(amount);

        return itemStack;
    }

    public CEnchantment getEnchant(ItemStack itemStack) {
        NbtFactory.NbtCompound nbtCompound = NbtFactory.fromItemTag(NbtFactory.getCraftItemStack(itemStack.clone()));

        return EnchantEngine.get().getByName(nbtCompound.getString(ENCHANT_NAME, ""));
    }

    public int getLevel(ItemStack itemStack) {
        NbtFactory.NbtCompound nbtCompound = NbtFactory.fromItemTag(NbtFactory.getCraftItemStack(itemStack.clone()));

        return nbtCompound.getInteger(ENCHANT_LEVEL, 0);
    }

    public int getSuccess(ItemStack itemStack) {
        NbtFactory.NbtCompound nbtCompound = NbtFactory.fromItemTag(NbtFactory.getCraftItemStack(itemStack.clone()));

        return nbtCompound.getInteger(ENCHANT_SUCCESS, 0);
    }

    public int getDestroy(ItemStack itemStack) {
        NbtFactory.NbtCompound nbtCompound = NbtFactory.fromItemTag(NbtFactory.getCraftItemStack(itemStack.clone()));

        return nbtCompound.getInteger(ENCHANT_DESTROY, 0);
    }

    public boolean canApply(ItemStack itemStack, CEnchantment enchantment) {
        if(!enchantment.canEnchant(itemStack)) return false;

        int enchantAmount = EnchantEngine.get().getEnchantsAmount(itemStack);

        return enchantAmount+1 <= EnchantConf.get().maxEnchants;
    }

    private ItemStack modifyEnchantBook(int successRate, int destroyRate, CEnchantment enchantment, int level) {
        CEnchantWrapper enchantWrapper = enchantment.getWrapper();
        RarityWrapper rarityWrapper = enchantment.getRarityWrapper();

        Map<String, String> replaceMap = MUtil.map(
                "{rarityColor}", rarityWrapper.getPrefix(),
                "{enchantName}", enchantment.getName(),
                "{enchantLevel}", RomanNumberUtils.get().getRomanNumber(level),
                "{success}", NumberUtils.get().formatNumber(successRate),
                "{destroy}", NumberUtils.get().formatNumber(destroyRate),
                "{type}", enchantWrapper.getEnchantmentType().name()
        );

        ItemStack itemStack = EnchantConf.get().enchantBook.toItemStack(replaceMap);
        ItemMeta itemMeta = itemStack.getItemMeta();
        List<String> lore = itemMeta.getLore();
        List<String> newLore = new ArrayList<>();

        for(String s : lore) {
            if(s.contains("{description}")) {
                newLore.addAll(enchantWrapper.getDescription());
            } else {
                newLore.add(s);
            }
        }

        newLore.replaceAll(Txt::parse);
        itemMeta.setLore(newLore);
        itemStack.setItemMeta(itemMeta);

        ItemStack craftStack = NbtFactory.getCraftItemStack(itemStack);
        NbtFactory.NbtCompound nbtCompound = NbtFactory.fromItemTag(craftStack);

        nbtCompound.put(ENCHANT_NAME, enchantment.getName());
        nbtCompound.put(ENCHANT_LEVEL, level);
        nbtCompound.put(ENCHANT_SUCCESS, successRate);
        nbtCompound.put(ENCHANT_DESTROY, destroyRate);

        return craftStack;
    }

    private ItemStack getItemStack() {
        return EnchantConf.get().enchantBook.toItemStack();
    }

}
