package net.aminecraftdev.cursecore.customenchants;

import net.aminecraftdev.cursecore.customenchants.commands.*;
import net.aminecraftdev.cursecore.customenchants.enchants.bad.*;
import net.aminecraftdev.cursecore.customenchants.enchants.epic.*;
import net.aminecraftdev.cursecore.customenchants.enchants.good.*;
import net.aminecraftdev.cursecore.customenchants.enchants.supers.*;
import net.aminecraftdev.cursecore.customenchants.engines.ArmorClickEngine;
import net.aminecraftdev.cursecore.customenchants.engines.EnchantEngine;
import net.aminecraftdev.cursecore.customenchants.engines.customitems.*;
import net.aminecraftdev.cursecore.customenchants.engines.gui.EnchantPreviewGuiEngine;
import net.aminecraftdev.cursecore.customenchants.engines.gui.EnchanterGuiEngine;
import net.aminecraftdev.cursecore.customenchants.engines.gui.EnchantsGuiEngine;
import net.aminecraftdev.cursecore.customenchants.engines.gui.TinkererEngine;
import net.aminecraftdev.cursecore.customenchants.entity.coll.EnchantInventoryConfColl;
import net.aminecraftdev.cursecore.customenchants.entity.coll.EnchantsConfColl;
import net.aminecraftdev.cursecore.customenchants.handlers.CEnchantment;
import net.aminecraftdev.cursecore.customenchants.tasks.TaskRagePlayer;
import net.aminecraftdev.cursecore.storage.PluginStorage;
import net.aminecraftdev.cursecore.storage.SubPlugin;
import net.aminecraftdev.cursecore.customenchants.entity.coll.EnchantConfColl;

public class CustomEnchants extends SubPlugin {

    private static CustomEnchants instance;
    public static CustomEnchants get() { return instance; }

    @Override
    public String getPluginName() {
        return "CustomEnchants";
    }

    @Override
    protected void onEnableInner() {
        instance = this;

        addColl(EnchantConfColl.class);
        addColl(EnchantsConfColl.class);
        addColl(EnchantInventoryConfColl.class);

        addEngine(ArmorClickEngine.class);
        addEngine(EnchantEngine.class);

        addEngine(EnchantPreviewGuiEngine.class);
        addEngine(EnchantsGuiEngine.class);
        addEngine(EnchanterGuiEngine.class);
        addEngine(TinkererEngine.class);

        addEngine(CustomItemBlackScrollEngine.class);
        addEngine(CustomItemBookEngine.class);
        addEngine(CustomItemDestroyDustEngine.class);
        addEngine(CustomItemMysteryDustEngine.class);
        addEngine(CustomItemOrderScrollEngine.class);
        addEngine(CustomItemShieldScrollEngine.class);
        addEngine(CustomItemSuccessDustEngine.class);

        addCommand(CustomItemsCmd.class);
        addCommand(EnchanterCmd.class);
        addCommand(EnchantsCmd.class);
        addCommand(GKitCmd.class);
        addCommand(TinkererCmd.class);

        addTask(TaskRagePlayer.class);
    }

    @Override
    protected void onEnablePost() {
        PluginStorage.get().log(" - Activating Enchants...");
        registerCustomEnchants();
    }

    @Override
    protected void onDisable() {
        unregisterCustomEnchants();
    }

    private void registerCustomEnchants() {
        /* BAD ENCHANTMENTS */
        PluginStorage.get().log("  == Bad Enchantments ==");
        registerEnchant(new Blackout());
        registerEnchant(new Fish());
        registerEnchant(new RetainXP());
        registerEnchant(new Scream());
        registerEnchant(new Vision());

        /* GOOD ENCHANTMENTS */
        PluginStorage.get().log("  == Good Enchantments ==");
        registerEnchant(new Arrow());
        registerEnchant(new BlastMine());
        registerEnchant(new Blunt());
        registerEnchant(new Bunny());
        registerEnchant(new CloudWalker());
        registerEnchant(new Explode());
        registerEnchant(new GreenGas());
        registerEnchant(new NoHunger());
        registerEnchant(new ObsidianDestroyer());
        registerEnchant(new Reduce());
        registerEnchant(new WebTrap());

        /* EPIC ENCHANTMENTS */
        PluginStorage.get().log("  == Epic Enchantments ==");
        registerEnchant(new Boom());
        registerEnchant(new Defend());
        registerEnchant(new ExtraHealth());
        registerEnchant(new Fast());
        registerEnchant(new FastBreak());
        registerEnchant(new McMMO());
        registerEnchant(new Ploy());

        /* SUPER ENCHANTMENTS */
        PluginStorage.get().log("  == Super Enchantments ==");
        registerEnchant(new AntiSteal());
        registerEnchant(new AttackDeflect());
        registerEnchant(new BullDog());
        registerEnchant(new Damage());
        registerEnchant(new Decay());
        registerEnchant(new Direct());
        registerEnchant(new ExtraOres());
        registerEnchant(new FireProtect());
        registerEnchant(new HeadRip());
        registerEnchant(new Hurt());
        registerEnchant(new ItemSteal());
        registerEnchant(new Lifeless());
        registerEnchant(new LifeSaver());
        registerEnchant(new Mob());
        registerEnchant(new MoneySteal());
        registerEnchant(new MoreXP());
        registerEnchant(new Shatter());
        registerEnchant(new Soul());
        registerEnchant(new SuperStrike());
        registerEnchant(new Tough());
        registerEnchant(new Voltage());
    }

    private void unregisterCustomEnchants() {
        EnchantEngine.get().getEnchantments().forEach(enchant -> EnchantEngine.get().unregisterCustomEnchant(enchant.getName()));
    }

    private void registerEnchant(CEnchantment enchantment) {
        try {
            enchantment.getName();
            enchantment.getWrapper();
            enchantment.getRarityWrapper();

            PluginStorage.get().log("  - Registering " + enchantment.getName());
            EnchantEngine.get().registerCustomEnchant(enchantment);
            addEngine(enchantment.getClass());
        } catch (Exception ex) {
            PluginStorage.get().log("  - Failed to register " + enchantment.getClass().getSimpleName());
        }

    }
}
